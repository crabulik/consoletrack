﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Markup;

namespace ScrumTrackerWPF.WpfConverters
{
    class ConvertorStaticExtension : StaticExtension
    {
        public ConvertorStaticExtension(string member) : base(member)
    {
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            
            object value = base.ProvideValue(serviceProvider);

            
            IProvideValueTarget target =
                serviceProvider.GetService(typeof(IProvideValueTarget)) as IProvideValueTarget;
            if (target != null)
            {
                Type targetType = typeof(object);
                
                DependencyProperty dp = target.TargetProperty as DependencyProperty;
                if (dp != null)
                {
                    targetType = dp.PropertyType;
                }
                else
                {
                    
                    PropertyInfo pi = target.TargetProperty as PropertyInfo;
                    if (pi != null)
                        targetType = pi.PropertyType;
                }

                
                TypeConverter converter = TypeDescriptor.GetConverter(targetType);
               
                value = converter.ConvertFrom(value);
            }

            return value;
        }
    }
}
