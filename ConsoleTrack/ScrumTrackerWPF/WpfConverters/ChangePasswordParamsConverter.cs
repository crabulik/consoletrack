﻿using System;
using System.Globalization;
using System.Windows.Controls;
using System.Windows.Data;
using ScrumTrackerWPF.Models;

namespace ScrumTrackerWPF.WpfConverters
{
    public class ChangePasswordParamsConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values.Length == 3)
            {
                var oldPassword = values[0] as PasswordBox;
                var newassword = values[1] as PasswordBox;
                var confirmNewPassword = values[2] as PasswordBox;
                if ((oldPassword != null) && (newassword != null) && (confirmNewPassword != null))
                {
                    return new ChangePasswordData
                    {
                        ConfirmNewPassword = confirmNewPassword,
                        NewPassword = newassword,
                        OldPassword = oldPassword
                    };
                }
            }
            throw new ArgumentException("Wrong data binding for changing user's password.");
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}