﻿using System;
using System.Windows.Input;

namespace ScrumTrackerWPF.Infrastructure
{
    public class DelegateCommand: ICommand
    {
        private readonly bool _canExecute;
        private readonly Action _execute;

        public bool CanExecute(object parameter)
        {
            return _canExecute;
        }

        public void Execute(object parameter)
        {
            _execute();
        }

        private event EventHandler CanExecuteChangedInternal;

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
                this.CanExecuteChangedInternal += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
                this.CanExecuteChangedInternal -= value;
            }
        }

        public DelegateCommand(Action execute)
                       : this(execute, true)
        {
        }

        public DelegateCommand(Action execute,
                       bool canExecute)
        {
            _execute = execute;
            _canExecute = canExecute;
        }

        public void RaiseCanExecuteChanged()
        {
            EventHandler handler = this.CanExecuteChangedInternal;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }
    }
}