﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using ScrumTrackerWPF.CoreServices;
using ScrumTrackerWPF.ShellInfrastructure;

namespace ScrumTrackerWPF
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private TaskTrackerCore _core;

        protected override void OnStartup(StartupEventArgs e)
        {
            _core = new TaskTrackerCore();
            var shell = new Shell();
            MainWindow = shell;
            shell.Init(new ShellViewModel(_core));
            base.OnStartup(e);
            Application.Current.MainWindow.Show();           
        }
    }
}
