﻿namespace ScrumTrackerWPF.Enums
{
    public enum RootView
    {
        ManageUsers,
        EditProfile,
        AddUser,
        CreateRole
    }
}