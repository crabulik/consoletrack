﻿using ScrumTrackerWPF.CoreServices;
using ScrumTrackerWPF.Enums;
using ScrumTrackerWPF.Infrastructure;
using ScrumTrackerWPF.ViewModels;

namespace ScrumTrackerWPF
{
    public sealed class ShellViewModel: ViewModelBase
    {
        private TaskTrackerCore Core { get; }

        private DelegateCommand _manageUsersCommand;
        private DelegateCommand _editProfileCommand;
        private DelegateCommand _createRoleCommand;

        public ShellViewModel(TaskTrackerCore core)
        {
            Core = core;
        }

        public void StartApplication(StartDisplayData initData)
        {
            Core.Start(initData);
        }

        public DelegateCommand ManageUsersCommand
        {
            get { return _manageUsersCommand ?? (_manageUsersCommand = new DelegateCommand(ManageUsers)); }
        }

        private void ManageUsers()
        {
            Core.OpenRootView(RootView.ManageUsers);
        }

        public DelegateCommand EditProfileCommand
        {
            get { return _editProfileCommand ?? (_editProfileCommand = new DelegateCommand(EditProfile)); }
        }

        private void EditProfile()
        {
            Core.OpenRootView(RootView.EditProfile);
        }

        public DelegateCommand CreateRoleCommand
        {
            get { return _createRoleCommand ?? (_createRoleCommand = new DelegateCommand(CreateRole)); }
        }

        private void CreateRole()
        {
            Core.OpenRootView(RootView.CreateRole);
        }
        
    }
}