﻿using System.Windows;
using ScrumTrackerWPF.CoreServices;

namespace ScrumTrackerWPF.ShellInfrastructure
{
    /// <summary>
    /// Interaction logic for Shell.xaml
    /// </summary>
    public partial class Shell : Window
    {
        public Shell()
        {
            InitializeComponent();
        }

        public void Init(ShellViewModel shellVm)
        {
            DataContext = shellVm;
            shellVm.StartApplication(new StartDisplayData
            {
                ContentNavigation = ContentNavigation,
                ContentPart = ContentPart
            });
        }
    }
}
