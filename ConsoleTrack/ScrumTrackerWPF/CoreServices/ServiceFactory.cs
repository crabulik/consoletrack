﻿using TaskTracker.DAL;
using TaskTracker.DAL.Contexts;
using TaskTracker.Services.DbServices;
using TaskTracker.Services.NotificationServices;
using TaskTracker.Services.Services;

namespace ScrumTrackerWPF.CoreServices
{
    public class ServiceFactory
    {

        public ServiceFactory()
        {
        }
        
        public DbUsersService GetUsersService()
        {
            return new DbUsersService(GetUnitOfWork());
        }

        public DbInfrastructureService GetInfrastructureService()
        {
            return new DbInfrastructureService(GetUnitOfWork());
        }

        public AuthorizationService GetAuthorizationService(DbUsersService usersService = null)
        {
            if (usersService == null)
            {
                usersService = GetUsersService();
            }

            return new AuthorizationService(usersService, GetUsersNotifier());
        }

        public UsersNotifier GetUsersNotifier()
        {
            return new UsersNotifier();
        }

        private UnitOfWork GetUnitOfWork()
        {
            var context = new TaskTrackerContext();
            return new UnitOfWork(context);
        }

    }
}