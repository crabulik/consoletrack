﻿using TaskTracker.Entities.General;

namespace ScrumTrackerWPF.CoreServices.Session
{
    public class CurrentUser
    {
        public int UserId { get; set; }

        public string Email { get; set; }

        public void FillDataFrom(User user)
        {
            UserId = user.UserId;
            Email = user.Email;
        }
    }
}