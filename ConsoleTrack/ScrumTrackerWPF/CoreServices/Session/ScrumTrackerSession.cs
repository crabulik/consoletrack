﻿namespace ScrumTrackerWPF.CoreServices.Session
{
    public class ScrumTrackerSession
    {
        public CurrentUser User { get; set; }

        public ScrumTrackerSession()
        {
            User = new CurrentUser();
        }
    }
}