﻿using System.Windows.Controls;

namespace ScrumTrackerWPF.CoreServices
{
    public struct StartDisplayData
    {
        public ContentControl ContentPart;
        public ContentControl ContentNavigation;
    }
}