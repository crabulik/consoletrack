﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using ScrumTrackerWPF.ViewModels;
using ScrumTrackerWPF.Views;

namespace ScrumTrackerWPF.CoreServices
{
    public sealed class NavigationManager
    {
        private ContentControl _frame = new ContentControl();
        private UserControl _currentView;

        public UserControl _defaultPage;

        public UserControl DefaultPage
        {
            set { _defaultPage = value; }
        }

        private FrameworkElement _navigationBar;

        public NavigationManager()
        {
            _defaultPage = new UserControl(){ Background = Brushes.AntiqueWhite };
        }

        public ContentControl Frame
        {
            get
            {
                return _frame;
            }
            set
            {
                _frame = value;
            }
        }

        public void Navigate(UserControl view)
        {
            if (view == null)
                throw new NullReferenceException("NavigationManager. View can not be null.");

            _frame.Content = view;
            _currentView = view;
        }

        public FrameworkElement NavigationBar
        {
            get
            {
                return _navigationBar;
            }
            set
            {
                _navigationBar = value;
            }
        }

        public void ShowNavigationBar()
        {
            if (_navigationBar != null)
                _navigationBar.Visibility = Visibility.Visible;
        }

        public void HideNavigationBar()
        {
            if (_navigationBar != null)
                _navigationBar.Visibility = Visibility.Collapsed;
        }

        public void NavigateToDefaultPage()
        {
            ShowNavigationBar();
            Navigate(_defaultPage);
        }

        public void ShowLoginForm(LoginViewModel loginVm)
        {
            var loginView = new LoginView {DataContext = loginVm };
            HideNavigationBar();
            Navigate(loginView);
        }

        public void ShowManageUsersForm(UsersViewModel usersViewModel)
        {
            Navigate(new UsersView {DataContext = usersViewModel});
        }

        public void ShowEditProfileForm(EditProfileViewModel editProfileVm)
        {
            Navigate(new EditProfileView { DataContext = editProfileVm });
        }

        public void ShowAddUserForm(AddUserViewModel addUserVm)
        {
            Navigate(new AddUserView {DataContext = addUserVm});
        }

        public void ShowEditUserForm(EditUserViewModel editUserVm)
        {
            Navigate(new EditUserView() { DataContext = editUserVm });
        }

        public void ShowChangePasswordForm(ChangePasswordViewModel changePasswordViewModel)
        {
            Navigate(new ChangePasswordView { DataContext = changePasswordViewModel });
        }

        public void ShowCreateRoleForm(CreateRoleViewModel createRoleVm)
        {
            Navigate(new CreateRoleView {DataContext = createRoleVm });
        }
    }
}