﻿using System;
using System.Windows;
using ScrumTrackerWPF.CoreServices.Session;
using ScrumTrackerWPF.Enums;
using ScrumTrackerWPF.Models.EventModels;
using ScrumTrackerWPF.ViewModels;
using TaskTracker.DAL;
using TaskTracker.DAL.Contexts;
using TaskTracker.Resources;
using TaskTracker.Services.DbServices;

namespace ScrumTrackerWPF.CoreServices
{
    public sealed class TaskTrackerCore
    {
        private NavigationManager NavigationManager { get; }
        private ScrumTrackerSession Session { get; }
        private ServiceFactory ServiceFactory { get; }

        private DbUsersService UsersService { get; }

        public TaskTrackerCore()
        {
            NavigationManager = new NavigationManager();
            Session = new ScrumTrackerSession();
            ServiceFactory = new ServiceFactory();
            UsersService = ServiceFactory.GetUsersService();
        }

        public void Start(StartDisplayData initDisplayData)
        {
            NavigationManager.Frame = initDisplayData.ContentPart;
            NavigationManager.NavigationBar = initDisplayData.ContentNavigation;
            
            var loginVm = new LoginViewModel(ServiceFactory.GetAuthorizationService());
            loginVm.UserAuthenticated += OnUserAuthenticated;
            NavigationManager.ShowLoginForm(loginVm);           
        }

        public void OpenRootView(RootView view)
        {
            if (!(Session.User.UserId > 0))
            {
                MessageBox.Show(TaskTracker.Resources.SystemResources.rs_UserNotAuthentificatedExitMessage);
                Environment.Exit(0);
            }
            switch (view)
            {
                case RootView.ManageUsers:
                    var manageUsersService = ServiceFactory.GetUsersService();
                    var manageUsersAuthService = ServiceFactory.GetAuthorizationService(manageUsersService);
                    var addUserVm = new AddUserViewModel(manageUsersService, ServiceFactory.GetInfrastructureService(), manageUsersAuthService);
                    var editUserVm = new EditUserViewModel(manageUsersService, ServiceFactory.GetInfrastructureService());
                    var manageUsersVm = new UsersViewModel(manageUsersService,
                        addUserVm,
                        editUserVm,
                        manageUsersAuthService) {NavigMgr = NavigationManager};
                    NavigationManager.ShowManageUsersForm(manageUsersVm);
                    break;
                case RootView.EditProfile:
                    var editProfileVm = new EditProfileViewModel(ServiceFactory.GetUsersService());
                    NavigationManager.ShowEditProfileForm(editProfileVm);
                    break;
                case RootView.CreateRole:
                    var createRoleVm = new CreateRoleViewModel(ServiceFactory.GetUsersService());
                    createRoleVm.NewRoleCreated += OnNewRoleCreated;
                    NavigationManager.ShowCreateRoleForm(createRoleVm);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(view), view, null);
            }
        }

        private void OnUserAuthenticated(object sender, UserAuthenticatedEventArgs userAuthenticatedEventArgs)
        {
            if (userAuthenticatedEventArgs != null)
            {
                var user = UsersService.GetUser(userAuthenticatedEventArgs.UserId);
                if (user != null)
                {
                    Session.User.FillDataFrom(user);

                    if (user.IsNeedPassCorrection)
                    {
                        var changeTempPasswordVm = new ChangePasswordViewModel(ServiceFactory.GetAuthorizationService());
                        changeTempPasswordVm.Init(user, ViewsResources.rs_ChangeTempPasswordCaption);
                        changeTempPasswordVm.ChangePasswordCanceled += OnChangePasswordCanceled;
                        changeTempPasswordVm.ChangePasswordSuccess += OnChangePasswordSuccess;

                        NavigationManager.ShowChangePasswordForm(changeTempPasswordVm);
                    }
                    else
                    {
                        NavigationManager.NavigateToDefaultPage();
                    }
                }
                else
                {
                    MessageBox.Show(TaskTracker.Resources.SystemResources.rs_UserNotAuthentificatedExitMessage);
                    Environment.Exit(0);
                }               
            }
        }

        private void OnChangePasswordSuccess(object sender, EventArgs e)
        {
            NavigationManager.NavigateToDefaultPage();
        }

        private void OnChangePasswordCanceled(object sender, EventArgs e)
        {
            NavigationManager.NavigateToDefaultPage();
        }

        private void OnNewRoleCreated(object sender, EventArgs e)
        {
            NavigationManager.NavigateToDefaultPage();
        }
    }
}