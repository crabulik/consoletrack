﻿using System;
using System.Windows.Controls;
using System.Windows.Input;
using ScrumTrackerWPF.Infrastructure;
using ScrumTrackerWPF.Models.EventModels;
using TaskTracker.Constants;
using TaskTracker.Resources;
using TaskTracker.Services.Services;

namespace ScrumTrackerWPF.ViewModels
{
    public class LoginViewModel: ViewModelBase
    {
        private string _email;

        private AuthorizationService AuthorizationService { get; }

        private DelegateParamCommand _loginCommand;
        private string _loginErrorMessage;

        public LoginViewModel(AuthorizationService authorizationService)
        {
            AuthorizationService = authorizationService;
            LoginErrorMessage = string.Empty;
        }

        public string Email
        {
            get { return _email; }
            set
            {
                _email = value; 
                NotifyPropertyChanged(nameof(Email));
            }
        }

        public bool IsLoginError
        {
            get { return !string.IsNullOrEmpty(LoginErrorMessage); }
        }

        public string LoginErrorMessage
        {
            get { return _loginErrorMessage; }
            set
            {
                _loginErrorMessage = value;
                NotifyPropertyChanged(nameof(IsLoginError));
                NotifyPropertyChanged(nameof(LoginErrorMessage));
            }
        }


        public event EventHandler<UserAuthenticatedEventArgs> UserAuthenticated;

        protected virtual void OnUserUserAuthenticated(UserAuthenticatedEventArgs e)
        {
            var receivers = UserAuthenticated;
            receivers?.Invoke(this, e);
        }

        public DelegateParamCommand LoginCommand
        {
            get { return _loginCommand ?? (_loginCommand = new DelegateParamCommand(Login, CanExecuteLogin)); }
        }

        private bool CanExecuteLogin(object pass)
        {
            var password = pass as PasswordBox;
            if ((password == null) || (password.Password.Length == 0))
            {
                return false;
            }
            return true;
        }

        private void Login(object pass)
        {
            var passwordBox = pass as PasswordBox;
            if(passwordBox == null)
                return;
            var password = passwordBox.Password;
            LoginErrorMessage = string.Empty;
            var checkResult = CheckRulesForName();
            if (checkResult)
            {
                var loginResult = AuthorizationService.LogIn(Email, password);
                if (loginResult != DbConstants.LoginFail)
                {
                    OnUserUserAuthenticated(new UserAuthenticatedEventArgs(loginResult));
                }
                else
                {
                    ShowErrorLoginCaption();
                }
            }
        }

        private void ShowErrorLoginCaption()
        {
            LoginErrorMessage = ViewsResources.rs_LoginViewEmailOsPasswordIncorrectMessager;
        }

        private bool CheckRulesForName()
        {
            var result = true;
            RemoveError(nameof(Email));
            if (string.IsNullOrEmpty(Email))
            {
                AddError(nameof(Email), ViewsResources.rs_LoginViewEmailMustBeSetError);
                result = false;
            }
            else if (Email.Length > DbConstants.LongString)
            {
                AddError(nameof(Email), ViewsResources.rs_LoginViewEmailMustBeLess256);
                result = false;
            }

            return result;
        }
    }
}