﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using ScrumTrackerWPF.Infrastructure;
using TaskTracker.DAL.DTO.Enums;
using TaskTracker.Entities.General;
using TaskTracker.Resources;
using TaskTracker.Services.DbServices;

namespace ScrumTrackerWPF.ViewModels
{
    public class CreateRoleViewModel : ViewModelBase
    {
        private string _newRoleCaption;
        private DelegateCommand _createRoleCommand;
        private IList<string> _actions;
        private IList<RoleAction> _roleAction; 
        private DbUsersService UsersService { get; }
        private Role NewRole { get; set; }

        public CreateRoleViewModel(DbUsersService usersService)
        {
            UsersService = usersService;
            _actions = GetEnumsAsList();
        }

        public string RoleCaption
        {
            get { return _newRoleCaption; }
            set
            {
                _newRoleCaption = value;
                NotifyPropertyChanged(nameof(RoleCaption));
            }
        }

        public IList<string> Actions
        {
            get { return _actions; }
            set
            {
                _actions = value;
                NotifyErrorsChanged(nameof(Actions));
            }
        }

        public event EventHandler NewRoleCreated;

        public DelegateCommand CreateRoleCommand
        {
            get { return _createRoleCommand ?? (_createRoleCommand = new DelegateCommand(CreateRole)); }
        }

        protected virtual void OnNewRoleCreated()
        {
            var receivers = NewRoleCreated;
            receivers?.Invoke(this, EventArgs.Empty);
        }

        private IList<string> GetEnumsAsList()
        {
            return Enum.GetNames(typeof (ProjectActionsEnum)).ToList();
        } 

        private bool CheckCreationRulesRespected()
        {
            var result = true;
            RemoveError(nameof(RoleCaption));
            if (string.IsNullOrWhiteSpace(RoleCaption))
            {
                AddError(nameof(RoleCaption), ViewsResources.rs_CreateRoleViewCaptionMustBeSetError);
                result = false;
            }
            return result;
        }

        private void CreateRole()
        {
            if (CheckCreationRulesRespected())
            {
                var role = UsersService.GetRole(RoleCaption);
                if (role != null)
                {
                    MessageBox.Show("Role already exists, you can change allowed actions for this Role");
                    NewRole = role;
                }
                else
                {
                    MessageBox.Show("New Role successfully created");
                    NewRole = new Role { Name = RoleCaption };
                }
                UsersService.SaveRole(NewRole);
                OnNewRoleCreated();
            }
        }
    }
}
