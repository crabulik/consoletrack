﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using ScrumTrackerWPF.Infrastructure;
using TaskTracker.Entities.General;
using TaskTracker.Entities.Infrastructure;
using TaskTracker.Services.DbServices;
using TaskTracker.Services.Enums;

namespace ScrumTrackerWPF.ViewModels
{
    public class EditUserViewModel: ViewModelBase
    {
        private DbUsersService UsersService { get; }
        private DbInfrastructureService InfrastructureService { get; }
        private Role _userRole;
        private Department _userDepartment;
        private DelegateCommand _editUserCommand;
        private User _userToEdit;
        private string _userEmail;
        private string _userName;
        private string _currentUserRole;
        private string _currentUserDepartment;

        public ObservableCollection<Department> Departments { get; set; }
        public ObservableCollection<Role> Roles { get; set; }
        
        public Role Role
        {
            get { return _userRole; }
            set
            {
                _userRole = value;
                NotifyPropertyChanged(nameof(Role));
            }
        }

        public Department Department
        {
            get { return _userDepartment; }
            set
            {
                _userDepartment = value;
                NotifyPropertyChanged(nameof(Department));
            }
        }

        public string UserEmail
        {
            get { return _userEmail; }
            set
            {
                _userEmail = value;
                NotifyErrorsChanged(nameof(UserEmail));
            }
        }

        public string UserName
        {
            get { return _userName; }
            set
            {
                _userName = value;
                NotifyErrorsChanged(nameof(UserName));
            }
        }

        public string CurrentUserRole
        {
            get { return _currentUserRole; }
            set
            {
                _currentUserRole = value;
                NotifyErrorsChanged(nameof(CurrentUserRole));
            }
        }

        public string CurrentUserDepartment
        {
            get { return _currentUserDepartment; }
            set
            {
                _currentUserDepartment = value;
                NotifyErrorsChanged(nameof(CurrentUserDepartment));
            }
        }

        public event EventHandler OnUserEdited;

        public delegate void EventHandler (object sender, EventArgs args);

        public EditUserViewModel(DbUsersService usersService, DbInfrastructureService infrastructureService)
        {
            UsersService = usersService;
            InfrastructureService = infrastructureService;
            FillComboBoxes();
        }

        private void FillComboBoxes()
        {
            var dptsList = InfrastructureService.GetDepartments();
            Departments = new ObservableCollection<Department>(dptsList);

            var rolesList = UsersService.GetRoles();
            Roles = new ObservableCollection<Role>(rolesList);
        }

        public DelegateCommand EditUserCommand
        {
            get { return _editUserCommand ?? (_editUserCommand = new DelegateCommand(EditUser)); }
        }

        private void EditUser()
        {
            if (Department != null)
            {
                _userToEdit.Department = Department;
            }
            if (Role != null)
            {
                _userToEdit.Role = Role;
            }
            var result = UsersService.SaveUser(_userToEdit);
            if (result == SaveResult.Saved)
            {
                OnUserEdited(this, new EventArgs());
                ResetInput();
            }
            else
            {
                ShowOperationFailedMessage();
            }
        }

        private void ResetInput()
        {
            Department = null;
            Role = null;
        }

        public void Init(int currentUserId)
        {
            _userToEdit = UsersService.GetUser(currentUserId);
            UserEmail = _userToEdit.Email;
            UserName = _userToEdit.RealName;
            CurrentUserRole = _userToEdit.Role.Name;
            CurrentUserDepartment = _userToEdit.Department.Name;
        }

        private void ShowOperationFailedMessage()
        {
            
        }
    }
}
