﻿using System;
using ScrumTrackerWPF.Infrastructure;
using ScrumTrackerWPF.Models;
using TaskTracker.Constants;
using TaskTracker.Entities.General;
using TaskTracker.Resources;
using TaskTracker.Services.DbServices;
using TaskTracker.Services.Services;

namespace ScrumTrackerWPF.ViewModels
{
    public class ChangePasswordViewModel: ViewModelBase
    {
        private bool _userInitialized = false;
        private string _viewCaption;
        private string _vmErrorMessage;
        private DelegateCommand _cancelCommand;
        private DelegateParamCommand _savePasswordCommand;

        private User _editedUser;
        private string _oldPassword;
        private string _newPassword;
        private string _confirmNewPassword;

        private AuthorizationService AuthorizationService { get; }

        public ChangePasswordViewModel(AuthorizationService authorizationService)
        {
            VmErrorMessage = String.Empty;
            AuthorizationService = authorizationService;
        }

        public void Init(User editUser, string viewCaption)
        {
            ViewCaption = viewCaption;
            _editedUser = editUser;
            _userInitialized = true;
        }     

        public string ViewCaption
        {
            get { return _viewCaption; }
            set
            {
                _viewCaption = value;
                NotifyPropertyChanged(nameof(ViewCaption));
            }
        }

        public bool IsVmError
        {
            get { return !string.IsNullOrEmpty(VmErrorMessage); }
        }

        public string VmErrorMessage
        {
            get { return _vmErrorMessage; }
            set
            {
                _vmErrorMessage = value;
                NotifyPropertyChanged(nameof(IsVmError));
                NotifyPropertyChanged(nameof(VmErrorMessage));
            }
        }

        public string OldPassword
        {
            get { return _oldPassword; }
            set
            {
                _oldPassword = value; 
                NotifyPropertyChanged(nameof(OldPassword));
            }
        }

        public string NewPassword
        {
            get { return _newPassword; }
            set
            {
                _newPassword = value;
                NotifyPropertyChanged(nameof(NewPassword));
            }
        }

        public string ConfirmNewPassword
        {
            get { return _confirmNewPassword; }
            set
            {
                _confirmNewPassword = value;
                NotifyPropertyChanged(nameof(ConfirmNewPassword));
            }
        }

        public DelegateParamCommand SavePasswordCommand
        {
            get { return _savePasswordCommand ?? (_savePasswordCommand = new DelegateParamCommand(SavePassword, CanSavePassword)); }
        }

        public DelegateCommand CancelCommand
        {
            get { return _cancelCommand ?? (_cancelCommand = new DelegateCommand(Cancel)); }
        }

        public event EventHandler ChangePasswordSuccess;

        public event EventHandler ChangePasswordCanceled;

        private bool CanSavePassword(object param)
        {
            if (!_userInitialized)
                return false;
            var data = (param as ChangePasswordData);
            if (data == null)
                return false;

            return true;
        }

        private void SavePassword(object param)
        {
            VmErrorMessage = string.Empty;
            var paswordsData = (param as ChangePasswordData);

            if ((paswordsData == null) || (paswordsData.OldPassword == null) || (paswordsData.NewPassword == null) ||
                (paswordsData.ConfirmNewPassword == null))
            {
                throw new ArgumentNullException("ChangePasswordData or one of passwords is null.");
            }

            if (PreliminaryChecks(paswordsData))
            {
                if (!AuthorizationService.IsPasswordMatch(_editedUser.UserId, paswordsData.OldPassword.Password))
                {
                    AddVmErrorMessge(ViewsResources.rs_ChangePasswordOldPassMismatchedMessage);
                    return;
                }
                AuthorizationService.SetNewPassword(_editedUser.UserId, paswordsData.OldPassword.Password,
                    paswordsData.NewPassword.Password);
                OnChangePasswordSuccess();
            }
        }
     
        private void Cancel()
        {
            OnChangePasswordCanceled();
        }      

        protected virtual void OnChangePasswordSuccess()
        {
            var receivers = ChangePasswordSuccess;
            receivers?.Invoke(this, EventArgs.Empty);
        }
      
        protected virtual void OnChangePasswordCanceled()
        {
            var receivers = ChangePasswordCanceled;
            receivers?.Invoke(this, EventArgs.Empty);
        }

        private bool PreliminaryChecks(ChangePasswordData passData)
        {
            var result = true;
            if (!CheckRulesForOldPassword(passData))
            {
                result = false;
            }
            if (!CheckRulesForNewPassword(passData))
            {
                result = false;
            }
            if (!CheckRulesForConfirmNewPassword(passData))
            {
                result = false;
            }
            return result;
        }

        private bool CheckRulesForOldPassword(ChangePasswordData passData)
        {
            var result = true;
            RemoveError(nameof(OldPassword));
            if (string.IsNullOrEmpty(passData.OldPassword.Password))
            {
                var message = ViewsResources.rs_ChangePasswordOldPasswordMustntBeEmpty;
                AddVmErrorMessge(message);
                AddError(nameof(OldPassword), message);
                result = false;
            }

            return result;
        }

        private bool CheckRulesForNewPassword(ChangePasswordData passData)
        {
            var result = true;
            RemoveError(nameof(NewPassword));
            if (string.IsNullOrEmpty(passData.NewPassword.Password))
            {
                var message = ViewsResources.rs_ChangePasswordNewPasswordMustntBeEmpty;
                AddVmErrorMessge(message);
                AddError(nameof(NewPassword), message);
                result = false;
            }
            else
            {
                if (passData.NewPassword.Password.Length < GeneralConstants.MinPasswordLength)
                {
                    var message = string.Format(ViewsResources.rs_ChangePasswordNewPasswordShouldBeLonger, GeneralConstants.MinPasswordLength);
                    AddVmErrorMessge(message);
                    AddError(nameof(NewPassword), message);
                    result = false;
                }
                if (string.Equals(passData.OldPassword.Password, passData.NewPassword.Password))
                {
                    var message = ViewsResources.rs_ChangePasswordNewPasswordShouldBeDifferent;
                    AddVmErrorMessge(message);
                    AddError(nameof(NewPassword), message);
                    result = false;                    
                }
            }

            return result;
        }

        private bool CheckRulesForConfirmNewPassword(ChangePasswordData passData)
        {
            var result = true;
            RemoveError(nameof(ConfirmNewPassword));
            if (string.IsNullOrEmpty(passData.ConfirmNewPassword.Password))
            {
                var message = ViewsResources.rs_ChangePasswordConfirmNewPasswordMustntBeEmpty;
                AddVmErrorMessge(message);
                AddError(nameof(ConfirmNewPassword), message);
                result = false;
            }
            else
            {
                if (!string.Equals(passData.ConfirmNewPassword.Password, passData.NewPassword.Password))
                {
                    var message = ViewsResources.rs_ChangePasswordConfirmNewPasswordShouldEquals;
                    AddVmErrorMessge(message);
                    AddError(nameof(ConfirmNewPassword), message);
                    result = false;
                }
            }

            return result;
        }

        private void AddVmErrorMessge(string message)
        {
            if (!string.IsNullOrEmpty(VmErrorMessage))
            {
                VmErrorMessage += Environment.NewLine;
            }
            VmErrorMessage += message;
        }
    }
}