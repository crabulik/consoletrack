﻿using TaskTracker.Services.DbServices;

namespace ScrumTrackerWPF.ViewModels
{
    public class EditProfileViewModel: ViewModelBase
    {
        private DbUsersService UsersService { get; }

        public EditProfileViewModel(DbUsersService usersService)
        {
            UsersService = usersService;
        }

        public void Init(int currentUserId)
        {
            
        }
    }
}