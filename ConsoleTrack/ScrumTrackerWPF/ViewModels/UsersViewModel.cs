﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Windows;
using ScrumTrackerWPF.CoreServices;
using ScrumTrackerWPF.Enums;
using ScrumTrackerWPF.Infrastructure;
using TaskTracker.Entities.General;
using TaskTracker.Services.DbServices;
using TaskTracker.Services.Services;

namespace ScrumTrackerWPF.ViewModels
{
    public sealed class UsersViewModel: ViewModelBase
    {
        private DbUsersService UsersService { get; }
        private AuthorizationService AuthorizationService { get; }
        private DelegateCommand _addUserCommand;
        private DelegateCommand _editUserCommand;

        private AddUserViewModel _addUserVm;
        private EditUserViewModel _editUserVm;

        public NavigationManager NavigMgr { get; set; }

        public User SelectedUser { get; set; }

        public ObservableCollection<User> Users { get; set; }

        public UsersViewModel(DbUsersService usersService, AddUserViewModel addUserVm, EditUserViewModel editUserVm, AuthorizationService athorizationService)
        {
            UsersService = usersService;
            _addUserVm = addUserVm;
            _editUserVm = editUserVm;
            AuthorizationService = athorizationService;
            FillDataGrid();
            _addUserVm.OnUserAdded += RepaintSelf;
            _editUserVm.OnUserEdited += RepaintSelf;
        }

        private void RepaintSelf(object sender, System.EventArgs args)
        {
            FillDataGrid();
            NavigMgr.ShowManageUsersForm(this);
        }

        public DelegateCommand AddUserCommand
        {
            get { return _addUserCommand ?? (_addUserCommand = new DelegateCommand(AddUser)); }
        }

        public DelegateCommand EditUserCommand
        {
            get { return _editUserCommand ?? (_editUserCommand = new DelegateCommand(EditUser)); }
        }

        private void EditUser()
        {
            if (SelectedUser != null)
            {
                _editUserVm.Init(SelectedUser.UserId);
                NavigMgr.ShowEditUserForm(_editUserVm);
            }
            else
            {
                MessageBox.Show("Select user");
            }
        }

        private void AddUser()
        {
            NavigMgr.ShowAddUserForm(_addUserVm);
        }

        private void FillDataGrid()
        {
            var usersList = UsersService.GetUsersForGrid();
            Users = new ObservableCollection<User>(usersList);
        }
    }
}