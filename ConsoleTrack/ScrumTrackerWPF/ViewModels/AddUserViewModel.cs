﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using ScrumTrackerWPF.Infrastructure;
using TaskTracker.Entities.General;
using TaskTracker.Entities.Infrastructure;
using TaskTracker.Resources;
using TaskTracker.Services.DbServices;
using TaskTracker.Services.Enums;
using TaskTracker.Services.Exceptions;
using TaskTracker.Services.Services;

namespace ScrumTrackerWPF.ViewModels
{
    public class AddUserViewModel : ViewModelBase
    {
        private DbUsersService UsersService { get; }
        private DbInfrastructureService InfrastructureService { get; }
        private AuthorizationService AuthorizationService { get; }

        private string _userName;
        private string _userEmail;
        private Role _userRole;
        private Department _userDepartment;

        private string _deptAndRoleError;

        private DelegateCommand _addUserCommand;

        public ObservableCollection<Department> Departments { get; set; }
        public ObservableCollection<Role> Roles { get; set; }

        public delegate void EventHandler(object sender, EventArgs args);
        public event EventHandler OnUserAdded = delegate { };

        public AddUserViewModel(DbUsersService usersService, DbInfrastructureService infrastructureService, AuthorizationService athorizationService)
        {
            UsersService = usersService;
            AuthorizationService = athorizationService;
            InfrastructureService = infrastructureService;
            FillComboBoxes();
            DepartmentAndRoleError = string.Empty;
        }

        public string Name
        {
            get { return _userName; }
            set
            {
                _userName = value;
                NotifyPropertyChanged(nameof(Name));
            }
        }

        public string Email
        {
            get { return _userEmail; }
            set
            {
                _userEmail = value;
                NotifyPropertyChanged(nameof(Email));
            }
        }

        public Role Role
        {
            get { return _userRole; }
            set
            {
                _userRole = value;
                NotifyPropertyChanged(nameof(Role));
            }
        }

        public Department Department
        {
            get { return _userDepartment; }
            set
            {
                _userDepartment = value;
                NotifyPropertyChanged(nameof(Department));
            }
        }

        public string DepartmentAndRoleError
        {
            get { return _deptAndRoleError; }
            set
            {
                _deptAndRoleError = value;
                NotifyPropertyChanged(nameof(IsDepartmentOrRoleNotSelected));
                NotifyPropertyChanged(nameof(DepartmentAndRoleError));
            }
        }

        public bool IsDepartmentOrRoleNotSelected
        {
            get { return !string.IsNullOrEmpty(DepartmentAndRoleError); }
        }

        public DelegateCommand AddUserCommand
        {
            get { return _addUserCommand ?? (_addUserCommand = new DelegateCommand(AddUser)); }
        }

        private void AddUser()
        {
            DepartmentAndRoleError = string.Empty;
            var rulesRespected = CheckAddingRulesRespected();
            if (rulesRespected)
            {
                try
                {
                    var newUser = InitUser();
                    if (UsersService.GetUser(newUser.Email) == null)
                    {
                        AuthorizationService.CreateNewUser(newUser);
                        OnUserAdded(this, new EventArgs());
                        ResetInput();
                    }
                    else
                    {
                        ShowOperationFailedMessage();
                    }
                }
                catch (AuthorizationServiceException)
                {
                    ShowOperationFailedMessage();
                }

            }
        }

        private User InitUser()
        {
            return new User()
            {
                Avatar = UsersService.GetDefaultAvatar(),
                Department = Department,
                Email = Email,
                IsNeedPassCorrection = true,
                RealName = Name,
                NickName = Name,
                PassHash = new byte[0],
                PassSalt = new byte[0],
                Role = Role
            };
        }

        private bool CheckAddingRulesRespected()
        {
            var result = true;
            RemoveError(nameof(Name));
            RemoveError(nameof(Email));

            if (string.IsNullOrWhiteSpace(Name))
            {
                AddError(nameof(Name), ViewsResources.rs_AddUserViewNameMustBeSetError);
                result = false;
            }
            else if (string.IsNullOrWhiteSpace(Email))
            {
                AddError(nameof(Email), ViewsResources.rs_AddUserViewEmailMustBeSetError);
                result = false;
            }
            else if (Department == null || Role == null)
            {
                DepartmentAndRoleError = ViewsResources.rs_AddUserViewDepartemntAndRoleMustBeSetError;
                result = false;
            }
            return result;
        }

        private void ResetInput()
        {
            Name = string.Empty;
            Email = string.Empty;
            Department = null;
            Role = null;
        }

        private void FillComboBoxes()
        {
            var dptsList = InfrastructureService.GetDepartments();
            Departments = new ObservableCollection<Department>(dptsList);

            var rolesList = UsersService.GetRoles();
            Roles = new ObservableCollection<Role>(rolesList);
        }

        private void ShowOperationFailedMessage()
        {
            MessageBox.Show("User with such Email already exists: " + Email);
        }
    }
}
