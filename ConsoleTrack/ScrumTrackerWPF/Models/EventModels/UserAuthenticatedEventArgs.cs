﻿using System;

namespace ScrumTrackerWPF.Models.EventModels
{
    public class UserAuthenticatedEventArgs: EventArgs
    {
        public int UserId { get; set; }

        public UserAuthenticatedEventArgs(int userId)
        {
            UserId = userId;
        }
    }
}