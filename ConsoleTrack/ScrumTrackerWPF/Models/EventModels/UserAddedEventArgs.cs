﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTracker.Services.Enums;

namespace ScrumTrackerWPF.Models.EventModels
{
    public class UserAddedEventArgs: EventArgs
    {
        public SaveResult OperationResult { get; set; }

        public UserAddedEventArgs(SaveResult result)
        {
            OperationResult = result;
        }
    }
}
