﻿using System.Windows.Controls;

namespace ScrumTrackerWPF.Models
{
    public class ChangePasswordData
    {
        public PasswordBox OldPassword;
        public PasswordBox NewPassword;
        public PasswordBox ConfirmNewPassword;
    }
}