﻿namespace TaskTracker.Constants
{
    public static class TablesNames
    {
        public const string Avatars = "Avatars";
        public const string Comments = "Comments";
        public const string Departments = "Departments";
        public const string Positions = "Positions";
        public const string Projects = "Projects";
        public const string RoleActions = "RoleActions";
        public const string Roles = "Roles";
        public const string Sprints = "Sprints";
        public const string SprintBacklogs = "SprintBacklogs";
        public const string Tasks = "Tasks";
        public const string TaskAttachments = "TaskAttachments";
        public const string TaskComments = "TaskComments";
        public const string TaskStatuses = "TaskStatuses";
        public const string TaskStatusHistoryItems = "TaskStatusHistoryItems";
        public const string TaskTimeTrackerItems = "TaskTimeTrackerItems";
        public const string TaskTypes = "TaskTypes";
        public const string Teams = "Teams";
        public const string TeamMembers = "TeamMembers";
        public const string Users = "Users";
    }
}