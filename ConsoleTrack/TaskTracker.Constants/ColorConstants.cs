﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskTracker.Constants
{
   public static class ColorConstants
   {
       public const string GradientFirst = "#FEFFFF";
       public const string GradientStop = "LightBlue";
       public const string Background = "#4FD5D6";
       public const string LabelError = "#FFF50D0D";
       public const string GradientFirstLabelError = "#FFF3BDBD";
       public const string GradientStopLabelError = "#FFF5ABAF";
       public const string ForegroundStopLabelError = "#FFF50D0D";
       public const string ForegroundButton = "White";
       
       
   }
}
