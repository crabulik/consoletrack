﻿namespace TaskTracker.Constants
{
    public static class SeedsConstants
    {
        public const string SuperAdminRoleName = "SuperAdmin";
        public const string DefaultDepartmentName = "DefaultDepartment";
        public const string DefaultAdminName = "admin";
    }
}