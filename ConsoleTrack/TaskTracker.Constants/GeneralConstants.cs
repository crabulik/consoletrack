﻿namespace TaskTracker.Constants
{
    public static class GeneralConstants
    {
        public const int MinPasswordLength = 5;
    }
}