﻿namespace TaskTracker.Constants
{
    public static class DbConstants
    {
        public const int AutoIncrementIdSeed = 1000;

        public const int ShortString = 30;
        public const int NameString = 50;
        public const int LongString = 256;
        public const int ExtraLongString = 1024;


        public const int LoginFail = -1;
    }
}