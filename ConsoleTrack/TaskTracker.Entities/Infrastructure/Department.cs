﻿using TaskTracker.Constants;
using TaskTracker.DAL.DTO;

namespace TaskTracker.Entities.Infrastructure
{
    public class Department
    {
        public int DepartmentId { get; set; }

        public string Name { get; set; }

        public bool IsRecordActive { get; set; }

        public Department()
        {
            IsRecordActive = true;
            DepartmentId = EntitiesConstants.IdIsNotSet;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}