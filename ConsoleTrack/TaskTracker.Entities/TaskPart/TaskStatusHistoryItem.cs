﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTracker.Constants;
using TaskTracker.Entities.General;

namespace TaskTracker.Entities.TaskPart
{
    public class TaskStatusHistoryItem
    {
        public TaskStatusHistoryItem()
        {
            TaskStatusHistoryItemId = EntitiesConstants.IdIsNotSet;
            IsRecordActive = true;
        }

        public int TaskStatusHistoryItemId { get; set; }

        public User AssignedTo { get; set; }

        public int TaskId { get; set; }

        public TaskStatus TaskStatus { get; set; }

        public DateTime CreationDate { get; set; }

        public bool IsRecordActive { get; set; }
    }
}
