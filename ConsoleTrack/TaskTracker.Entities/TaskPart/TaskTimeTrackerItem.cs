﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTracker.Constants;
using TaskTracker.Entities.General;

namespace TaskTracker.Entities.TaskPart
{
    public class TaskTimeTrackerItem
    {
        public TaskTimeTrackerItem()
        {
            TaskTimeTrackerItemId = EntitiesConstants.IdIsNotSet;
            IsRecordActive = true;
        }

        public int TaskTimeTrackerItemId { get; set; }

        public User Executor { get; set; }

        public int TaskId { get; set; }
        
        public string Description { get; set; }

        public DateTime CreationDate { get; set; }

        public int SpentTime { get; set; }
        
        public bool IsRecordActive { get; set; }
    }
}
