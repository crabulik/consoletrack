﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTracker.Constants;
using TaskTracker.Entities.General;

namespace TaskTracker.Entities.TaskPart
{
    public class TaskAttachment
    {
        public TaskAttachment()
        {
            TaskAttachmentsId = EntitiesConstants.IdIsNotSet;
            IsRecordActive = true;
        }

        public int TaskAttachmentsId { get; set; }

        public User Author { get; set; }

        public int TaskId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
        
        public DateTime AttachDate { get; set; }

        public byte[] Data { get; set; }

        public string MIME { get; set; }

        public bool IsRecordActive { get; set; }
    }
}
