﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTracker.Constants;

namespace TaskTracker.Entities.TaskPart
{
    public class TaskStatus
    {
        public TaskStatus()
        {
            TaskStatusId = EntitiesConstants.IdIsNotSet;
            IsRecordActive = true;
        }

        public int TaskStatusId { get; set; }

        public string Name { get; set; }
        
        public bool IsRecordActive { get; set; }
    }
}
