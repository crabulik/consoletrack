﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTracker.Constants;
using TaskTracker.Entities.General;

namespace TaskTracker.Entities.TaskPart
{
    public class TaskComment
    {
        public TaskComment()
        {
            TaskCommentId = EntitiesConstants.IdIsNotSet;
            IsRecordActive = true;
        }

        public int TaskCommentId { get; set; }

        public User Author { get; set; }

        public int TaskId { get; set; }

        public string Text { get; set; }

        public DateTime WhenPost { get; set; }
        
        public bool IsRecordActive { get; set; }
    }
}
