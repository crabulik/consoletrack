﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTracker.Constants;
using TaskTracker.Entities.General;

namespace TaskTracker.Entities.TaskPart
{
    public class Task
    {
        public Task()
        {
            TaskId = EntitiesConstants.IdIsNotSet;
            IsRecordActive = true;

            StatusHistory = new List<TaskStatusHistoryItem>();
            Attachments = new List<TaskAttachment>();
            Comments = new List<TaskComment>();
            TaskTimeTracker = new List<TaskTimeTrackerItem>();
        }

        public int TaskId { get; set; }

        public int ProjectId { get; set; }

        public User Author { get; set; }

        public TaskType Type { get; set; }

        public TaskStatus Status { get; set; }

        public User AssignedTo { get; set; }

        public int MasterTaskId { get; set; }

        public string Caption { get; set; }

        public string Acronym { get; set; }

        public string Description { get; set; }

        public int EstimateTime { get; set; }

        public int SpentTime { get; set; }

        public DateTime CreationTime { get; set; }
        
        public List<TaskStatusHistoryItem> StatusHistory { get; }

        public List<TaskAttachment> Attachments { get; }

        public List<TaskComment> Comments { get; }

        public List<TaskTimeTrackerItem> TaskTimeTracker { get; }

        public bool IsRecordActive { get; set; }
    }
}
