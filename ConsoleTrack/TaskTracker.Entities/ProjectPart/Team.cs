﻿using TaskTracker.Constants;

namespace TaskTracker.Entities.ProjectPart
{
    public class Team
    {
        public int TeamId { get; set; }

        public string Name { get; set; }

        public bool IsRecordActive { get; set; }

        public Team()
        {
            TeamId = EntitiesConstants.IdIsNotSet;
            IsRecordActive = false;
        }
    }
}
