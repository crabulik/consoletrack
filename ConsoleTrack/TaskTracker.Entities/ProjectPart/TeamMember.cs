﻿using TaskTracker.Constants;
using TaskTracker.Entities.General;

namespace TaskTracker.Entities.ProjectPart
{
    public class TeamMember
    {
        public int TeamMemberId { get; set; }

        public User User { get; set; }

        public Team Team { get; set; }

        public Position Position { get; set; }

        public TeamMember()
        {
            TeamMemberId = EntitiesConstants.IdIsNotSet;
        }
    }
}
