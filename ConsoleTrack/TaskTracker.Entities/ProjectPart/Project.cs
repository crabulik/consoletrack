﻿using System.Collections.Generic;
using TaskTracker.Constants;
using TaskTracker.DAL.DTO.Enums;
using TaskTracker.Entities.TaskPart;

namespace TaskTracker.Entities.ProjectPart
{
    public class Project
    {
        public int  ProjectId { get; set; }

        public string Acronym { get; set; }

        public string Description { get; set; }

        public string Name { get; set; }

        public Team Team { get; set; }

        public ProjectStatusesEnum Status { get; set; }

        public bool IsRecordActive { get; set; }

        public int EstimateTime { get; set; }

        public List<Sprint> Sprints { get; set; }

        public List<Task> Backlog { get; set; }

        public Project()
        {
            ProjectId = EntitiesConstants.IdIsNotSet;
            Sprints = new List<Sprint>();
            IsRecordActive = false;
        }
    }
}
