﻿using System;
using System.Collections.Generic;
using TaskTracker.Constants;
using TaskTracker.DAL.DTO.Enums;
using TaskTracker.Entities.TaskPart;

namespace TaskTracker.Entities.ProjectPart
{
    public class Sprint
    {
        public int SprintId { get; set; }

        public Project Project { get; set; }

        public List<Task> SprintBacklog { get; set; }

        public string Caption { get; set; }

        public string Description { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public SprintStatusesEnum SprintStatus { get; set; }

        public bool IsRecordActive { get; set; }

        public Sprint()
        {
            SprintId = EntitiesConstants.IdIsNotSet;
            SprintBacklog = new List<Task>();
            IsRecordActive = false;
        }
    }
}
