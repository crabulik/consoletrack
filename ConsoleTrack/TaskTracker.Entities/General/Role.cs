﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTracker.Constants;

namespace TaskTracker.Entities.General
{
    public class Role
    {
        public Role()
        {
            AllowedProjectActions = new List<RoleAction>();
            IsRecordActive = true;
            RoleId = EntitiesConstants.IdIsNotSet;
        }

        public int RoleId { get; set; }

        public string Name { get; set; }

        public List<RoleAction> AllowedProjectActions { get; }

        public bool IsSuperAdmin { get; set; }

        public bool IsRecordActive { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
