﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTracker.Constants;
using TaskTracker.Entities.Infrastructure;

namespace TaskTracker.Entities.General
{
    public class User
    {
        public int UserId { get; set; }

        public string Email { get; set; }

        public Role Role { get; set; }

        public Avatar Avatar { get; set; }

        public Department Department { get; set; }

        public string RealName { get; set; }

        public string NickName { get; set; }

        public byte[] PassHash { get; set; }

        public byte[] PassSalt { get; set; }

        public bool IsNeedPassCorrection { get; set; }

        public bool IsRecordActive { get; set; }

        public User()
        {
            IsRecordActive = true;
            IsNeedPassCorrection = false;
            UserId = EntitiesConstants.IdIsNotSet;
        }
    }
}
