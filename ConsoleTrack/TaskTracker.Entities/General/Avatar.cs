﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTracker.Constants;

namespace TaskTracker.Entities.General
{
    public class Avatar
    {
        public int AvatarId { get; set; }

        public byte[] Data { get; set; }

        public Avatar()
        {
            AvatarId = EntitiesConstants.IdIsNotSet;
        }
    }
}
