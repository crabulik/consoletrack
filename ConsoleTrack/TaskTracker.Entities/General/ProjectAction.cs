﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTracker.Constants;
using TaskTracker.DAL.DTO.Enums;

namespace TaskTracker.Entities.General
{
    public class ProjectAction
    {
        public ProjectActionsEnum Value { get; set; }

        public string Description { get; set; }

        public bool IsRecordActive { get; set; }

        public ProjectAction()
        {
            IsRecordActive = true;
            Value = ProjectActionsEnum.DisableAll;
        }
    }
}
