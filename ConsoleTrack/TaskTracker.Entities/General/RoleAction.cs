﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTracker.DAL.DTO.Enums;

namespace TaskTracker.Entities.General
{
    public class RoleAction
    {
        public ProjectActionsEnum ProjectActionValue { get; set; }

        public bool IsSet { get; set; }

        public RoleAction()
        {
            ProjectActionValue = ProjectActionsEnum.DisableAll;
            IsSet = false;
        }
    }
}
