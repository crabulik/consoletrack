﻿using TaskTracker.Constants;

namespace TaskTracker.Entities.General
{
    public class Position
    {
        public int PositionId { get; set; }

        public string Title { get; set; }

        public bool IsRecordActive { get; set; }

        public Position()
        {
            IsRecordActive = true;
            PositionId = EntitiesConstants.IdIsNotSet;
            
        }
    }
}
