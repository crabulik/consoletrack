﻿namespace TaskTracker.DAL.DTO
{
    public static class TrackerConstants
    {
        public const int IdIsNotSet = -1;
        public const int AutoIncrementIdSeed = 1000;
        public const string ExceptionMassageOperationNotSupported = "Operation is not supported for this table!";
    }
}