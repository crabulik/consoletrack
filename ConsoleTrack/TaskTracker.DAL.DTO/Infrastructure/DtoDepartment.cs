﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TaskTracker.Constants;

namespace TaskTracker.DAL.DTO.Infrastructure
{
    [Table(TablesNames.Departments)]
    public class DtoDepartment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DepartmentId { get; set; }

        [MaxLength(DbConstants.LongString), Required]
        public string Name { get; set; }

        [Required]
        public bool IsRecordActive { get; set; }

        public DtoDepartment()
        {
            IsRecordActive = true;
            DepartmentId = EntitiesConstants.IdIsNotSet;
        }

        public override string ToString()
        {
            return $"DepartmentId: {DepartmentId}, Name: {Name}, IsRecordActive: {IsRecordActive}";
        }

        public void Copy(DtoDepartment original)
        {
            if (original != null)
            {
                Name = original.Name;
                IsRecordActive = original.IsRecordActive;
            }
        }
    }
}