﻿namespace TaskTracker.DAL.DTO.Enums
{
    public enum ProjectActionsEnum
    {
        // ***** presumably only regular user
        DisableAll = 0, 
        AllowEnterTracker = 1,

        AllowAddTaskComment = 10,
        AllowAddTaskAttachment = 20,
        AllowAddTaskTimeTrackerItem = 30,


        // ***** presumably only PM
        AllowCreateProject = 100,
        AllowCreateTask = 110,
        AllowChangeTaskStatus = 120,
        AllowChangeTaskType = 130,
        AllowEditTaskInfo = 140,

        AllowCreateTeam = 500,
        AllowAddUserToTeam = 510,
        AllowRemoveUserFromTeam = 520,
        AllowChangeMemberPosition = 530,


        // ***** presumably only admin
        AllowAddUser = 2000,
        AllowRemoveUser = 2010,
        AllowChangeUserPassword = 2020,
        AllowEditUserPublicInfo = 2030,
        AllowEditUserPrivateInfo = 2040,

        AllowAddRole = 2100,
        AllowRemoveRole = 2110,
        AllowEditRoleDetails = 2120,
        AllowAddPosition = 2130,
        AllowRemovePosition = 2140,
        AllowEditPositionDetails = 2150,
        AllowAddDepartment = 2160,
        AllowRemoveDepartment = 2170,
        AllowEditDepartmentDetails = 2180,
    }
}
