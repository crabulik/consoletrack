﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskTracker.DAL.DTO.Enums
{
    public enum SprintStatusesEnum
    {
        NotSet = 0,

        Created = 10,
        InProgress = 20,

        Cancelled = 40,
        Suspended = 50,

        Finished = 100
    }
}
