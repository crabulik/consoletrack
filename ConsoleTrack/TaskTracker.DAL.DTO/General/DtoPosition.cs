﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TaskTracker.Constants;

namespace TaskTracker.DAL.DTO.General
{
    [Table(TablesNames.Positions)]
    public class DtoPosition
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PositionId { get; set; }

        [MaxLength(DbConstants.NameString), Required]
        public string Title { get; set; }

        [Required]
        public bool IsRecordActive { get; set; }
        public DtoPosition()
        {
            IsRecordActive = true;
            PositionId = EntitiesConstants.IdIsNotSet;
        }

        public void Copy(DtoPosition original)
        {
            if (original != null)
            {
                Title = original.Title;
                IsRecordActive = original.IsRecordActive;
            }
        }
    }
}
