﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TaskTracker.Constants;

namespace TaskTracker.DAL.DTO.General
{
    [Table(TablesNames.Avatars)]
    public class DtoAvatar
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AvatarId { get; set; }

        [Required]
        public byte[] Data { get; set; }

        [Required]
        public bool IsDefault { get; set; }
        

        public DtoAvatar()
        {
            AvatarId = EntitiesConstants.IdIsNotSet;
        }
    }
}
