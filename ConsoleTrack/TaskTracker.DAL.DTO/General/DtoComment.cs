﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TaskTracker.Constants;

namespace TaskTracker.DAL.DTO.General
{
    [Table(TablesNames.Comments)]
    public class DtoComment
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CommentId { get; set; }

        [Required]
        public int AuthorUserId { get; set; }

        [ForeignKey(nameof(AuthorUserId))]
        public DtoUser Author { get; set; }

        [MaxLength(DbConstants.ExtraLongString), Required]
        public string Text { get; set; }

        [Required]
        public DateTime WhenPosted { get; set; }

        [Required]
        public bool IsRecordActive { get; set; }

    }
}
