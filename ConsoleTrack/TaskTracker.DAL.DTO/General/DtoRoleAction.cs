﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TaskTracker.Constants;
using TaskTracker.DAL.DTO.Enums;

namespace TaskTracker.DAL.DTO.General
{
    [Table(TablesNames.RoleActions)]
    public class DtoRoleAction
    {
        public DtoRoleAction()
        {
            RoleId = EntitiesConstants.IdIsNotSet;
            ProjectActionValue = (int) ProjectActionsEnum.DisableAll;
        }

        [Key]
        [Column(Order = 1)]
        public int RoleId { get; set; }

        [ForeignKey(nameof(RoleId))]
        public DtoRole Role { get; set; }

        [Key]
        [Column(Order = 2)]
        public int ProjectActionValue { get; set; }

        [Required]
        public bool IsSet { get; set; }

        public override string ToString()
        {
            return $"RoleId: {RoleId}, ProjectActionValue: {ProjectActionValue} ({Enum.GetName(typeof(ProjectActionsEnum),ProjectActionValue)}), IsSet = {IsSet}";
        }
    }
}
