﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TaskTracker.Constants;
using TaskTracker.DAL.DTO.Infrastructure;

namespace TaskTracker.DAL.DTO.General
{
    [Table(TablesNames.Users)]
    public class DtoUser
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }

        [Required]
        public int RoleId { get; set; }

        [ForeignKey(nameof(RoleId))]
        public DtoRole Role { get; set; }

        [Required]
        public int AvatarId { get; set; }

        [ForeignKey(nameof(AvatarId))]
        public DtoAvatar Avatar { get; set; }

        [Required]
        public int DepartmentId { get; set; }

        [ForeignKey(nameof(DepartmentId))]
        public DtoDepartment Department { get; set; }

        [MaxLength(DbConstants.LongString), Required]
        public string RealName { get; set; }

        [MaxLength(DbConstants.NameString), Required]
        public string NickName { get; set; }

        [MaxLength(DbConstants.LongString), Required, Index(IsUnique = true)]
        public string Email { get; set; }

        [MaxLength(1024), Required]
        public byte[] PassHash { get; set; }

        [MaxLength(1024), Required]
        public byte[] PassSalt { get; set; }

        [Required]
        public bool IsNeedPassCorrection { get; set; }

        [Required]
        public bool IsRecordActive { get; set; }

        public void Copy(DtoUser original)
        {
            if (original != null)
            {
                //Role = original.Role;
                //Department = original.Department;
                RealName = original.RealName;
                NickName = original.NickName;
                Email = original.Email;
                PassHash = original.PassHash;
                PassSalt = original.PassSalt;
                IsNeedPassCorrection = original.IsNeedPassCorrection;
                IsRecordActive = original.IsRecordActive;
            }
        }

    }
}
