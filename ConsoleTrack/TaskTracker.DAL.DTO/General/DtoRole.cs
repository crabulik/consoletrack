﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TaskTracker.Constants;

namespace TaskTracker.DAL.DTO.General
{
    [Table(TablesNames.Roles)]
    public class DtoRole
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RoleId { get; set; }

        [MaxLength(DbConstants.NameString), Required]
        public string Name { get; set; }

        [Required]
        public bool IsSuperAdmin { get; set; }

        [Required]
        public bool IsRecordActive { get; set; }

        public void Copy(DtoRole original)
        {
            if (original != null)
            {
                
            }
        }

        public override string ToString()
        {
            return $"RoleId: {RoleId}, Name: {Name}, IsSuperAdmin = {IsSuperAdmin}, IsRecordActive: {IsRecordActive}";
        }

    }
}
