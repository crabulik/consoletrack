﻿using TaskTracker.Constants;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TaskTracker.DAL.DTO.General;

namespace TaskTracker.DAL.DTO.Tasks
{
    [Table(TablesNames.TaskComments)]
    public class DtoTaskComment
    {
        public DtoTaskComment()

        {
            TaskId = EntitiesConstants.IdIsNotSet;
            CommentId = EntitiesConstants.IdIsNotSet;
        }

        [Key]
        [Column(Order = 1)]
        public int TaskId { get; set; }

        [ForeignKey(nameof(TaskId))]
        public DtoTask Task { get; set; }

        [Key]
        [Column(Order = 2)]
        public int CommentId { get; set; }

        [ForeignKey(nameof(CommentId))]
        public DtoComment Comment { get; set; }

        public override string ToString()
        {
            return $"TaskId: {TaskId}, CommentId: {CommentId}";
        }
    }
}
