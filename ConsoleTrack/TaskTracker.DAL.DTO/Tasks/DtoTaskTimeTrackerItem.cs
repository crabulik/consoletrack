﻿using System;
using TaskTracker.Constants;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TaskTracker.DAL.DTO.General;

namespace TaskTracker.DAL.DTO.Tasks
{
    [Table(TablesNames.TaskTimeTrackerItems)]
    public class DtoTaskTimeTrackerItem
    {
        public DtoTaskTimeTrackerItem()

        {
            IsRecordActive = true;
            TaskTimeTrackerItemId = EntitiesConstants.IdIsNotSet;
        }
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TaskTimeTrackerItemId { get; set; }

        [Required]
        public int ExecutorUserId { get; set; }

        [ForeignKey(nameof(ExecutorUserId))]
        public DtoUser Executor { get; set; }

        [Required]
        public int TaskId { get; set; }

        [ForeignKey(nameof(TaskId))]
        public DtoTask Task { get; set; }

        [MaxLength(DbConstants.ExtraLongString), Required]
        public string Description { get; set; }

        [Required]
        public DateTime CreationDate { get; set; }

        [Required]
        public int SpentTime { get; set; }

        [Required]
        public bool IsRecordActive { get; set; }

        public override string ToString()
        {
            return $"TaskTimeTrackerItemId: {TaskTimeTrackerItemId}, ExecutorUserId: {ExecutorUserId}, TaskId: {TaskId},\n" +
                   $"Description: {Description},\n" +
                   $"CreationDate: {CreationDate}, SpentTime: {SpentTime}";
        }
    }
}
