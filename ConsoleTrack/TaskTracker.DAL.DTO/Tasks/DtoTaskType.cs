﻿using TaskTracker.Constants;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TaskTracker.DAL.DTO.Tasks
{
    [Table(TablesNames.TaskTypes)]
    public class DtoTaskType
    {

        public DtoTaskType()
        {
            IsRecordActive = true;
            TaskTypeId = EntitiesConstants.IdIsNotSet;
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TaskTypeId { get; set; }

        [MaxLength(DbConstants.NameString), Required]
        public string Name { get; set; }

        [Required]
        public bool IsRecordActive { get; set; }

        public override string ToString()
        {
            return $"TaskTypeId: {TaskTypeId}, Name: {Name}";
        }
    }
}
