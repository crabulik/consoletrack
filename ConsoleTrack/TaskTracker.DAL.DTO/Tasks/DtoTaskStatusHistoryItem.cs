﻿using System;
using TaskTracker.Constants;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TaskTracker.DAL.DTO.General;

namespace TaskTracker.DAL.DTO.Tasks
{
    [Table(TablesNames.TaskStatusHistoryItems)]
    public class DtoTaskStatusHistoryItem
    {
        public DtoTaskStatusHistoryItem()

        {
            IsRecordActive = true;
            TaskStatusHistoryItemId = EntitiesConstants.IdIsNotSet;
        }
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TaskStatusHistoryItemId { get; set; }

        [Required]
        public int AssignedToUserId { get; set; }

        [ForeignKey(nameof(AssignedToUserId))]
        public DtoUser AssignedTo { get; set; }

        [Required]
        public int TaskId { get; set; }

        [ForeignKey(nameof(TaskId))]
        public DtoTask Task { get; set; }

        [Required]
        public int TaskStatusId { get; set; }

        [ForeignKey(nameof(TaskStatusId))]
        public DtoTaskStatus TaskStatus { get; set; }

        [Required]
        public DateTime CreationDate { get; set; }

        [Required]
        public bool IsRecordActive { get; set; }

        public override string ToString()
        {
            return $"TaskStatusHistoryItemId: {TaskStatusHistoryItemId}, AssignedToUserId: {AssignedToUserId}, " +
                   $"TaskId: {TaskId}, TaskStatusId: {TaskStatusId}, CreationDate: {CreationDate}";
        }
    }
}
