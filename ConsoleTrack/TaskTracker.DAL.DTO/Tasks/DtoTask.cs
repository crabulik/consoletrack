﻿using System;
using TaskTracker.Constants;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TaskTracker.DAL.DTO.General;
using TaskTracker.DAL.DTO.Projects;

namespace TaskTracker.DAL.DTO.Tasks
{
    [Table(TablesNames.Tasks)]
    public class DtoTask
    {
        public DtoTask()
        {
            IsRecordAcitve = true;
            TaskId = EntitiesConstants.IdIsNotSet;
        }
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]	
        public int TaskId { get; set; }

        [Required]
        public int ProjectId { get; set; }

        [ForeignKey(nameof(ProjectId))]
        public DtoProject Project { get; set; }

        [Required]
        public int AuthorUserId { get; set; }

        [ForeignKey(nameof(AuthorUserId))]
        public DtoUser Author { get; set; }

        [Required]
        public int TaskTypeId { get; set; }

        [ForeignKey(nameof(TaskTypeId))]
        public DtoTaskType TaskType { get; set; }

        [Required]
        public int TaskStatusId { get; set; }

        [ForeignKey(nameof(TaskStatusId))]
        public DtoTaskStatus TaskStatus { get; set; }

        [Required]
        public int AssignedToUserId { get; set; }

        [ForeignKey(nameof(AssignedToUserId))]
        public DtoUser AssignedTo { get; set; }

        [Required]
        public int MasterTaskId { get; set; }

        [ForeignKey(nameof(MasterTaskId))]
        public DtoTask Master { get; set; }

        [MaxLength(DbConstants.LongString), Required]
        public string Caption { get; set; }

        [MaxLength(DbConstants.ShortString), Required, Index(IsUnique = true)]
        public string Acronym { get; set; }

        [MaxLength]
        public string Description { get; set; }

        [Required]
        public int EstimateTime { get; set; }

        [Required]
        public int SpentTime { get; set; }

        [Required]
        public DateTime CreateTime { get; set; }

        [Required]
        public bool IsRecordAcitve { get; set; }

        public ICollection<DtoTaskStatusHistoryItem> TaskStatusHistory { get; set; }

        public ICollection<DtoTaskAttachment> Attachments { get; set; }

        public ICollection<DtoTaskComment> Comments { get; set; }

        public ICollection<DtoTaskTimeTrackerItem> TaskTimeTracker { get; set; }

        public override string ToString()
        {
            return $"TaskId: {TaskId}, ProjectId: {ProjectId}, AuthorUserId: {AuthorUserId},\n" +
                   $"TaskTypeId: {TaskTypeId}, TaskStatusId: {TaskStatusId}, AssignedToUser: {AssignedToUserId}, MasterTaskId: {MasterTaskId},\n" +
                   $"Caption: {Caption}, Acronym: {Acronym}\n," +
                   $" Description: {Description},\n" +
                   $"CreationTime: {CreateTime}, SpentTime: {SpentTime}, EstimateTime: {EstimateTime}";
        }
    }
}
