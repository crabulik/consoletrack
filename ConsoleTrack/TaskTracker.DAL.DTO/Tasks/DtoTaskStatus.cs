﻿using TaskTracker.Constants;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TaskTracker.DAL.DTO.Tasks
{
    [Table(TablesNames.TaskStatuses)]
    public class DtoTaskStatus
    {
        public DtoTaskStatus()
        {
            IsRecordActive = true;
            TaskStatusId = EntitiesConstants.IdIsNotSet;
        }
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TaskStatusId { get; set; }

        [MaxLength(DbConstants.NameString), Required]
        public string Name { get; set; }

        [Required]
        public bool IsRecordActive { get; set; }

        public override string ToString()
        {
            return $"TaskStatusId: {TaskStatusId}, Name: {Name}";
        }
    }
}
