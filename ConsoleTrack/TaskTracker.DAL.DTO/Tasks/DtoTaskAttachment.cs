﻿using System;
using TaskTracker.Constants;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TaskTracker.DAL.DTO.General;

namespace TaskTracker.DAL.DTO.Tasks
{
    [Table(TablesNames.TaskAttachments)]
    public class DtoTaskAttachment
    {
        public DtoTaskAttachment()
        {
            IsRecordActive = true;
            TaskAttachmentId = EntitiesConstants.IdIsNotSet;
        }
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TaskAttachmentId { get; set; }

        [Required]
        public int AuthorUserId { get; set; }

        [ForeignKey(nameof(AuthorUserId))]
        public DtoUser Author { get; set; }

        [Required]
        public int TaskId { get; set; }

        [ForeignKey(nameof(TaskId))]
        public DtoTask Task { get; set; }

        [MaxLength(DbConstants.LongString), Required]
        public string Name { get; set; }

        [MaxLength(DbConstants.ExtraLongString)]
        public string Description { get; set; }

        [Required]
        public DateTime AttachDate { get; set; }

        [Required]
        public byte[] Data { get; set; }

        [MaxLength(DbConstants.LongString), Required]
        public string Mime { get; set; }

        [Required]
        public bool IsRecordActive { get; set; }

        public override string ToString()
        {
            return $"TaskAttachmentId: {TaskAttachmentId}, AuthorUserId: {AuthorUserId}, TaskId: {TaskId}, Name: {Name}, \n" +
                   $"Description: {Description},\n" +
                   $"AttachDate: {AttachDate}, Data: {Data}, MIME: {Mime}";
        }
    }
}
