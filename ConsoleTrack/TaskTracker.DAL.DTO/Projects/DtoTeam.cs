﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TaskTracker.Constants;

namespace TaskTracker.DAL.DTO.Projects
{
    [Table(TablesNames.Teams)]
    public class DtoTeam
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TeamId { get; set; }

        [MaxLength(DbConstants.NameString), Required]
        public string Name { get; set; }

        [Required]
        public bool IsRecordActive { get; set; }

        public ICollection<DtoTeamMember> Members { get; set; }

        public override string ToString()
        {
            return $"TeamId: {TeamId}, Name: {Name}, IsRecordActive: {IsRecordActive}";
        }
    }
}
