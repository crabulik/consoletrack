﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TaskTracker.Constants;
using TaskTracker.DAL.DTO.General;

namespace TaskTracker.DAL.DTO.Projects
{
    [Table(TablesNames.TeamMembers)]
    public class DtoTeamMember
    {
        [Key]
        [Column(Order = 1)]
        public int UserId { get; set; }

        [ForeignKey(nameof(UserId))]
        public DtoUser User { get; set; }

        [Key]
        [Column(Order = 2)]
        public int TeamId { get; set; }

        [ForeignKey(nameof(TeamId))]
        public DtoTeam Team { get; set; }

        [Required]
        public int PositionId { get; set; }

        [ForeignKey(nameof(PositionId))]
        public DtoPosition Position { get; set; }

        public override string ToString()
        {
            return $"UserId: {UserId}, TeamId: {TeamId}, PositionId: {PositionId}";
        }
    }
}
