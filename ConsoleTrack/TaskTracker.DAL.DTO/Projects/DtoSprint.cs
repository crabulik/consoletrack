﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TaskTracker.Constants;
using TaskTracker.DAL.DTO.Tasks;

namespace TaskTracker.DAL.DTO.Projects
{
    [Table(TablesNames.Sprints)]
    public class DtoSprint
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SprintId { get; set; }

        [Required]
        public int ProjectId { get; set; }

        [ForeignKey(nameof(ProjectId))]
        public DtoProject Project { get; set; }

        [MaxLength(DbConstants.LongString), Required]
        public string Caption { get; set; }

        [MaxLength(DbConstants.ExtraLongString)]
        public string Description { get; set; }

        [Required]
        public DateTime StartDate { get; set; }

        [Required]
        public DateTime EndDate { get; set; }

        [Required]
        public int SprintStatus { get; set; }

        [Required]
        public bool IsRecordActive { get; set; }

        public ICollection<DtoSprintBacklogItem> Backlog { get; set; }

        public override string ToString()
        {
            
            return $"SprintId: {SprintId}, ProjectId: {ProjectId}, Caption: {Caption}, Description: {Description}," +
                   $"StartDate: {StartDate}, EndDate: {EndDate}, SprintStatus: {SprintStatus}, IsRecordActive: {IsRecordActive}";
        }
    }
}
