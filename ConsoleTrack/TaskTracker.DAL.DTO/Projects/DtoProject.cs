﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TaskTracker.Constants;
using TaskTracker.DAL.DTO.General;
using TaskTracker.DAL.DTO.Tasks;

namespace TaskTracker.DAL.DTO.Projects
{
    [Table(TablesNames.Projects)]
    public class DtoProject
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProjectId { get; set; }

        [Required]
        public int TeamId { get; set; }

        [ForeignKey(nameof(TeamId))]
        public DtoTeam Team { get; set; }

        [MaxLength(DbConstants.LongString), Required]
        public string Name { get; set; }

        [MaxLength(DbConstants.ShortString), Required, Index(IsUnique = true)]
        public string Acronym { get; set; }

        [MaxLength(DbConstants.ExtraLongString)]
        public string Description { get; set; }

        [Required]
        public int ProjectStatus { get; set; }

        [Required]
        public int EstimateTime { get; set; }

        [Required]
        public bool IsRecordActive { get; set; }

        public ICollection<DtoTask> Tasks { get; set; }

        public ICollection<DtoSprint> Sprints { get; set; }

        public override string ToString()
        {
            return $"ProjectId: {ProjectId}, TeamId: {TeamId}, Name: {Name}, Acronym: {Acronym}, " +
                   $"Description: {Description}, ProjectStatus: {ProjectStatus}, EstimateTime: {EstimateTime}, " +
                   $"IsRecordActive: {IsRecordActive}";
        }

    }
}
