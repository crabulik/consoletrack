﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TaskTracker.Constants;
using TaskTracker.DAL.DTO.Tasks;

namespace TaskTracker.DAL.DTO.Projects
{
    [Table(TablesNames.SprintBacklogs)]
    public class DtoSprintBacklogItem
    {
        [Key]
        [Column(Order = 1)]
        public int SprintId { get; set; }

        [ForeignKey(nameof(SprintId))]
        public DtoSprint Sprint { get; set; }

        [Key]
        [Column(Order = 2)]
        public int TaskId { get; set; }

        [ForeignKey(nameof(TaskId))]
        public DtoTask Task { get; set; }

        public override string ToString()
        {
            return $"SprintId: {SprintId}, TaskId: {TaskId}";
        }
    }
}
