namespace TaskTracker.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Avatars",
                c => new
                    {
                        AvatarId = c.Int(nullable: false, identity: true),
                        Data = c.Binary(nullable: false),
                    })
                .PrimaryKey(t => t.AvatarId);
            
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        CommentId = c.Int(nullable: false, identity: true),
                        AuthorUserId = c.Int(nullable: false),
                        Text = c.String(nullable: false, maxLength: 1024),
                        WhenPosted = c.DateTime(nullable: false),
                        IsRecordActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.CommentId)
                .ForeignKey("dbo.Users", t => t.AuthorUserId)
                .Index(t => t.AuthorUserId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        RoleId = c.Int(nullable: false),
                        AvatarId = c.Int(nullable: false),
                        DepartmentId = c.Int(nullable: false),
                        RealName = c.String(nullable: false, maxLength: 256),
                        NickName = c.String(nullable: false, maxLength: 50),
                        Email = c.String(nullable: false, maxLength: 256),
                        PassHash = c.Binary(nullable: false, maxLength: 1024),
                        PassSalt = c.Binary(nullable: false, maxLength: 1024),
                        IsNeedPassCorrection = c.Boolean(nullable: false),
                        IsRecordActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.Avatars", t => t.AvatarId)
                .ForeignKey("dbo.Departments", t => t.DepartmentId)
                .ForeignKey("dbo.Roles", t => t.RoleId)
                .Index(t => t.RoleId)
                .Index(t => t.AvatarId)
                .Index(t => t.DepartmentId)
                .Index(t => t.Email, unique: true);
            
            CreateTable(
                "dbo.Departments",
                c => new
                    {
                        DepartmentId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 256),
                        IsRecordActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.DepartmentId);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        RoleId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        IsSuperAdmin = c.Boolean(nullable: false),
                        IsRecordActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.RoleId);
            
            CreateTable(
                "dbo.Positions",
                c => new
                    {
                        PositionId = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 50),
                        IsRecordActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.PositionId);
            
            CreateTable(
                "dbo.Projects",
                c => new
                    {
                        ProjectId = c.Int(nullable: false, identity: true),
                        TeamId = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 256),
                        Acronym = c.String(nullable: false, maxLength: 30),
                        Description = c.String(maxLength: 1024),
                        ProjectStatus = c.Int(nullable: false),
                        EstimateTime = c.Int(nullable: false),
                        IsRecordActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ProjectId)
                .ForeignKey("dbo.Teams", t => t.TeamId)
                .Index(t => t.TeamId)
                .Index(t => t.Acronym, unique: true);
            
            CreateTable(
                "dbo.Sprints",
                c => new
                    {
                        SprintId = c.Int(nullable: false, identity: true),
                        ProjectId = c.Int(nullable: false),
                        Caption = c.String(nullable: false, maxLength: 256),
                        Description = c.String(maxLength: 1024),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        SprintStatus = c.Int(nullable: false),
                        IsRecordActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.SprintId)
                .ForeignKey("dbo.Projects", t => t.ProjectId)
                .Index(t => t.ProjectId);
            
            CreateTable(
                "dbo.SprintBacklogs",
                c => new
                    {
                        SprintId = c.Int(nullable: false),
                        TaskId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.SprintId, t.TaskId })
                .ForeignKey("dbo.Sprints", t => t.SprintId)
                .ForeignKey("dbo.Tasks", t => t.TaskId)
                .Index(t => t.SprintId)
                .Index(t => t.TaskId);
            
            CreateTable(
                "dbo.Tasks",
                c => new
                    {
                        TaskId = c.Int(nullable: false, identity: true),
                        ProjectId = c.Int(nullable: false),
                        AuthorUserId = c.Int(nullable: false),
                        TaskTypeId = c.Int(nullable: false),
                        TaskStatusId = c.Int(nullable: false),
                        AssignedToUserId = c.Int(nullable: false),
                        MasterTaskId = c.Int(nullable: false),
                        Caption = c.String(nullable: false, maxLength: 256),
                        Acronym = c.String(nullable: false, maxLength: 30),
                        Description = c.String(),
                        EstimateTime = c.Int(nullable: false),
                        SpentTime = c.Int(nullable: false),
                        CreateTime = c.DateTime(nullable: false),
                        IsRecordAcitve = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.TaskId)
                .ForeignKey("dbo.Users", t => t.AssignedToUserId)
                .ForeignKey("dbo.Users", t => t.AuthorUserId)
                .ForeignKey("dbo.Tasks", t => t.MasterTaskId)
                .ForeignKey("dbo.Projects", t => t.ProjectId)
                .ForeignKey("dbo.TaskStatuses", t => t.TaskStatusId)
                .ForeignKey("dbo.TaskTypes", t => t.TaskTypeId)
                .Index(t => t.ProjectId)
                .Index(t => t.AuthorUserId)
                .Index(t => t.TaskTypeId)
                .Index(t => t.TaskStatusId)
                .Index(t => t.AssignedToUserId)
                .Index(t => t.MasterTaskId)
                .Index(t => t.Acronym, unique: true);
            
            CreateTable(
                "dbo.TaskAttachments",
                c => new
                    {
                        TaskAttachmentId = c.Int(nullable: false, identity: true),
                        AuthorUserId = c.Int(nullable: false),
                        TaskId = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 256),
                        Description = c.String(maxLength: 1024),
                        AttachDate = c.DateTime(nullable: false),
                        Data = c.Binary(nullable: false),
                        Mime = c.String(nullable: false, maxLength: 256),
                        IsRecordActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.TaskAttachmentId)
                .ForeignKey("dbo.Users", t => t.AuthorUserId)
                .ForeignKey("dbo.Tasks", t => t.TaskId)
                .Index(t => t.AuthorUserId)
                .Index(t => t.TaskId);
            
            CreateTable(
                "dbo.TaskComments",
                c => new
                    {
                        TaskId = c.Int(nullable: false),
                        CommentId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.TaskId, t.CommentId })
                .ForeignKey("dbo.Comments", t => t.CommentId)
                .ForeignKey("dbo.Tasks", t => t.TaskId)
                .Index(t => t.TaskId)
                .Index(t => t.CommentId);
            
            CreateTable(
                "dbo.TaskStatuses",
                c => new
                    {
                        TaskStatusId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        IsRecordActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.TaskStatusId);
            
            CreateTable(
                "dbo.TaskStatusHistoryItems",
                c => new
                    {
                        TaskStatusHistoryItemId = c.Int(nullable: false, identity: true),
                        AssignedToUserId = c.Int(nullable: false),
                        TaskId = c.Int(nullable: false),
                        TaskStatusId = c.Int(nullable: false),
                        CreationDate = c.DateTime(nullable: false),
                        IsRecordActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.TaskStatusHistoryItemId)
                .ForeignKey("dbo.Users", t => t.AssignedToUserId)
                .ForeignKey("dbo.Tasks", t => t.TaskId)
                .ForeignKey("dbo.TaskStatuses", t => t.TaskStatusId)
                .Index(t => t.AssignedToUserId)
                .Index(t => t.TaskId)
                .Index(t => t.TaskStatusId);
            
            CreateTable(
                "dbo.TaskTimeTrackerItems",
                c => new
                    {
                        TaskTimeTrackerItemId = c.Int(nullable: false, identity: true),
                        ExecutorUserId = c.Int(nullable: false),
                        TaskId = c.Int(nullable: false),
                        Description = c.String(nullable: false, maxLength: 1024),
                        CreationDate = c.DateTime(nullable: false),
                        SpentTime = c.Int(nullable: false),
                        IsRecordActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.TaskTimeTrackerItemId)
                .ForeignKey("dbo.Users", t => t.ExecutorUserId)
                .ForeignKey("dbo.Tasks", t => t.TaskId)
                .Index(t => t.ExecutorUserId)
                .Index(t => t.TaskId);
            
            CreateTable(
                "dbo.TaskTypes",
                c => new
                    {
                        TaskTypeId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        IsRecordActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.TaskTypeId);
            
            CreateTable(
                "dbo.Teams",
                c => new
                    {
                        TeamId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        IsRecordActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.TeamId);
            
            CreateTable(
                "dbo.TeamMembers",
                c => new
                    {
                        UserId = c.Int(nullable: false),
                        TeamId = c.Int(nullable: false),
                        PositionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.TeamId })
                .ForeignKey("dbo.Positions", t => t.PositionId)
                .ForeignKey("dbo.Teams", t => t.TeamId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.TeamId)
                .Index(t => t.PositionId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Projects", "TeamId", "dbo.Teams");
            DropForeignKey("dbo.TeamMembers", "UserId", "dbo.Users");
            DropForeignKey("dbo.TeamMembers", "TeamId", "dbo.Teams");
            DropForeignKey("dbo.TeamMembers", "PositionId", "dbo.Positions");
            DropForeignKey("dbo.Sprints", "ProjectId", "dbo.Projects");
            DropForeignKey("dbo.SprintBacklogs", "TaskId", "dbo.Tasks");
            DropForeignKey("dbo.Tasks", "TaskTypeId", "dbo.TaskTypes");
            DropForeignKey("dbo.TaskTimeTrackerItems", "TaskId", "dbo.Tasks");
            DropForeignKey("dbo.TaskTimeTrackerItems", "ExecutorUserId", "dbo.Users");
            DropForeignKey("dbo.TaskStatusHistoryItems", "TaskStatusId", "dbo.TaskStatuses");
            DropForeignKey("dbo.TaskStatusHistoryItems", "TaskId", "dbo.Tasks");
            DropForeignKey("dbo.TaskStatusHistoryItems", "AssignedToUserId", "dbo.Users");
            DropForeignKey("dbo.Tasks", "TaskStatusId", "dbo.TaskStatuses");
            DropForeignKey("dbo.Tasks", "ProjectId", "dbo.Projects");
            DropForeignKey("dbo.Tasks", "MasterTaskId", "dbo.Tasks");
            DropForeignKey("dbo.TaskComments", "TaskId", "dbo.Tasks");
            DropForeignKey("dbo.TaskComments", "CommentId", "dbo.Comments");
            DropForeignKey("dbo.Tasks", "AuthorUserId", "dbo.Users");
            DropForeignKey("dbo.TaskAttachments", "TaskId", "dbo.Tasks");
            DropForeignKey("dbo.TaskAttachments", "AuthorUserId", "dbo.Users");
            DropForeignKey("dbo.Tasks", "AssignedToUserId", "dbo.Users");
            DropForeignKey("dbo.SprintBacklogs", "SprintId", "dbo.Sprints");
            DropForeignKey("dbo.Comments", "AuthorUserId", "dbo.Users");
            DropForeignKey("dbo.Users", "RoleId", "dbo.Roles");
            DropForeignKey("dbo.Users", "DepartmentId", "dbo.Departments");
            DropForeignKey("dbo.Users", "AvatarId", "dbo.Avatars");
            DropIndex("dbo.TeamMembers", new[] { "PositionId" });
            DropIndex("dbo.TeamMembers", new[] { "TeamId" });
            DropIndex("dbo.TeamMembers", new[] { "UserId" });
            DropIndex("dbo.TaskTimeTrackerItems", new[] { "TaskId" });
            DropIndex("dbo.TaskTimeTrackerItems", new[] { "ExecutorUserId" });
            DropIndex("dbo.TaskStatusHistoryItems", new[] { "TaskStatusId" });
            DropIndex("dbo.TaskStatusHistoryItems", new[] { "TaskId" });
            DropIndex("dbo.TaskStatusHistoryItems", new[] { "AssignedToUserId" });
            DropIndex("dbo.TaskComments", new[] { "CommentId" });
            DropIndex("dbo.TaskComments", new[] { "TaskId" });
            DropIndex("dbo.TaskAttachments", new[] { "TaskId" });
            DropIndex("dbo.TaskAttachments", new[] { "AuthorUserId" });
            DropIndex("dbo.Tasks", new[] { "Acronym" });
            DropIndex("dbo.Tasks", new[] { "MasterTaskId" });
            DropIndex("dbo.Tasks", new[] { "AssignedToUserId" });
            DropIndex("dbo.Tasks", new[] { "TaskStatusId" });
            DropIndex("dbo.Tasks", new[] { "TaskTypeId" });
            DropIndex("dbo.Tasks", new[] { "AuthorUserId" });
            DropIndex("dbo.Tasks", new[] { "ProjectId" });
            DropIndex("dbo.SprintBacklogs", new[] { "TaskId" });
            DropIndex("dbo.SprintBacklogs", new[] { "SprintId" });
            DropIndex("dbo.Sprints", new[] { "ProjectId" });
            DropIndex("dbo.Projects", new[] { "Acronym" });
            DropIndex("dbo.Projects", new[] { "TeamId" });
            DropIndex("dbo.Users", new[] { "Email" });
            DropIndex("dbo.Users", new[] { "DepartmentId" });
            DropIndex("dbo.Users", new[] { "AvatarId" });
            DropIndex("dbo.Users", new[] { "RoleId" });
            DropIndex("dbo.Comments", new[] { "AuthorUserId" });
            DropTable("dbo.TeamMembers");
            DropTable("dbo.Teams");
            DropTable("dbo.TaskTypes");
            DropTable("dbo.TaskTimeTrackerItems");
            DropTable("dbo.TaskStatusHistoryItems");
            DropTable("dbo.TaskStatuses");
            DropTable("dbo.TaskComments");
            DropTable("dbo.TaskAttachments");
            DropTable("dbo.Tasks");
            DropTable("dbo.SprintBacklogs");
            DropTable("dbo.Sprints");
            DropTable("dbo.Projects");
            DropTable("dbo.Positions");
            DropTable("dbo.Roles");
            DropTable("dbo.Departments");
            DropTable("dbo.Users");
            DropTable("dbo.Comments");
            DropTable("dbo.Avatars");
        }
    }
}
