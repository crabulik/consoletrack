namespace TaskTracker.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRoleActions : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.RoleActions",
                c => new
                    {
                        RoleId = c.Int(nullable: false),
                        ProjectActionValue = c.Int(nullable: false),
                        IsSet = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => new { t.RoleId, t.ProjectActionValue })
                .ForeignKey("dbo.Roles", t => t.RoleId)
                .Index(t => t.RoleId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RoleActions", "RoleId", "dbo.Roles");
            DropIndex("dbo.RoleActions", new[] { "RoleId" });
            DropTable("dbo.RoleActions");
        }
    }
}
