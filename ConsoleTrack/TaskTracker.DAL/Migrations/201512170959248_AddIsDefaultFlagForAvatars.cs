namespace TaskTracker.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddIsDefaultFlagForAvatars : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Avatars", "IsDefault", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Avatars", "IsDefault");
        }
    }
}
