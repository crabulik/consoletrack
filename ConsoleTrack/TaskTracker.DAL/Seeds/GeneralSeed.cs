﻿using System;
using System.Data.Entity.Migrations;
using System.Linq;
using TaskTracker.Constants;
using TaskTracker.DAL.Contexts;
using TaskTracker.DAL.DTO.General;
using TaskTracker.DAL.DTO.Infrastructure;

namespace TaskTracker.DAL.Seeds
{
    internal sealed class GeneralSeed
    {
        public void RoleSeed(TaskTrackerContext context)
        {
            if (context != null)
            {
                if (!context.Roles.Any(p => p.IsSuperAdmin))
                {
                    context.Roles.Add(
                        new DtoRole
                        {
                            IsSuperAdmin = true,
                            IsRecordActive = true,
                            Name = SeedsConstants.SuperAdminRoleName
                        });
                }
            }
        }

        public void DepartmentSeed(TaskTrackerContext context)
        {
            if (context != null)
            {
                if (!context.Departments.Any(p => string.Equals(p.Name, SeedsConstants.DefaultDepartmentName)))
                {
                    context.Departments.Add(
                        new DtoDepartment
                        {
                            IsRecordActive = true,
                            Name = SeedsConstants.DefaultDepartmentName
                        });
                }
            }
        }

        public void AvatarSeed(TaskTrackerContext context)
        {
            if (context != null)
            {
                if (!context.Avatars.Any(p => p.IsDefault))
                {
                    context.Avatars.Add(
                        new DtoAvatar
                        {
                            IsDefault = true,
                            Data = new byte[0] 
                        });
                }
            }
        }

        public void UserSeed(TaskTrackerContext context)
        {
            if (context != null)
            {
                
                if (!context.Users.Any(p => string.Equals(p.NickName, SeedsConstants.DefaultAdminName)))
                {
                    var superadminRole = context.Roles.FirstOrDefault(p => p.IsSuperAdmin);
                    var defaultDepartment =
                        context.Departments.FirstOrDefault(
                            p =>
                                string.Equals(p.Name, SeedsConstants.DefaultDepartmentName));
                    var defaultAvatar = context.Avatars.FirstOrDefault(p => p.IsDefault);

                    context.Users.Add(
                        new DtoUser
                        {
                            Role = superadminRole,
                            Avatar = defaultAvatar,
                            Department = defaultDepartment,
                            Email = SeedsConstants.DefaultAdminName,
                            IsNeedPassCorrection = true,
                            IsRecordActive = true,
                            NickName = SeedsConstants.DefaultAdminName,
                            RealName = SeedsConstants.DefaultAdminName,
                            PassHash = new byte[0],
                            PassSalt = new byte[0]
                        });
                }
            }
        }
    }
}