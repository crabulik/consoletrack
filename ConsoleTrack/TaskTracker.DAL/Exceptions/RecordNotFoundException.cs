﻿using System;

namespace TaskTracker.DAL.Exceptions
{
    public class RecordNotFoundException: Exception
    {
        public int RecordId { get; set; }

        public string TableName { get; set; }

        public RecordNotFoundException(int recordId, string tableName): base ()
        {
            RecordId = recordId;
            TableName = tableName;
        }
    }
}