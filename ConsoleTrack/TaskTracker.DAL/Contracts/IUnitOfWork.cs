﻿using System;
using TaskTracker.DAL.Contracts.Repositories.General;
using TaskTracker.DAL.Contracts.Repositories.Infrastructure;

namespace TaskTracker.DAL.Contracts
{
    public interface IUnitOfWork: IDisposable
    {
        IDepartmentsRepository Departments { get; }
        IAvatarsRepository Avatars { get; }
        IRolesRepository Roles { get; }
        IRoleActionsRepository RoleAction { get; }
        IUsersRepository Users { get; }
        IPositionRepository Positions { get; }

        void Save();
    }
}