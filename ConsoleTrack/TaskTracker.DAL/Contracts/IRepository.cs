﻿using System.Linq;

namespace TaskTracker.DAL.Contracts
{
    public interface IRepository<T>
    {
        T Get(int id);

        void Create(T item);

        void Update(T item);

        void Delete(int id);

        IQueryable<T> GetQueryable();
    }
}