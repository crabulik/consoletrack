﻿using TaskTracker.DAL.DTO.Projects;

namespace TaskTracker.DAL.Contracts.Repositories.Projects
{
    interface ISprintBacklogsRepository : IGenericRepository<DtoSprintBacklogItem>
    {
        DtoSprintBacklogItem GetSingle(int sprintId, int taskId);
    }
}
