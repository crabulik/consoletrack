﻿using TaskTracker.DAL.DTO.Projects;

namespace TaskTracker.DAL.Contracts.Repositories.Projects
{
    interface ITeamsRepository : IGenericRepository<DtoTeam>
    {
        DtoTeam GetSingle(int id);
    }
}
