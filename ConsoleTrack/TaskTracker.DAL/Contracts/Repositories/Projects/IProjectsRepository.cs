﻿using TaskTracker.DAL.DTO.Projects;

namespace TaskTracker.DAL.Contracts.Repositories.Projects
{
    public interface IProjectsRepository : IGenericRepository<DtoProject>
    {
        DtoProject GetSingle(int id);
    }
}
