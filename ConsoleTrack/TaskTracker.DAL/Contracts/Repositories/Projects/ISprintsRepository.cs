﻿using TaskTracker.DAL.DTO.Projects;

namespace TaskTracker.DAL.Contracts.Repositories.Projects
{
    public interface ISprintsRepository : IGenericRepository<DtoSprint>
    {
        DtoSprint GetSingle(int id);
    }
}
