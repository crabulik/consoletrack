﻿using TaskTracker.DAL.DTO.Projects;

namespace TaskTracker.DAL.Contracts.Repositories.Projects
{
    interface ITeamMembersRepository : IGenericRepository<DtoTeamMember>
    {
        DtoTeamMember GetSingle(int teamId, int userId);
    }
}
