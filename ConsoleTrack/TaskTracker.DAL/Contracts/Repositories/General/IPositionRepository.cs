﻿using TaskTracker.DAL.DTO.General;

namespace TaskTracker.DAL.Contracts
{
    public interface IPositionRepository : IGenericRepository<DtoPosition>
    {
        DtoPosition GetSingle(int id);
    }
}