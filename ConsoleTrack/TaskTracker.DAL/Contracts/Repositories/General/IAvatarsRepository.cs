﻿using TaskTracker.DAL.DTO.General;

namespace TaskTracker.DAL.Contracts.Repositories.General
{
    public interface IAvatarsRepository: IGenericRepository<DtoAvatar>
    {
        DtoAvatar GetSingle(int id);
    }
}