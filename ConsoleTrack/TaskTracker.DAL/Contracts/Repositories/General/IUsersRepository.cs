﻿using TaskTracker.DAL.DTO.General;

namespace TaskTracker.DAL.Contracts.Repositories.General
{
    public interface IUsersRepository : IGenericRepository<DtoUser>
    {
        DtoUser GetSingle(int id);
    }
}