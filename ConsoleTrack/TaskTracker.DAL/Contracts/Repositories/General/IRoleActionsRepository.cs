﻿using TaskTracker.DAL.DTO.General;

namespace TaskTracker.DAL.Contracts.Repositories.General
{
    public interface IRoleActionsRepository : IGenericRepository<DtoRoleAction>
    {
        DtoRoleAction GetSingle(int roleId, int projectActionValue);
    }
}