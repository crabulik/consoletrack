﻿using TaskTracker.DAL.DTO.General;

namespace TaskTracker.DAL.Contracts.Repositories.General
{
    public interface IRolesRepository: IGenericRepository<DtoRole>
    {
        DtoRole GetSingle(int id);
    }
}