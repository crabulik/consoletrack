﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTracker.DAL.DTO.Tasks;
using TaskTracker.DAL.Repositories;

namespace TaskTracker.DAL.Contracts.Repositories.Tasks
{
    public interface ITaskCommentsRepository : IGenericRepository<DtoTaskComment>
    {
        DtoTaskComment GetSingle(int taskId, int commentId);
    }
}
