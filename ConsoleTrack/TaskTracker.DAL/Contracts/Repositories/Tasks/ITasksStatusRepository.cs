﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTracker.DAL.DTO.Tasks;

namespace TaskTracker.DAL.Contracts.Repositories.Tasks
{
    public interface ITaskStatusesRepository : IGenericRepository<DtoTaskStatus>
    {
        DtoTaskStatus GetSingle(int id);
    }
}
