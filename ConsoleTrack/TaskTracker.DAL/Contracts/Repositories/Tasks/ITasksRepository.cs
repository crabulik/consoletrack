﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTracker.DAL.DTO.Tasks;

namespace TaskTracker.DAL.Contracts.Repositories.Tasks
{
    public interface ITasksRepository : IGenericRepository<DtoTask>
    {
        DtoTask GetSingle(int id);
    }
}
