﻿using TaskTracker.DAL.DTO.Infrastructure;

namespace TaskTracker.DAL.Contracts.Repositories.Infrastructure
{
    public interface IDepartmentsRepository: IGenericRepository<DtoDepartment>
    {
        DtoDepartment GetSingle(int id);
    }
}