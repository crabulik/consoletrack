﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTracker.DAL.DTO.General;

namespace TaskTracker.DAL.Contracts.Repositories.Infrastructure
{
    public interface IPositionRepository : IGenericRepository<DtoPosition>
    {
        DtoPosition GetSingle(int id);
    }
}
