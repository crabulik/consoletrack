﻿namespace TaskTracker.DAL.Constants
{
    public class TaskStatusesFields
    {
        public const string TaskStatusId = "TaskStatusId";
        public const string Name = "Name";
        public const string IsRecordActive = "IsRecordActive";
        
    }
}
