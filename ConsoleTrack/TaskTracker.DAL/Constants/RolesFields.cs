﻿namespace TaskTracker.DAL.Constants
{
    public class RolesFields
    {
        public const string RoleId = "RoleId";
        public const string Name = "Name";
        public const string IsSuperAdmin = "IsSuperAdmin";
        public const string IsRecordActive = "IsRecordActive";

    }
}
