﻿namespace TaskTracker.DAL.Constants
{
    public class SprintsFields
    {
        public const string SprintId = "SprintId";
        public const string ProjectId = "ProjectId";
        public const string Caption = "Caption";
        public const string Description = "Description";
        public const string StartDate = "StartDate";
        public const string EndDate = "EndDate";
        public const string SprintStatus = "SprintStatus";
        public const string IsRecordActive = "IsRecordActive";
        
    }
}
