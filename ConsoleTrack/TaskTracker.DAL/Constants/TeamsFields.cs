﻿namespace TaskTracker.DAL.Constants
{
    public class TeamsFields
    {
        public const string TeamId = "TeamId";
        public const string Name = "Name";
        public const string IsRecordActive = "IsRecordActive";

    }
}
