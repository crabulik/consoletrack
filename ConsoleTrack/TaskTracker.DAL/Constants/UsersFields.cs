﻿namespace TaskTracker.DAL.Constants
{
    public class UsersFields
    {
        public const string UserId = "UserId";
        public const string RoleId = "RoleId";
        public const string AvatarId = "AvatarId";
        public const string DepartmentId = "DepartmentId";
        public const string RealName = "RealName";
        public const string NickName = "NickName";
        public const string Email = "Email";
        public const string PassHash = "PassHash";
        public const string PassSalt = "PassSalt";
        public const string IsNeedPassCorrection = "IsNeedPassCorrection";
        public const string IsRecordActive = "IsRecordActive";

    }
}
