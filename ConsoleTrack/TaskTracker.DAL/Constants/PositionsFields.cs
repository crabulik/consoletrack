﻿namespace TaskTracker.DAL.Constants
{
    public class PositionsFields
    {
        public const string PositionId = "PositionId";
        public const string Title = "Title";
        public const string IsRecordActive = "IsRecordActive";

    }
}
