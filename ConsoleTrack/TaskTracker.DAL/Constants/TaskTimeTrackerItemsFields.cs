﻿namespace TaskTracker.DAL.Constants
{
    public class TaskTimeTrackerItemsFields
    {
        public const string TaskTimeTrackerItemId = "TaskTimeTrackerItemId";
        public const string ExecutorUserId = "ExecutorUserId";
        public const string TaskId = "TaskId";
        public const string Description = "Description";
        public const string CreationDate = "CreationDate";
        public const string SpentTime = "SpentTime";
        public const string IsRecordActive = "IsRecordActive";

    }
}
