﻿namespace TaskTracker.DAL.Constants
{
    public class CommentsFields
    {
        public const string CommentId = "CommentId";
        public const string AuthorUserId = "AuthorUserId";
        public const string Text = "Text";
        public const string WhenPosted = "WhenPosted";
        public const string IsRecordActive = "IsRecordActive";

    }
}
