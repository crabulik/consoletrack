﻿namespace TaskTracker.DAL.Constants
{
    public class TaskCommentsFields
    {
        public const string TaskId = "TaskId";
        public const string CommentId = "CommentId";

    }
}
