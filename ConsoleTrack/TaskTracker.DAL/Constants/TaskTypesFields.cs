﻿namespace TaskTracker.DAL.Constants
{
    public class TaskTypesFields
    {
        public const string TaskTypeId = "TaskTypeId";
        public const string Name = "Name";
        public const string IsRecordActive = "IsRecordActive";
        
    }
}
