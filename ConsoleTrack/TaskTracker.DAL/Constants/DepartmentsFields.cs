﻿namespace TaskTracker.DAL.Constants
{
    public class DepartmentsFields
    {
        public const string DepartmentId = "DepartmentId";
        public const string Name = "Name";
        public const string IsRecordActive = "IsRecordActive";

    }
}