﻿namespace TaskTracker.DAL.Constants
{
    public class TaskAttachmentsFields
    {
        public const string TaskAttachmentId = "TaskAttachmentId";
        public const string AuthorUserId = "AuthorUserId";
        public const string TaskId = "TaskId";
        public const string Name = "Name";
        public const string Description = "Description";
        public const string AttachDate = "AttachDate";
        public const string Data = "Data";
        public const string MIME = "MIME";
        public const string IsRecordActive = "IsRecordActive";

    }
}
