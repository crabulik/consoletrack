﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskTracker.DAL.Constants
{
    public class RoleActionsFields
    {
        public const string RoleId = "RoleId";
        public const string ProjectActionId = "ProjectActionId";
    }
}
