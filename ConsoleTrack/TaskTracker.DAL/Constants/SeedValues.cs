﻿namespace TaskTracker.DAL.Constants
{
    public static class SeedValues
    {
        public const int GeneralDepartmentId = 0;
        public const string GeneralDepartmentName = "Main Department";

        public const int SuperAdminAvatarId = 0;

        public const int SuperAdminRoleId = 0;
        public const string SuperAdminRoleName = "SuperAdmin";

        public const int UserAdminId = 0;
        public const string UserAdminName = "Admin";
    }
}