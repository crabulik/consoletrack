﻿namespace TaskTracker.DAL.Constants
{
    public class TasksFields
    {
        public const string TaskId = "TaskId";
        public const string ProjectId = "ProjectId";
        public const string AuthorUserId = "AuthorUserId";
        public const string TaskTypeId = "TaskTypeId";
        public const string TaskStatusId = "TaskStatusId";
        public const string AssignedToUserId = "AssignedToUserId";
        public const string MasterTaskId = "MasterTaskId";
        public const string Caption = "Caption";
        public const string Acronym = "Acronym";
        public const string Description = "Description";
        public const string EstimateTime = "EstimateTime";
        public const string SpentTime = "SpentTime";
        public const string CreateTime = "CreateTime";
        public const string IsRecordActive = "IsRecordActive";

    }
}
