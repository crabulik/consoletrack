﻿namespace TaskTracker.DAL.Constants
{
    public class ProjectsFields
    {
        public const string ProjectId = "ProjectId";
        public const string TeamId = "TeamId";
        public const string Name = "Name";
        public const string Acronym = "Acronym";
        public const string Description = "Description";
        public const string ProjectStatus = "ProjectStatus";
        public const string EstimateTime = "EstimateTime";
        public const string IsRecordActive = "IsRecordActive";

    }
}
