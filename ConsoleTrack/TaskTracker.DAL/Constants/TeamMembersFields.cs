﻿namespace TaskTracker.DAL.Constants
{
    public class TeamMembersFields
    {
        public const string UserId = "UserId";
        public const string TeamId = "TeamId";
        public const string PositionId = "PositionId";

    }
}
