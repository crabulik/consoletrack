﻿namespace TaskTracker.DAL.Constants
{
    public class TaskStatusHistoryItemsFields
    {
        public const string TaskStatusHistoryItemId = "TaskStatusHistoryItemId";
        public const string AssignedToUserId = "AssigendToUserId";
        public const string TaskId = "TaskId";
        public const string TaskStatusId = "TaskStatusId";
        public const string CreationDate = "CreationDate";
        public const string IsRecordActive = "IsRecordActive";

    }
}
