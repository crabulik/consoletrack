﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using TaskTracker.DAL.DTO.General;
using TaskTracker.DAL.DTO.Infrastructure;
using TaskTracker.DAL.DTO.Projects;
using TaskTracker.DAL.DTO.Tasks;

namespace TaskTracker.DAL.Contexts
{
    public sealed class TaskTrackerContext: DbContext
    {
        public TaskTrackerContext()
            : base("name=TaskTrackerDb")
        {

        }

        public DbSet<DtoAvatar> Avatars { get; set; }
        public DbSet<DtoComment> Comments { get; set; }
        public DbSet<DtoPosition> Positions { get; set; }
        public DbSet<DtoRole> Roles { get; set; }
        public DbSet<DtoRoleAction> RoleActions { get; set; }
        public DbSet<DtoUser> Users { get; set; }
        public DbSet<DtoDepartment> Departments { get; set; }
        public DbSet<DtoProject> Projects { get; set; }
        public DbSet<DtoSprint> Sprints { get; set; }
        public DbSet<DtoSprintBacklogItem> SprintBacklogItems { get; set; }
        public DbSet<DtoTeam> Teams { get; set; }
        public DbSet<DtoTeamMember> TeamMembers { get; set; }
        public DbSet<DtoTask> Tasks { get; set; }
        public DbSet<DtoTaskAttachment> TaskAttachments { get; set; }
        public DbSet<DtoTaskComment> TaskComments { get; set; }
        public DbSet<DtoTaskStatus> TaskStatuses { get; set; }
        public DbSet<DtoTaskStatusHistoryItem> TaskStatusHistoryItems { get; set; }
        public DbSet<DtoTaskTimeTrackerItem> TaskTimeTrackerItem { get; set; }
        public DbSet<DtoTaskType> TaskTypes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
        }
    }
}