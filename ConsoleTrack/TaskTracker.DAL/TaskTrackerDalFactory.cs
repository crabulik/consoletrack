﻿using TaskTracker.DAL.Contexts;
using TaskTracker.DAL.Contracts;

namespace TaskTracker.DAL
{
    public class TaskTrackerDalFactory
    {
        public IUnitOfWork GetClearUnitOfWork()
        {
            var dataSetMapper = new DataSetMapper();
            return new UnitOfWork(new TaskTrackerContext());
        }
    }
}