﻿using System;
using System.Data;
using TaskTracker.Constants;
using TaskTracker.DAL.Constants;
using TaskTracker.DAL.DTO;

namespace TaskTracker.DAL
{
    internal sealed class DataSetMapper
    {
        public DataSet CreateEmptyDataSet()
        {
            var result = new DataSet();

            result.Tables.Add(GetDepartmentsTable());
            result.Tables.Add(GetAvatarsTable());
            result.Tables.Add(GetRolesTable());
            result.Tables.Add(GetUsersTable());
            result.Tables.Add(GetCommentsTable());
            result.Tables.Add(GetProjectsTable());
            result.Tables.Add(GetSprintsTable());

            InitForeignKeys(result);
            InsertSeedData(result);
            return result;
        }

        #region Create Tables

        private DataTable GetDepartmentsTable()
        {
            var result = new DataTable(TablesNames.Departments);
            var idCol = result.Columns.Add(DepartmentsFields.DepartmentId, typeof (Int32));
            idCol.AllowDBNull = false;
            idCol.Unique = true;
            idCol.AutoIncrement = true;
            idCol.AutoIncrementSeed = DbConstants.AutoIncrementIdSeed;

            result.Columns.Add(DepartmentsFields.Name, typeof (String)).AllowDBNull = false;
            result.Columns.Add(DepartmentsFields.IsRecordActive, typeof (bool)).AllowDBNull = false;

            result.PrimaryKey = new[] {idCol};
            return result;
        }

        private DataTable GetRolesTable()
        {
            var result = new DataTable(TablesNames.Roles);
            var idCol = result.Columns.Add(RolesFields.RoleId, typeof (Int32));
            idCol.AllowDBNull = false;
            idCol.Unique = true;
            idCol.AutoIncrement = true;
            idCol.AutoIncrementSeed = DbConstants.AutoIncrementIdSeed;

            result.Columns.Add(RolesFields.Name, typeof (String)).AllowDBNull = false;
            result.Columns.Add(RolesFields.IsSuperAdmin, typeof (bool)).AllowDBNull = false;
            result.Columns.Add(RolesFields.IsRecordActive, typeof (bool)).AllowDBNull = false;

            result.PrimaryKey = new[] {idCol};
            return result;
        }

        private DataTable GetAvatarsTable()
        {
            var result = new DataTable(TablesNames.Avatars);
            var idCol = result.Columns.Add(AvatarsFields.AvatarId, typeof(Int32));
            idCol.AllowDBNull = false;
            idCol.Unique = true;
            idCol.AutoIncrement = true;
            idCol.AutoIncrementSeed = DbConstants.AutoIncrementIdSeed;

            result.Columns.Add(AvatarsFields.Data, typeof(byte[])).AllowDBNull = false;

            result.PrimaryKey = new[] { idCol };
            return result;
        }

        private DataTable GetUsersTable()
        {
            var result = new DataTable(TablesNames.Users);
            var idCol = result.Columns.Add(UsersFields.UserId, typeof(Int32));
            idCol.AllowDBNull = false;
            idCol.Unique = true;
            idCol.AutoIncrement = true;
            idCol.AutoIncrementSeed = DbConstants.AutoIncrementIdSeed;

            result.Columns.Add(UsersFields.RoleId, typeof(Int32)).AllowDBNull = false;
            result.Columns.Add(UsersFields.AvatarId, typeof(Int32)).AllowDBNull = false;
            result.Columns.Add(UsersFields.DepartmentId, typeof(Int32)).AllowDBNull = false;
            result.Columns.Add(UsersFields.RealName, typeof(string)).AllowDBNull = false;
            result.Columns.Add(UsersFields.NickName, typeof(string)).AllowDBNull = false;
            var email = result.Columns.Add(UsersFields.Email, typeof(string));
            email.AllowDBNull = false;
            email.Unique = true;

            result.Columns.Add(UsersFields.PassHash, typeof(byte[])).AllowDBNull = false;
            result.Columns.Add(UsersFields.PassSalt, typeof(byte[])).AllowDBNull = false;
            result.Columns.Add(UsersFields.IsNeedPassCorrection, typeof(bool)).AllowDBNull = false;
            result.Columns.Add(UsersFields.IsRecordActive, typeof(bool)).AllowDBNull = false;

            result.PrimaryKey = new[] { idCol };
            return result;
        }

        private DataTable GetCommentsTable()
        {
            var result = new DataTable(TablesNames.Comments);
            var idCol = result.Columns.Add(CommentsFields.CommentId, typeof(Int32));
            idCol.AllowDBNull = false;
            idCol.Unique = true;
            idCol.AutoIncrement = true;
            idCol.AutoIncrementSeed = DbConstants.AutoIncrementIdSeed;

            result.Columns.Add(CommentsFields.AuthorUserId, typeof(Int32)).AllowDBNull = false;
            result.Columns.Add(CommentsFields.IsRecordActive, typeof(bool)).AllowDBNull = false;
            result.Columns.Add(CommentsFields.Text, typeof(string)).AllowDBNull = false;
            result.Columns.Add(CommentsFields.WhenPosted, typeof(DateTime)).AllowDBNull = false;

            result.PrimaryKey = new[] { idCol };
            return result;
        }

        private DataTable GetProjectsTable()
        {
            var result = new DataTable(TablesNames.Projects);
            var idCol = result.Columns.Add(ProjectsFields.ProjectId, typeof (Int32));
            idCol.AllowDBNull = false;
            idCol.Unique = true;
            idCol.AutoIncrement = true;
            idCol.AutoIncrementSeed = DbConstants.AutoIncrementIdSeed;
            
            result.Columns.Add(ProjectsFields.TeamId, typeof (Int32)).AllowDBNull = false;
            result.Columns.Add(ProjectsFields.Acronym, typeof (string)).AllowDBNull = false;
            result.Columns.Add(ProjectsFields.Description, typeof (string)).AllowDBNull = true;
            result.Columns.Add(ProjectsFields.EstimateTime, typeof (Int32)).AllowDBNull = false;
            result.Columns.Add(ProjectsFields.IsRecordActive, typeof (bool)).AllowDBNull = false;
            result.Columns.Add(ProjectsFields.Name, typeof (string)).AllowDBNull = false;
            result.Columns.Add(ProjectsFields.ProjectStatus, typeof (Int32)).AllowDBNull = false;
            return result;
        }

        private DataTable GetSprintsTable()
        {
            var result = new DataTable(TablesNames.Sprints);
            var idCol = result.Columns.Add(SprintsFields.SprintId, typeof (Int32));
            idCol.AllowDBNull = false;
            idCol.Unique = true;
            idCol.AutoIncrement = true;
            idCol.AutoIncrementSeed = DbConstants.AutoIncrementIdSeed;
            
            result.Columns.Add(SprintsFields.ProjectId, typeof (Int32)).AllowDBNull = false;
            result.Columns.Add(SprintsFields.Caption, typeof (string)).AllowDBNull = false;
            result.Columns.Add(SprintsFields.Description, typeof (string)).AllowDBNull = true;
            result.Columns.Add(SprintsFields.StartDate, typeof (DateTime)).AllowDBNull = false;
            result.Columns.Add(SprintsFields.EndDate, typeof (DateTime)).AllowDBNull = false;
            result.Columns.Add(SprintsFields.IsRecordActive, typeof (bool)).AllowDBNull = false;
            result.Columns.Add(SprintsFields.SprintStatus, typeof (Int32)).AllowDBNull = false;
            return result;
        }
        #endregion


        /// <summary>
        /// Initialixe all foreign keys.
        /// </summary>
        private void InitForeignKeys(DataSet db)
        {
            var fkUsersRoles = new ForeignKeyConstraint("FK_Users_Roles",
                db.Tables[TablesNames.Roles].Columns[RolesFields.RoleId],
                db.Tables[TablesNames.Users].Columns[UsersFields.RoleId]) {DeleteRule = Rule.None};
            db.Tables[TablesNames.Users].Constraints.Add(fkUsersRoles);

            var fkUsersAvatars = new ForeignKeyConstraint("FK_Users_Avatars",
                db.Tables[TablesNames.Avatars].Columns[AvatarsFields.AvatarId],
                db.Tables[TablesNames.Users].Columns[UsersFields.AvatarId]) {DeleteRule = Rule.None};
            db.Tables[TablesNames.Users].Constraints.Add(fkUsersAvatars);

            var fkUsersDepartments = new ForeignKeyConstraint("FK_Users_Departments",
                db.Tables[TablesNames.Departments].Columns[DepartmentsFields.DepartmentId],
                db.Tables[TablesNames.Users].Columns[UsersFields.DepartmentId]) {DeleteRule = Rule.None};
            db.Tables[TablesNames.Users].Constraints.Add(fkUsersDepartments);

            var fkCommentsUsers = new ForeignKeyConstraint("FK_Comments_Users",
                db.Tables[TablesNames.Users].Columns[UsersFields.UserId],
                db.Tables[TablesNames.Comments].Columns[CommentsFields.AuthorUserId]) {DeleteRule = Rule.None};
            db.Tables[TablesNames.Comments].Constraints.Add(fkCommentsUsers);

            //var fkProjectsTeams = new ForeignKeyConstraint("FK_Projects_Teams",
            //    db.Tables[TablesNames.Teams].Columns[TeamsFields.TeamId],
            //    db.Tables[TablesNames.Projects].Columns[ProjectsFields.TeamId]) {DeleteRule = Rule.None};
            //db.Tables[TablesNames.Projects].Constraints.Add(fkProjectsTeams);

            var fkSprintsProjects = new ForeignKeyConstraint("FK_Sprints_Projects",
                db.Tables[TablesNames.Projects].Columns[ProjectsFields.ProjectId],
                db.Tables[TablesNames.Sprints].Columns[SprintsFields.ProjectId]) {DeleteRule = Rule.None};
            db.Tables[TablesNames.Sprints].Constraints.Add(fkSprintsProjects);
        }

        /// <summary>
        /// Insert initial data to tables. For example Default operators or standart positions
        /// </summary>
        private void InsertSeedData(DataSet db)
        {
            var departmentTable = db.Tables[TablesNames.Departments];
            var departmentSeed = departmentTable.NewRow();
            departmentSeed[DepartmentsFields.DepartmentId] = SeedValues.GeneralDepartmentId;
            departmentSeed[DepartmentsFields.IsRecordActive] = true;
            departmentSeed[DepartmentsFields.Name] = SeedValues.GeneralDepartmentName;
            departmentTable.Rows.Add(departmentSeed);

            var avatarsTable = db.Tables[TablesNames.Avatars];
            var avatarSeed = avatarsTable.NewRow();
            avatarSeed[AvatarsFields.AvatarId] = SeedValues.SuperAdminAvatarId;
            avatarSeed[AvatarsFields.Data] = new byte[0];
            avatarsTable.Rows.Add(avatarSeed);

            var rolesTable = db.Tables[TablesNames.Roles];
            var seedRow = rolesTable.NewRow();
            seedRow[RolesFields.RoleId] = SeedValues.SuperAdminRoleId;
            seedRow[RolesFields.Name] = SeedValues.SuperAdminRoleName;
            seedRow[RolesFields.IsSuperAdmin] = true;
            seedRow[RolesFields.IsRecordActive] = true;
            rolesTable.Rows.Add(seedRow);

            var usersTable = db.Tables[TablesNames.Users];
            seedRow = usersTable.NewRow();
            seedRow[UsersFields.UserId] = SeedValues.UserAdminId;
            seedRow[UsersFields.RoleId] = SeedValues.SuperAdminRoleId;
            seedRow[UsersFields.AvatarId] = SeedValues.SuperAdminAvatarId;
            seedRow[UsersFields.DepartmentId] = SeedValues.GeneralDepartmentId;
            seedRow[UsersFields.NickName] = SeedValues.UserAdminName;
            seedRow[UsersFields.RealName] = SeedValues.UserAdminName;
            seedRow[UsersFields.Email] = SeedValues.UserAdminName;
            seedRow[UsersFields.PassHash] = new byte[0];
            seedRow[UsersFields.PassSalt] = new byte[0];
            seedRow[UsersFields.IsNeedPassCorrection] = false;
            seedRow[UsersFields.IsRecordActive] = true;
            usersTable.Rows.Add(seedRow);

        }
    }
}