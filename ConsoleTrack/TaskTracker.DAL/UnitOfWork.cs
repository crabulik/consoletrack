﻿using System;
using System.Data;
using TaskTracker.DAL.Contexts;
using TaskTracker.DAL.Contracts;
using TaskTracker.DAL.Contracts.Repositories.General;
using TaskTracker.DAL.Contracts.Repositories.Infrastructure;
using TaskTracker.DAL.DTO.General;
using TaskTracker.DAL.DTO.Infrastructure;
using TaskTracker.DAL.Repositories;
using TaskTracker.DAL.Repositories.General;
using TaskTracker.DAL.Repositories.Infrastructure;

namespace TaskTracker.DAL
{
    public sealed class UnitOfWork: IUnitOfWork
    {
        private object _lockObject = new object();
        private TaskTrackerContext _context;

        private PositionRepository _positionRepository;
        private DepartmentsRepository _departmentsRepository;
        private AvatarsRepository _avatarsRepository;
        private RolesRepository _rolesRepository;
        private RoleActionRepository _roleActionsRepository;
        private UsersRepository _usersRepository;

        public UnitOfWork(TaskTrackerContext context)
        {
            _context = context;
        }

        public IDepartmentsRepository Departments
        {
            get
            {
                if (_departmentsRepository == null)
                {
                    lock (_lockObject)
                    {
                        if (_departmentsRepository == null)
                        {
                            _departmentsRepository = new DepartmentsRepository(_context);
                        }
                    }
                }
                return _departmentsRepository;
            }
        }
        
        
        public IPositionRepository Positions
        {
            get
            {
                if (_positionRepository == null)
                {
                    lock (_lockObject)
                    {
                        if (_positionRepository == null)
                        {
                            _positionRepository = new PositionRepository(_context);
                        }
                    }
                }
                return _positionRepository;
            }
        }

        public IAvatarsRepository Avatars
        {
            get
            {
                if (_avatarsRepository == null)
                {
                    lock (_lockObject)
                    {
                        if (_avatarsRepository == null)
                        {
                            _avatarsRepository = new AvatarsRepository(_context);
                        }
                    }
                }
                return _avatarsRepository;
            }
        }

        public IRolesRepository Roles
        {
            get
            {
                if (_rolesRepository == null)
                {
                    lock (_lockObject)
                    {
                        if (_rolesRepository == null)
                        {
                            _rolesRepository = new RolesRepository(_context);
                        }
                    }
                }
                return _rolesRepository;
            }
        }

        public IUsersRepository Users
        {
            get
            {
                if (_usersRepository == null)
                {
                    lock (_lockObject)
                    {
                        if (_usersRepository == null)
                        {
                            _usersRepository = new UsersRepository(_context);
                        }
                    }
                }
                return _usersRepository;
            }
        }

        

        public void Save()
        {
            _context.SaveChanges();
        }

        public IRoleActionsRepository RoleAction
        {
            get
            {
                if (_roleActionsRepository == null)
                {
                    lock (_lockObject)
                    {
                        if (_roleActionsRepository == null)
                        {
                            _roleActionsRepository = new RoleActionRepository(_context);
                        }
                    }
                }
                return _roleActionsRepository;
            }
        }

        private bool disposed = false;

        private void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}