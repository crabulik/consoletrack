﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using TaskTracker.Constants;
using TaskTracker.DAL.Constants;
using TaskTracker.DAL.Contexts;
using TaskTracker.DAL.Contracts;
using TaskTracker.DAL.DTO.General;
using TaskTracker.DAL.Exceptions;

namespace TaskTracker.DAL.Repositories.General
{
    public class PositionRepository : GenericRepository<DtoPosition>, IPositionRepository
    {
        public PositionRepository(TaskTrackerContext context) : base(context)
        {
        }

        public DtoPosition GetSingle(int id)
        {
            return GetAll().FirstOrDefault(x => x.PositionId == id);
        }
    }
}

//    class PositionRepository : IRepository<DtoPosition>
//    {
//        private TaskTrackerContext _context;

//        private DataSet Db { get; }





//        public PositionRepository(DataSet db)
//        {
//            Db = db;
//        }

//        public PositionRepository(TaskTrackerContext _context)
//        {
//            this._context = _context;
//        }

//        public DtoPosition Get(int id)
//        {
//            var table = Db.Tables[TablesNames.Positions];

//            var dataForReturn = table.Select($"{PositionsFields.PositionId}={id}");
//            if (dataForReturn.Length <= 0)
//                return null;
//            return GetDtoByDataRow(dataForReturn[0]);
//        }

//        public void Create(DtoPosition item)
//        {
//            if (item == null)
//            {
//                throw new ArgumentNullException(nameof(item));
//            }

//            var table = Db.Tables[TablesNames.Positions];

//            var createdRow = table.NewRow();

//            FillRowByData(createdRow, item);

//            table.Rows.Add(createdRow);

//            item.PositionId = (int)createdRow[PositionsFields.PositionId];
//        }

//        public void Delete(int id)
//        {
//            var table = Db.Tables[TablesNames.Positions];

//            var dataForChange = table.Select($"{PositionsFields.PositionId}={id}");
//            if (dataForChange.Length > 0)
//                foreach (var row in dataForChange)
//                {
//                    row[PositionsFields.IsRecordActive] = false;
//                }
//        }

//        public IQueryable<DtoPosition> GetQueryable()
//        {
//            var resultList = new List<DtoPosition>();

//            var table = Db.Tables[TablesNames.Positions];

//            foreach (DataRow row in table.Rows)
//            {
//                resultList.Add(GetDtoByDataRow(row));
//            }

//            return resultList.AsQueryable();
//        }

//        public void Update(DtoPosition item)
//        {
//            if (item == null)
//            {
//                throw new ArgumentNullException(nameof(item));
//            }

//            var table = Db.Tables[TablesNames.Positions];

//            var dataForChange = table.Select($"{PositionsFields.PositionId}={item.PositionId}");
//            if (dataForChange.Length <= 0)
//                throw new RecordNotFoundException(item.PositionId, TablesNames.Positions);

//            foreach (var row in dataForChange)
//            {
//                FillRowByData(row, item);
//            }
//        }

//        private DtoPosition GetDtoByDataRow(DataRow row)
//        {
//            return new DtoPosition
//            {
//                PositionId = (int)row[PositionsFields.PositionId],
//                Title = (string)row[PositionsFields.Title],
//                IsRecordActive = (bool)row[PositionsFields.IsRecordActive]
//            };
//        }

//        private void FillRowByData(DataRow row, DtoPosition dtoData)
//        {
//            //row[PositionsFields.PositionId] = dtoData.PositionId;
//            row[PositionsFields.Title] = dtoData.Title;
//            row[PositionsFields.IsRecordActive] = dtoData.IsRecordActive;
//        }

//    }
//}
