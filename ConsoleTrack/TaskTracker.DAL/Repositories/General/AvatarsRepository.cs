﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using TaskTracker.Constants;
using TaskTracker.DAL.Constants;
using TaskTracker.DAL.Contexts;
using TaskTracker.DAL.Contracts;
using TaskTracker.DAL.Contracts.Repositories.General;
using TaskTracker.DAL.DTO.General;
using TaskTracker.DAL.Exceptions;

namespace TaskTracker.DAL.Repositories.General
{
    public class AvatarsRepository : GenericRepository<DtoAvatar>, IAvatarsRepository
    {
        public AvatarsRepository(TaskTrackerContext context) : base(context)
        {
        }

        public DtoAvatar GetSingle(int id)
        {
            return GetAll().FirstOrDefault(x => x.AvatarId == id);
        }
    }
}