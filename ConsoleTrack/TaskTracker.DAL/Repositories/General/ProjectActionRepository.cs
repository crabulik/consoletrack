﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTracker.Constants;
using TaskTracker.DAL.Constants;
using TaskTracker.DAL.Contracts;
using TaskTracker.DAL.DTO.General;
using TaskTracker.DAL.DTO.Tasks;
using TaskTracker.DAL.Exceptions;

namespace TaskTracker.DAL.Repositories.General
{
    public class ProjectActionRepository : IRepository<DtoProjectAction>
    {
        private DataSet Db { get; }

        public ProjectActionRepository(DataSet db)
        {
            Db = db;
        }

        public DtoProjectAction Get(int id)
        {
            var table = Db.Tables[TablesNames.ProjectActions];

            var dataForReturn = table.Select($"{ProjectActionsFields.Value}={id}");
            if (dataForReturn.Length <= 0)
                return null;
            return GetDtoByDataRow(dataForReturn[0]);
        }

        public void Create(DtoProjectAction item)
        {
            if (item == null)
            {
                throw new ArgumentNullException(nameof(item));
            }

            var table = Db.Tables[TablesNames.ProjectActions];

            var createdRow = table.NewRow();

            FillRowByData(createdRow, item);

            table.Rows.Add(createdRow);

            item.Value = (int)createdRow[ProjectActionsFields.Value];
        }

        public void Update(DtoProjectAction item)
        {
            if (item == null)
            {
                throw new ArgumentNullException(nameof(item));
            }

            var table = Db.Tables[TablesNames.ProjectActions];

            var dataForChange = table.Select($"{ProjectActionsFields.Value}={item.Value}");
            if (dataForChange.Length <= 0)
                throw new RecordNotFoundException(item.Value, TablesNames.ProjectActions);

            foreach (var row in dataForChange)
            {
                FillRowByData(row, item);
            }
        }

        public void Delete(int id)
        {
            var table = Db.Tables[TablesNames.ProjectActions];

            var dataForChange = table.Select($"{ProjectActionsFields.Value}={id}");
            if (dataForChange.Length > 0)
                foreach (var row in dataForChange)
                {
                    row[ProjectActionsFields.IsRecordActive] = false;
                }
        }

        public IQueryable<DtoProjectAction> GetQueryable()
        {
            var resultList = new List<DtoProjectAction>();

            var table = Db.Tables[TablesNames.ProjectActions];

            foreach (DataRow row in table.Rows)
            {
                resultList.Add(GetDtoByDataRow(row));
            }

            return resultList.AsQueryable();
        }

        private DtoProjectAction GetDtoByDataRow(DataRow row)
        {
            return new DtoProjectAction
            {
                Value = (int)row[ProjectActionsFields.Value],
                Description = (string)row[ProjectActionsFields.Description],
                IsRecordActive = (bool)row[ProjectActionsFields.IsRecordActive]
            };
        }

        private void FillRowByData(DataRow row, DtoProjectAction dtoData)
        {
            row[ProjectActionsFields.Value] = dtoData.Value;
            row[ProjectActionsFields.Description] = dtoData.Description;
            row[ProjectActionsFields.IsRecordActive] = dtoData.IsRecordActive;
        }

    }
}
