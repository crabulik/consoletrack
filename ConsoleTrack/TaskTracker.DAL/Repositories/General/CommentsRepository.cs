﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using TaskTracker.Constants;
using TaskTracker.DAL.Constants;
using TaskTracker.DAL.Contracts;
using TaskTracker.DAL.DTO.General;
using TaskTracker.DAL.Exceptions;

namespace TaskTracker.DAL.Repositories.General
{
    public class CommentsRepository : IRepository<DtoComment>
    {
        private DataSet Db { get; }

        public CommentsRepository(DataSet db)
        {
            Db = db;
        }

        public DtoComment Get(int id)
        {
            var table = Db.Tables[TablesNames.Comments];

            var dataForReturn = table.Select($"{CommentsFields.CommentId}={id}");
            if (dataForReturn.Length <= 0)
                return null;
            return GetDtoByDataRow(dataForReturn[0]);

        }

        public void Create(DtoComment item)
        {
            if (item == null)
            {
                throw new ArgumentNullException(nameof(item));
            }

            var table = Db.Tables[TablesNames.Comments];

            var createdRow = table.NewRow();

            FillRowByData(createdRow, item);

            table.Rows.Add(createdRow);

            item.CommentId = (int)createdRow[CommentsFields.CommentId];
        }

        public void Update(DtoComment item)
        {
            if (item == null)
            {
                throw new ArgumentNullException(nameof(item));
            }

            var table = Db.Tables[TablesNames.Comments];

            var dataForChange = table.Select($"{CommentsFields.CommentId}={item.CommentId}");
            if (dataForChange.Length <= 0)
                throw new RecordNotFoundException(item.CommentId, TablesNames.Comments);

            foreach (var row in dataForChange)
            {
                FillRowByData(row, item);
            }
        }

        public void Delete(int id)
        {
            var table = Db.Tables[TablesNames.Comments];

            var dataForChange = table.Select($"{CommentsFields.CommentId}={id}");
            if (dataForChange.Length > 0)
                foreach (var row in dataForChange)
                {
                    row[CommentsFields.IsRecordActive] = false;
                }
        }

        public IQueryable<DtoComment> GetQueryable()
        {
            var resultList = new List<DtoComment>();

            var table = Db.Tables[TablesNames.Comments];

            foreach (DataRow row in table.Rows)
            {
                resultList.Add(GetDtoByDataRow(row));
            }

            return resultList.AsQueryable();
        }

        private DtoComment GetDtoByDataRow(DataRow row)
        {
            return new DtoComment
            {
                CommentId = (int)row[CommentsFields.CommentId],
                AuthorUserId = (int)row[CommentsFields.AuthorUserId],
                IsRecordActive = (bool)row[CommentsFields.IsRecordActive],
                Text = (string)row[CommentsFields.Text],
                WhenPosted = (DateTime)row[CommentsFields.WhenPosted]
            };

        }

        private void FillRowByData(DataRow row, DtoComment dtoComment)
        {
            //row[CommentsFields.CommentId] = dtoComment.CommentId;
            row[CommentsFields.AuthorUserId] = dtoComment.AuthorUserId;
            row[CommentsFields.IsRecordActive] = dtoComment.IsRecordActive;
            row[CommentsFields.Text] = dtoComment.Text;
            row[CommentsFields.WhenPosted] = dtoComment.WhenPosted;

        }
    }
}
