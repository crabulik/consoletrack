﻿using System.Linq;
using TaskTracker.DAL.Contexts;
using TaskTracker.DAL.Contracts.Repositories.General;
using TaskTracker.DAL.DTO.General;

namespace TaskTracker.DAL.Repositories.General
{
    public class UsersRepository : GenericRepository<DtoUser>, IUsersRepository
    {
        public UsersRepository(TaskTrackerContext context) : base(context)
        {
        }

        public DtoUser GetSingle(int id)
        {
            return GetAll().FirstOrDefault(x => x.UserId == id);
        }
    }
}