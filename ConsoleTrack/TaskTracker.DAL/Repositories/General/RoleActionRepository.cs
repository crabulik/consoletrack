﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using TaskTracker.Constants;
using TaskTracker.DAL.Constants;
using TaskTracker.DAL.Contexts;
using TaskTracker.DAL.Contracts;
using TaskTracker.DAL.Contracts.Repositories.General;
using TaskTracker.DAL.DTO.General;
using TaskTracker.Resources;

namespace TaskTracker.DAL.Repositories.General
{
    class RoleActionRepository : GenericRepository<DtoRoleAction>, IRoleActionsRepository
    {
        public RoleActionRepository(TaskTrackerContext context) : base(context)
        {
        }

        public DtoRoleAction GetSingle(int roleId, int projectActionValue)
        {
            return GetAll().FirstOrDefault(x => (x.RoleId == roleId) && (x.ProjectActionValue == projectActionValue));
        }
    }
}
