﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using TaskTracker.Constants;
using TaskTracker.DAL.Constants;
using TaskTracker.DAL.Contexts;
using TaskTracker.DAL.Contracts;
using TaskTracker.DAL.Contracts.Repositories.General;
using TaskTracker.DAL.DTO.General;
using TaskTracker.DAL.Exceptions;

namespace TaskTracker.DAL.Repositories.General
{
    public class RolesRepository : GenericRepository<DtoRole>, IRolesRepository
    {
        public RolesRepository(TaskTrackerContext context) : base(context)
        {
        }

        public DtoRole GetSingle(int id)
        {
            return GetAll().FirstOrDefault(x => x.RoleId == id);
        }
    }
}