﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTracker.Constants;
using TaskTracker.DAL.Constants;
using TaskTracker.DAL.Contexts;
using TaskTracker.DAL.Contracts;
using TaskTracker.DAL.Contracts.Repositories.Tasks;
using TaskTracker.DAL.DTO;
using TaskTracker.DAL.DTO.Infrastructure;
using TaskTracker.DAL.DTO.Tasks;
using TaskTracker.DAL.Exceptions;
using TaskTracker.Resources;

namespace TaskTracker.DAL.Repositories.Tasks
{
    public class TaskCommentsRepository : GenericRepository<DtoTaskComment>, ITaskCommentsRepository
    {
        public TaskCommentsRepository(TaskTrackerContext context) : base(context)
        {
        }
        
        public DtoTaskComment GetSingle(int taskId, int commentId)
        {
            return GetAll().FirstOrDefault(x => (x.TaskId == taskId) && (x.CommentId == commentId));
        }
    }
}
