﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using TaskTracker.Constants;
using TaskTracker.DAL.Constants;
using TaskTracker.DAL.Contexts;
using TaskTracker.DAL.Contracts;
using TaskTracker.DAL.Contracts.Repositories.Tasks;
using TaskTracker.DAL.DTO.Tasks;
using TaskTracker.DAL.Exceptions;

namespace TaskTracker.DAL.Repositories.Tasks
{
    public class TaskAttachmentsRepository : GenericRepository<DtoTaskAttachment>, ITaskAttachmentsRepository
    {
        public TaskAttachmentsRepository(TaskTrackerContext context) : base(context)
        {
        }

        public DtoTaskAttachment GetSingle(int id)
        {
            return GetAll().FirstOrDefault(x => x.TaskAttachmentId == id);
        }
    }
}
