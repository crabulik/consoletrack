﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTracker.Constants;
using TaskTracker.DAL.Constants;
using TaskTracker.DAL.Contexts;
using TaskTracker.DAL.Contracts;
using TaskTracker.DAL.Contracts.Repositories.Tasks;
using TaskTracker.DAL.DTO.Tasks;
using TaskTracker.DAL.Exceptions;

namespace TaskTracker.DAL.Repositories.Tasks
{
    public class TaskTypesRepository : GenericRepository<DtoTaskType>, ITaskTypesRepository
    {
        public TaskTypesRepository(TaskTrackerContext context) : base(context)
        {
        }

        public DtoTaskType GetSingle(int id)
        {
            return GetAll().FirstOrDefault(x => x.TaskTypeId == id);
        }
    }
}
