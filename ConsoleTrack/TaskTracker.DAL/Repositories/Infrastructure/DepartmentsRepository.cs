﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using TaskTracker.Constants;
using TaskTracker.DAL.Constants;
using TaskTracker.DAL.Contexts;
using TaskTracker.DAL.Contracts;
using TaskTracker.DAL.Contracts.Repositories.Infrastructure;
using TaskTracker.DAL.DTO.Infrastructure;
using TaskTracker.DAL.Exceptions;

namespace TaskTracker.DAL.Repositories.Infrastructure
{
    public class DepartmentsRepository : GenericRepository<DtoDepartment>, IDepartmentsRepository
    {
        public DepartmentsRepository(TaskTrackerContext context) : base(context)
        {
        }

        public DtoDepartment GetSingle(int id)
        {
            return GetAll().FirstOrDefault(x => x.DepartmentId == id);
        }
    }
}