﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using TaskTracker.Constants;
using TaskTracker.DAL.Constants;
using TaskTracker.DAL.Contexts;
using TaskTracker.DAL.Contracts;
using TaskTracker.DAL.Contracts.Repositories.Projects;
using TaskTracker.DAL.DTO.Infrastructure;
using TaskTracker.DAL.DTO.Projects;
using TaskTracker.DAL.Exceptions;

namespace TaskTracker.DAL.Repositories.Projects
{
    public class SprintsRepository : GenericRepository<DtoSprint>, ISprintsRepository
    {
        public SprintsRepository(TaskTrackerContext context) : base(context)
        {
        }

        public DtoSprint GetSingle(int id)
        {
            return GetAll().FirstOrDefault(p => p.SprintId == id);
        }
    }
}
