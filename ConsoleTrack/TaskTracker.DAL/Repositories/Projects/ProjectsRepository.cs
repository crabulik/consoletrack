﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using TaskTracker.Constants;
using TaskTracker.DAL.Constants;
using TaskTracker.DAL.Contexts;
using TaskTracker.DAL.Contracts;
using TaskTracker.DAL.Contracts.Repositories.Projects;
using TaskTracker.DAL.DTO.Infrastructure;
using TaskTracker.DAL.DTO.Projects;
using TaskTracker.DAL.Exceptions;

namespace TaskTracker.DAL.Repositories.Projects
{
    public class ProjectsRepository : GenericRepository<DtoProject>, IProjectsRepository
    {
        public ProjectsRepository(TaskTrackerContext context) : base(context)
        {
        }

        public DtoProject GetSingle(int id)
        {
            return GetAll().FirstOrDefault(p => p.ProjectId == id);
        }
    }
}
