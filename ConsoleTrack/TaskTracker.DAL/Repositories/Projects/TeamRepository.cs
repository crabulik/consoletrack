﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using TaskTracker.Constants;
using TaskTracker.DAL.Constants;
using TaskTracker.DAL.Contexts;
using TaskTracker.DAL.Contracts;
using TaskTracker.DAL.Contracts.Repositories.Projects;
using TaskTracker.DAL.DTO.Infrastructure;
using TaskTracker.DAL.DTO.Projects;
using TaskTracker.DAL.Exceptions;

namespace TaskTracker.DAL.Repositories.Projects
{
    public class TeamsRepository : GenericRepository<DtoTeam>, ITeamsRepository
    {
        public TeamsRepository(TaskTrackerContext context) : base(context)
        {
        }

        public DtoTeam GetSingle(int id)
        {
            return GetAll().FirstOrDefault(p => p.TeamId == id);
        }
    }
}
