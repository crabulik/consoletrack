﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using TaskTracker.Constants;
using TaskTracker.DAL.Constants;
using TaskTracker.DAL.Contexts;
using TaskTracker.DAL.Contracts;
using TaskTracker.DAL.Contracts.Repositories.Projects;
using TaskTracker.DAL.DTO.Projects;
using TaskTracker.Resources;

namespace TaskTracker.DAL.Repositories.Projects
{
    public class SprintBacklogsRepository : GenericRepository<DtoSprintBacklogItem>, ISprintBacklogsRepository
    {
        public SprintBacklogsRepository(TaskTrackerContext context) : base(context)
        {
        }

        public DtoSprintBacklogItem GetSingle(int sprintId, int taskId)
        {
            return GetAll().FirstOrDefault(p => (p.SprintId == sprintId) && (p.TaskId == taskId));
        }
    }
}
