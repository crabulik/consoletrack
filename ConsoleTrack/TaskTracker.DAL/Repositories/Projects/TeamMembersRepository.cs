﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTracker.Constants;
using TaskTracker.DAL.Constants;
using TaskTracker.DAL.Contexts;
using TaskTracker.DAL.Contracts;
using TaskTracker.DAL.Contracts.Repositories.Projects;
using TaskTracker.DAL.DTO.Infrastructure;
using TaskTracker.DAL.DTO.Projects;
using TaskTracker.DAL.Exceptions;
using TaskTracker.Resources;

namespace TaskTracker.DAL.Repositories.Projects
{
    public class TeamMembersRepository : GenericRepository<DtoTeamMember>, ITeamMembersRepository
    {
        public TeamMembersRepository(TaskTrackerContext context) : base(context)
        {
        }

        public DtoTeamMember GetSingle(int teamId, int userId)
        {
            return GetAll().FirstOrDefault(p => (p.TeamId == teamId) && (p.UserId == userId));
        }
    }
}
