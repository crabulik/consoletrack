﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using TaskTracker.DAL.Contexts;
using TaskTracker.DAL.Contracts;

namespace TaskTracker.DAL.Repositories
{
    public abstract class GenericRepository<T> : IGenericRepository<T> where T : class
    {

        private readonly TaskTrackerContext _context;

        protected GenericRepository(TaskTrackerContext context)
        {
            _context = context;
        }

        public virtual IQueryable<T> GetAll()
        {
            IQueryable<T> query = _context.Set<T>();
            return query;
        }

        public virtual IQueryable<T> GetAll(IList<string> includesList)
        {
            DbQuery<T> dbQuery =_context.Set<T>();
            if (includesList != null)
            {
                foreach (var item in includesList)
                {
                    dbQuery = dbQuery.Include(item);
                }
            }
            IQueryable<T> query = dbQuery.AsQueryable();
            return query;
        }

        public IQueryable<T> FindBy(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {

            IQueryable<T> query = _context.Set<T>().Where(predicate);
            return query;
        }

        public virtual void Add(T entity)
        {
            _context.Set<T>().Add(entity);
        }

        public virtual void Delete(T entity)
        {
            _context.Set<T>().Remove(entity);
        }

        public virtual void Edit(T entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
        }
    }
}