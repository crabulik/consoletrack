﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace TaskTracker.Utils
{
    public sealed class PasswordCypher
    {
        private const string SaltIsEmptyMessage = "Salt can't be empty.";
        private const string PasswordIsEmptyMessage = "Password can't be empty.";
        private const string SaltToLowMessage = "Salt length must be bigger or equal 8 bytes.";

        private const int KEY_LENGTH = 20;
        private const int SALT_LENGTH = 10;

        private byte[] _key;
        private byte[] _salt;
        private string _password;

        public string KeyString
        {
            get
            {
                return System.Text.Encoding.Default.GetString(_key);
            }
        }

        public string SaltString
        {
            get
            {
                return System.Text.Encoding.Default.GetString(_salt);
            }
        }

        public byte[] KeyBytes
        {
            get
            {
                return _key;
            }
        }

        public byte[] SaltBytes
        {
            get
            {
                return _salt;
            }
        }

        public void Generate(string password, string salt)
        {
            if (string.IsNullOrEmpty(salt))
                throw new ArgumentException(SaltIsEmptyMessage);

            _salt = System.Text.Encoding.Default.GetBytes(salt);
            Generate(password, _salt);
        }

        public void Generate(string password, byte[] salt)
        {
            _password = password;
            _salt = salt;
            CheckPassword();
            CheckSalt();

            using (var deriveBytes = new Rfc2898DeriveBytes(_password, _salt))
            {
                _salt = deriveBytes.Salt;
                _key = deriveBytes.GetBytes(KEY_LENGTH); // derive a 20-byte key
            }
        }

        public void Generate(string password)
        {
            _password = password;
            CheckPassword();

            using (var deriveBytes = new Rfc2898DeriveBytes(password, SALT_LENGTH))
            {
                _salt = deriveBytes.Salt;
                _key = deriveBytes.GetBytes(KEY_LENGTH); // derive a 20-byte key
            }
        }

        private void CheckPassword()
        {
            if (string.IsNullOrEmpty(_password))
                throw new ArgumentException(PasswordIsEmptyMessage);
        }

        private void CheckSalt()
        {
            if (_salt.Length < 8)
                throw new ArgumentException(SaltToLowMessage);
        }
    }
}
