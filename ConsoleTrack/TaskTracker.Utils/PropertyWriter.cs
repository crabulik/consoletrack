﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace TaskTracker.Utils
{
    public static class PropertyWriter
    {
        public static string GetPropertyData(object targetValue)
        {
            string retStr = String.Empty;
            StringBuilder sb = new StringBuilder();

            Type targetType = targetValue.GetType();

            try
            {
                //
                PropertyInfo[] targetProperties = targetType.GetProperties(BindingFlags.Public | BindingFlags.Instance);
                string[] namespacesPart = targetType.ToString().Split(new char[] {'.'});
                sb.Append($"Properties of {namespacesPart[namespacesPart.Length - 1]} : => ");
                for (int i = 0; i < targetProperties.Length; ++i)
                {
                    sb.AppendFormat("{0} = {1}, ", targetProperties[i].Name, targetProperties[i].GetValue(targetValue));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : {0}", e.Message);
            }

            return sb.ToString(0, sb.Length - 2);
        }
    }
}
