﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaskTracker.Constants;
using TaskTracker.DAL.Contracts;
using TaskTracker.DAL.Exceptions;
using TaskTracker.Entities.Infrastructure;
using TaskTrackerServices.DbServices.Converters;
using TaskTrackerServices.Enums;

namespace TaskTrackerServices.DbServices
{
    public sealed class DbInfrastructureService
    {
        private IUnitOfWork Uow { get; }

        public DbInfrastructureService(IUnitOfWork uow)
        {
            Uow = uow;
        }

        public IList<Department> GetDepartments()
        {
            var result = new List<Department>();

            var repository = Uow.Departments;

            foreach (var dto in repository.GetQueryable().Where(p => p.IsRecordActive))
            {
                result.Add(DtoConverter.Converter(dto));
            }

            return result;
        }

        public Department GetDepartment(int id)
        {
            var repository = Uow.Departments;
            var dto = repository.Get(id);
            if (dto != null)
            {
                return DtoConverter.Converter(dto);
            }

            return null;
        }

        public void DeleteDepartment(int id)
        {
            Uow.Departments.Delete(id);
        }

        public SaveResult SaveDepartment(Department entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }
            var dtoForSave = DtoConverter.Converter(entity);
            var repository = Uow.Departments;

            if (entity.DepartmentId == EntitiesConstants.IdIsNotSet)
            {
                repository.Create(dtoForSave);
                entity.DepartmentId = dtoForSave.DepartmentId;
            }
            else
            {
                try
                {
                    repository.Update(dtoForSave);
                }
                catch (RecordNotFoundException)
                {
                    return SaveResult.EntityNotFound;
                }
                
            }

            return SaveResult.UndefinedError;
        }
    }
}
