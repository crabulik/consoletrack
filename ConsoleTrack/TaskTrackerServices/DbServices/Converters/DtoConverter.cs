﻿using TaskTracker.DAL.DTO.Infrastructure;
using TaskTracker.Entities.Infrastructure;

namespace TaskTrackerServices.DbServices.Converters
{
    public static class DtoConverter
    {
        public static DtoDepartment Converter(Department entity)
        {
            return new DtoDepartment
            {
                DepartmentId = entity.DepartmentId,
                IsRecordActive = entity.IsRecordActive,
                Name = entity.Name
            };
        }

        public static Department Converter(DtoDepartment dto)
        {
            return new Department
            {
                DepartmentId = dto.DepartmentId,
                IsRecordActive = dto.IsRecordActive,
                Name = dto.Name
            };
        }
    }
}