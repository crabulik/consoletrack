﻿namespace TaskTrackerServices.Enums
{
    public enum SaveResult
    {
         Saved,
         EntityNotFound,
         UndefinedError
    }
}