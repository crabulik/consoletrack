﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using TaskTracker.DAL;
using TaskTracker.DAL.Contexts;
using TaskTracker.DAL.DTO.Enums;
using TaskTracker.DAL.DTO.General;
using TaskTracker.DAL.DTO.Infrastructure;
using TaskTracker.DAL.DTO.Projects;
using TaskTracker.DAL.DTO.Tasks;
using TaskTracker.Entities.General;
using TaskTracker.Entities.Infrastructure;
using TaskTracker.Entities.ProjectPart;
using TaskTracker.Entities.TaskPart;
using TaskTracker.Services.DbServices;
using TaskTracker.Services.DbServices.Converters;
using TaskTracker.Services.NotificationServices;
using TaskTracker.Services.NotificationServices.Contracts;
using TaskTracker.Services.NotificationServices.Entities;
using TaskTracker.Utils;
using Task = System.Threading.Tasks.Task;

namespace ConsoleTrack
{
    class Program
    {
        static void Main(string[] args)
        {
            //TestDepartment();
            //TestPosition();
            //TestUser();
            //TestEmailService();
            //TestEmitMapper();

        }

        #region Testing CRUD functions

        private static void TestDepartment()
        {
            // Просто код для тестирования репозиториев. В будущем снесется.
            var service = new DbInfrastructureService(new UnitOfWork(new TaskTrackerContext()));

            service.SaveDepartment(new Department {Name = "Main"});
            service.SaveDepartment(new Department {Name = "SubChild"});

            Console.WriteLine("Inserted");
            foreach (var dtoDepartment in service.GetDepartments())
            {
                Console.WriteLine(dtoDepartment);
            }

            var updatedItem = service.GetDepartments().FirstOrDefault(p => string.Equals(p.Name, "SubChild"));
            if (updatedItem != null)
            {
                updatedItem.Name = "SubChild 1";
                service.SaveDepartment(updatedItem);

                Console.WriteLine("Updated");
                foreach (var dtoDepartment in service.GetDepartments())
                {
                    Console.WriteLine(dtoDepartment);
                }

                Console.WriteLine("Get: " + service.GetDepartment(updatedItem.DepartmentId));


                service.DeleteDepartment(updatedItem.DepartmentId);

                Console.WriteLine("AfterDelete");
                foreach (var dtoDepartment in service.GetDepartments())
                {
                    Console.WriteLine(dtoDepartment);
                }
            }

            Console.ReadKey();
        }

        private static void TestPosition()
        {
            
            var service = new DbInfrastructureService(new UnitOfWork(new TaskTrackerContext()));

            service.SavePosition(new Position { Title = "PM" });
            service.SavePosition(new Position { Title= "MasterLomaster" });
            service.SavePosition(new Position { Title= "kodoJop" });

            Console.WriteLine("YPA cod robe");
            foreach (var dtoPosition in service.GetPositions())
            {
                Console.WriteLine(dtoPosition);
            }

            var updatedItem = service.GetPositions().FirstOrDefault(p => string.Equals(p.Title, "MasterLomaster"));
            if (updatedItem != null)
            {
                updatedItem.Title = "Up-data kodoJop";
                service.SavePosition(updatedItem);}

                Console.WriteLine("very hot");
                foreach (var dtoPosition in service.GetPositions())
                {
                    Console.WriteLine(dtoPosition);
                }

            Console.WriteLine("Get: " + service.GetPosition(updatedItem.PositionId));


                service.DeletePosition(updatedItem.PositionId);

                Console.WriteLine("AfterDelete");
                foreach (var dtoPosition in service.GetPositions())
                {
                    Console.WriteLine(dtoPosition);
                }
            }

        //private static void TestUser()
        //{
        //    try
        //    {
        //        var uow = new UnitOfWork(new TaskTrackerContext());

        //        var service = new DbUsersService(uow);
        //        var infraService = new DbInfrastructureService(uow);

        //        var randomEnding = new Random().Next(100);

        //        service.SaveUser(new User()
        //        {
        //            Avatar = new Avatar()
        //            {
        //                Data = new byte[0]
        //            },
        //            Department = infraService.GetDepartments().FirstOrDefault(),
        //            Role = new Role() {Name = "Coffee keeper"},
        //            Email = string.Concat("Email", randomEnding),
        //            NickName = "NickName",
        //            PassHash = new byte[0],
        //            PassSalt = new byte[0],
        //            RealName = "RealName"
        //        });
        //        Console.WriteLine(new string('-', 25));
        //        Console.WriteLine("Inserted");

        //        foreach (var dtoUser in service.GetUsers())
        //        {
        //            Console.WriteLine(dtoUser);
        //        }

        //        var updatedItem = service.GetUsers().FirstOrDefault(p => string.Equals(p.NickName, "NickName"));
        //        updatedItem = service.GetUser(updatedItem.UserId);
        //        if (updatedItem != null)
        //        {
        //            updatedItem.NickName = "UpdatedNickName";
        //            service.SaveUser(updatedItem);

        //            Console.WriteLine(new string('-', 25));
        //            Console.WriteLine("Updated");
        //            foreach (var dtoUser in service.GetUsers())
        //            {
        //                Console.WriteLine(dtoUser);
        //            }

        //            Console.WriteLine(new string('-', 25));
        //            Console.WriteLine("Get: " + service.GetUser(updatedItem.UserId));

        //            Console.WriteLine(new string('-', 25));
        //            Console.WriteLine("Get All Users: ");
        //            foreach (var user in service.GetUsers())
        //            {
        //                Console.WriteLine(user);
        //            }


        //            service.DeleteUser(updatedItem.UserId);

        //            Console.WriteLine(new string('-', 25));
        //            Console.WriteLine("AfterDelete");
        //            foreach (var dtoUser in service.GetUsers())
        //            {
        //                Console.WriteLine(dtoUser);
        //            }
        //        }

        //        Console.ReadKey();
        //    }
        //    catch (DbUpdateException ex)
        //    {
        //        Console.WriteLine("Exception: {0}", ex.InnerException);
        //        Console.ReadKey();
        //    }
        //    catch (System.Data.Entity.Validation.DbEntityValidationException ex)
        //    {
        //        foreach (var validationErrors in ex.EntityValidationErrors)
        //        {
        //            foreach (var validationError in validationErrors.ValidationErrors)
        //            {
        //                Console.WriteLine("Property: {0} throws Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
        //                Console.ReadKey();
        //            }
        //        }
        //    }
        //}

        private static void TestEmailService()
        {
            bool done = false;

            TrackerEmailMessage message = new TrackerEmailMessage()
            {
                Destination = @"viktor.vasylenko.78@gmail.com",
                //Destination = @"oleksandr.brusylo@gmail.com",
                Subject = @"Smtp sending test",
                Body = @"This was sended from ConsoleTrack!"
            };

            SmtpConfigurator configurator= new SmtpConfigurator();
            SmtpConfiguration smtpConfig = configurator.CreateBaseGmailConfiguration();

            EmailService eService = new EmailService((ISmtpConfig) smtpConfig);
            try
            {
                Task mailTask = eService.SendAsync(message);
                mailTask.Wait();
                done = mailTask.Wait(5000);
            }
            catch (Exception exc)
            {
                Console.WriteLine($"Message = {exc.Message}\nInnerExeptionMessage = {exc.InnerException.Message}");
            }

            if (done)
            {
                Console.WriteLine("Message is sended!");
            }
            else
            {
                Console.WriteLine("Time interval left, message not sended!");
            }
            Console.ReadKey(true);
        }

        private static void TestEmitMapper()
        {
            Console.WindowWidth = Console.LargestWindowWidth - 10;
            Console.WindowHeight = Console.LargestWindowHeight - 5;
            Console.WindowLeft = 0;
            Console.WindowTop = 0;

            // *** Department
            Department department = new Department() {DepartmentId = 12,IsRecordActive = true,Name = "depOne"};
            DtoDepartment dtoDepartment = new DtoDepartment();

            dtoDepartment = DtoConverter.Converter(department);
            department = DtoConverter.Converter(dtoDepartment);

            Console.WriteLine(PropertyWriter.GetPropertyData(dtoDepartment));
            Console.WriteLine(PropertyWriter.GetPropertyData(department) + "\n");

            // *** Role
            Role role = new Role() {RoleId = 13, IsSuperAdmin = false, IsRecordActive = true, Name = "roleOne"};
            DtoRole dtoRole = new DtoRole();

            dtoRole = DtoConverter.Converter(role);
            role = DtoConverter.Converter(dtoRole);

            Console.WriteLine(PropertyWriter.GetPropertyData(dtoRole));
            Console.WriteLine(PropertyWriter.GetPropertyData(role) + "\n");

            // *** RoleAction
            RoleAction roleAction = new RoleAction() {IsSet = false, ProjectActionValue = ProjectActionsEnum.AllowAddRole};
            DtoRoleAction dtoRoleAction = new DtoRoleAction();

            dtoRoleAction = DtoConverter.Converter(roleAction, 15);
            roleAction = DtoConverter.Converter(dtoRoleAction);

            Console.WriteLine(PropertyWriter.GetPropertyData(dtoRoleAction));
            Console.WriteLine(PropertyWriter.GetPropertyData(roleAction) + "\n");

            // *** Position
            Position position = new Position() { PositionId = 1, Title = "posTitle", IsRecordActive = true };
            DtoPosition dtoPosition = new DtoPosition();

            dtoPosition = DtoConverter.Converter(position);
            position = DtoConverter.Converter(dtoPosition);

            Console.WriteLine(PropertyWriter.GetPropertyData(dtoPosition));
            Console.WriteLine(PropertyWriter.GetPropertyData(position) + "\n");

            // *** Avatar
            Avatar avatar = new Avatar() { AvatarId = 100, Data = new byte[] { 255, 12, 1, 0, 47 } };
            DtoAvatar dtoAvatar = new DtoAvatar();

            dtoAvatar = DtoConverter.Converter(avatar);
            avatar = DtoConverter.Converter(dtoAvatar);

            Console.WriteLine(PropertyWriter.GetPropertyData(dtoAvatar));
            Console.WriteLine(PropertyWriter.GetPropertyData(avatar) + "\n");

            // *** TaskType
            TaskType taskType = new TaskType() {TaskTypeId = 99, Name = "epic", IsRecordActive = true};
            DtoTaskType dtoTaskType = new DtoTaskType();

            dtoTaskType = DtoConverter.Converter(taskType);
            taskType = DtoConverter.Converter(dtoTaskType);

            Console.WriteLine(PropertyWriter.GetPropertyData(dtoTaskType));
            Console.WriteLine(PropertyWriter.GetPropertyData(taskType) + "\n");

            // *** TaskStatus
            TaskStatus taskStatus = new TaskStatus() { TaskStatusId = 90, Name = "running", IsRecordActive = true };
            DtoTaskStatus dtoTaskStatus = new DtoTaskStatus();

            dtoTaskStatus = DtoConverter.Converter(taskStatus);
            taskStatus = DtoConverter.Converter(dtoTaskStatus);

            Console.WriteLine(PropertyWriter.GetPropertyData(dtoTaskStatus));
            Console.WriteLine(PropertyWriter.GetPropertyData(taskStatus) + "\n");

            // *** TaskAttacment
            TaskAttachment taskAttachment = new TaskAttachment()
            {
                TaskAttachmentsId = 45,
                Author = new User()
                    {
                        Avatar = new Avatar(){Data = new byte[0]},
                        Department = new Department() {DepartmentId = 9,IsRecordActive = true,Name = "Some app"},
                        Role = new Role() { Name = "Coffee keeper" },
                        Email = "mail@example.com",
                        NickName = "NickName",
                        PassHash = new byte[0],
                        PassSalt = new byte[0],
                        RealName = "RealName"
                    },
                AttachDate = DateTime.Today,
                Name = "Attach #1",
                Description = "Descr #1",
                TaskId = 12,
                MIME = "text",
                Data = new byte[] {12,21,13,31,14,41},
                IsRecordActive = true
            };
            DtoTaskAttachment dtoTaskAttachment = new DtoTaskAttachment();

            dtoTaskAttachment = DtoConverter.Converter(taskAttachment);
            taskAttachment = DtoConverter.Converter(dtoTaskAttachment);

            Console.WriteLine(PropertyWriter.GetPropertyData(dtoTaskAttachment));
            Console.WriteLine(PropertyWriter.GetPropertyData(taskAttachment) + "\n");

            // *** Team
            Team team = new Team() { TeamId = 91, Name = "DreamTeam", IsRecordActive = true };
            DtoTeam dtoTeam = new DtoTeam();

            dtoTeam = DtoConverter.Converter(team);
            team = DtoConverter.Converter(dtoTeam);

            Console.WriteLine(PropertyWriter.GetPropertyData(dtoTeam));
            Console.WriteLine(PropertyWriter.GetPropertyData(team) + "\n");

            // *** End
            Console.ReadKey(true);

        }

        #endregion
    }
}
