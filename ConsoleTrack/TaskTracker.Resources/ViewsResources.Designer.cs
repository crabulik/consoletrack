﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TaskTracker.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class ViewsResources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal ViewsResources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("TaskTracker.Resources.ViewsResources", typeof(ViewsResources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Profile.
        /// </summary>
        public static string rs__PanelMenuProfile {
            get {
                return ResourceManager.GetString("rs__PanelMenuProfile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add User.
        /// </summary>
        public static string rs_AddUserView {
            get {
                return ResourceManager.GetString("rs_AddUserView", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Submit.
        /// </summary>
        public static string rs_AddUserViewButton {
            get {
                return ResourceManager.GetString("rs_AddUserViewButton", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Department and Role should be set..
        /// </summary>
        public static string rs_AddUserViewDepartemntAndRoleMustBeSetError {
            get {
                return ResourceManager.GetString("rs_AddUserViewDepartemntAndRoleMustBeSetError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User Department.
        /// </summary>
        public static string rs_AddUserViewDepartment {
            get {
                return ResourceManager.GetString("rs_AddUserViewDepartment", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User Email.
        /// </summary>
        public static string rs_AddUserViewEmail {
            get {
                return ResourceManager.GetString("rs_AddUserViewEmail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Email value should be set..
        /// </summary>
        public static string rs_AddUserViewEmailMustBeSetError {
            get {
                return ResourceManager.GetString("rs_AddUserViewEmailMustBeSetError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User Name.
        /// </summary>
        public static string rs_AddUserViewName {
            get {
                return ResourceManager.GetString("rs_AddUserViewName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Name value should be set..
        /// </summary>
        public static string rs_AddUserViewNameMustBeSetError {
            get {
                return ResourceManager.GetString("rs_AddUserViewNameMustBeSetError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User Role.
        /// </summary>
        public static string rs_AddUserViewRole {
            get {
                return ResourceManager.GetString("rs_AddUserViewRole", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Confirm new password.
        /// </summary>
        public static string rs_ChangePasswordConfirmNewPasswordLabel {
            get {
                return ResourceManager.GetString("rs_ChangePasswordConfirmNewPasswordLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The ConfirmNewPassword mustn&apos;t be empty..
        /// </summary>
        public static string rs_ChangePasswordConfirmNewPasswordMustntBeEmpty {
            get {
                return ResourceManager.GetString("rs_ChangePasswordConfirmNewPasswordMustntBeEmpty", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The ConfirmNewPassword must equals the NewPassword..
        /// </summary>
        public static string rs_ChangePasswordConfirmNewPasswordShouldEquals {
            get {
                return ResourceManager.GetString("rs_ChangePasswordConfirmNewPasswordShouldEquals", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to New password.
        /// </summary>
        public static string rs_ChangePasswordNewPasswordLabel {
            get {
                return ResourceManager.GetString("rs_ChangePasswordNewPasswordLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The NewPassword mustn&apos;t be empty..
        /// </summary>
        public static string rs_ChangePasswordNewPasswordMustntBeEmpty {
            get {
                return ResourceManager.GetString("rs_ChangePasswordNewPasswordMustntBeEmpty", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The NewPassword must be different from th OldPassword..
        /// </summary>
        public static string rs_ChangePasswordNewPasswordShouldBeDifferent {
            get {
                return ResourceManager.GetString("rs_ChangePasswordNewPasswordShouldBeDifferent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The NewPassword mustn&apos;t have less then {0} characters..
        /// </summary>
        public static string rs_ChangePasswordNewPasswordShouldBeLonger {
            get {
                return ResourceManager.GetString("rs_ChangePasswordNewPasswordShouldBeLonger", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Your old password is incorrect.
        /// </summary>
        public static string rs_ChangePasswordOldPassMismatchedMessage {
            get {
                return ResourceManager.GetString("rs_ChangePasswordOldPassMismatchedMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Old password.
        /// </summary>
        public static string rs_ChangePasswordOldPasswordLabel {
            get {
                return ResourceManager.GetString("rs_ChangePasswordOldPasswordLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The OldPassword mustn&apos;t be empty..
        /// </summary>
        public static string rs_ChangePasswordOldPasswordMustntBeEmpty {
            get {
                return ResourceManager.GetString("rs_ChangePasswordOldPasswordMustntBeEmpty", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please, change your temporary password.
        /// </summary>
        public static string rs_ChangeTempPasswordCaption {
            get {
                return ResourceManager.GetString("rs_ChangeTempPasswordCaption", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to New Role Caption:.
        /// </summary>
        public static string rs_CreateRoleViewCaption {
            get {
                return ResourceManager.GetString("rs_CreateRoleViewCaption", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Caption should be set..
        /// </summary>
        public static string rs_CreateRoleViewCaptionMustBeSetError {
            get {
                return ResourceManager.GetString("rs_CreateRoleViewCaptionMustBeSetError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cancel.
        /// </summary>
        public static string rs_GeneralCancelButtonCaption {
            get {
                return ResourceManager.GetString("rs_GeneralCancelButtonCaption", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Save.
        /// </summary>
        public static string rs_GeneralSaveButtonCaption {
            get {
                return ResourceManager.GetString("rs_GeneralSaveButtonCaption", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Email.
        /// </summary>
        public static string rs_LoginViewEmailLabel {
            get {
                return ResourceManager.GetString("rs_LoginViewEmailLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Email value should be less than 256 characters..
        /// </summary>
        public static string rs_LoginViewEmailMustBeLess256 {
            get {
                return ResourceManager.GetString("rs_LoginViewEmailMustBeLess256", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Email value should be set..
        /// </summary>
        public static string rs_LoginViewEmailMustBeSetError {
            get {
                return ResourceManager.GetString("rs_LoginViewEmailMustBeSetError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Your Email or password is incorrect..
        /// </summary>
        public static string rs_LoginViewEmailOsPasswordIncorrectMessager {
            get {
                return ResourceManager.GetString("rs_LoginViewEmailOsPasswordIncorrectMessager", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Login.
        /// </summary>
        public static string rs_LoginViewLoginButton {
            get {
                return ResourceManager.GetString("rs_LoginViewLoginButton", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Password.
        /// </summary>
        public static string rs_LoginViewPasswordLable {
            get {
                return ResourceManager.GetString("rs_LoginViewPasswordLable", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Password value should be set..
        /// </summary>
        public static string rs_LoginViewPasswordMustBeSetError {
            get {
                return ResourceManager.GetString("rs_LoginViewPasswordMustBeSetError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Create new role.
        /// </summary>
        public static string rs_PanelMenuCreateRole {
            get {
                return ResourceManager.GetString("rs_PanelMenuCreateRole", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Manage users.
        /// </summary>
        public static string rs_PanelMenuManage {
            get {
                return ResourceManager.GetString("rs_PanelMenuManage", resourceCulture);
            }
        }
    }
}
