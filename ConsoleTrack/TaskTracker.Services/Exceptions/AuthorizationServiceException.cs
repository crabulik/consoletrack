﻿using System;
using System.Runtime.Serialization;

namespace TaskTracker.Services.Exceptions
{
    public class AuthorizationServiceException : Exception
    {
        public AuthorizationServiceException()
        {
        }

        public AuthorizationServiceException(string message) : base(message)
        {
        }

        public AuthorizationServiceException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected AuthorizationServiceException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}