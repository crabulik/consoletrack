﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace TaskTracker.Services.NotificationServices.Entities
{
    public class SmtpConfigurator
    {
        public SmtpConfiguration CreateBaseGmailConfiguration()
        {
            return new SmtpConfiguration(@"smtp.gmail.com", 587, @"console.track.lab@gmail.com", @"AltexSoftLab2015", @"console.track.lab@gmail.com");
        }
    }
}
