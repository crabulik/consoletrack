﻿using System;
using System.Net.Mail;
using TaskTracker.Services.NotificationServices.Contracts;

namespace TaskTracker.Services.NotificationServices.Entities
{
    public class SmtpConfiguration : ISmtpConfig
    {
        public SmtpConfiguration(string smtpServer, int smtpPort, string smtpUserName, string smtpPassword, string addressFrom)
        {
            SmtpServer = smtpServer;
            SmtpPort = smtpPort;
            SmtpUserName = smtpUserName;
            SmtpPassword = smtpPassword;
            AddressFrom = addressFrom;

            DeliveryMethod = SmtpDeliveryMethod.Network;
            DisplayedUserName = AddressFrom;
            EnableSsl = true;
        }
        public string AddressFrom { get; set; }

        public SmtpDeliveryMethod DeliveryMethod { get; set; }

        public string DisplayedUserName { get; set; }

        public bool EnableSsl { get; set; }

        public string SmtpPassword { get; set; }

        public int SmtpPort { get; set; }

        public string SmtpServer { get; set; }

        public string SmtpUserName { get; set; }
    }
}
