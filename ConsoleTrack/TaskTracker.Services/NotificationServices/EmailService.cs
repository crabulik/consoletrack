﻿using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using TaskTracker.Services.NotificationServices.Contracts;
using TaskTracker.Services.NotificationServices.Entities;

namespace TaskTracker.Services.NotificationServices
{
    public class EmailService : IEmailMessageService
    {
        private ISmtpConfig Config { get; set; }

        public EmailService(ISmtpConfig config)
        {
            Config = config;
        }

        public Task SendAsync(TrackerEmailMessage message)
        {
            SmtpClient smtp = new SmtpClient(Config.SmtpServer, Config.SmtpPort);
            smtp.DeliveryMethod = Config.DeliveryMethod;
            smtp.EnableSsl = Config.EnableSsl;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(Config.SmtpUserName, Config.SmtpPassword);

            // наш email с заголовком письма
            MailAddress from = new MailAddress(Config.AddressFrom, Config.DisplayedUserName);
            // кому отправляем
            MailAddress to = new MailAddress(message.Destination);
            // создаем объект сообщения
            MailMessage m = new MailMessage(from, to);

            m.Subject = message.Subject;
            m.Body = message.Body;

            // Send:
            return smtp.SendMailAsync(m);
        }
    }
}
