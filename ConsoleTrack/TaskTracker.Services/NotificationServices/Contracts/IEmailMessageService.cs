﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTracker.Services.NotificationServices.Entities;

namespace TaskTracker.Services.NotificationServices.Contracts
{
    interface IEmailMessageService
    {
        Task SendAsync(TrackerEmailMessage message);
    }
}
