﻿using System.Net.Mail;

namespace TaskTracker.Services.NotificationServices.Contracts
{
    public interface ISmtpConfig
    {
        string SmtpServer { get; set; }
        int SmtpPort { get; set; }
        bool EnableSsl { get; set; }
        string SmtpUserName { get; set; }
        string SmtpPassword { get; set; }
        SmtpDeliveryMethod DeliveryMethod { get; set; }
        string AddressFrom { get; set; }
        string DisplayedUserName { get; set; }
    }
}
