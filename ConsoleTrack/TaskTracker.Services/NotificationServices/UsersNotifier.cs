﻿using TaskTracker.Constants;
using TaskTracker.Entities.General;
using TaskTracker.Resources;
using TaskTracker.Services.NotificationServices.Entities;

namespace TaskTracker.Services.NotificationServices
{
    public class UsersNotifier
    {
        public void NotifyUserAboutRegistration(User user, string password)
        {
            if (user.Email.Equals(SeedsConstants.DefaultAdminName)) { return;}

            var configuraton = new SmtpConfigurator().CreateBaseGmailConfiguration();
            var service = new EmailService(configuraton);

            var message = new TrackerEmailMessage
            {
                Destination = user.Email,
                Subject = SystemResources.rs_NotificationRegistrationSubject,
                Body = string.Concat(user.RealName, SystemResources.rs_NotificationRegistrationBody, password)
            };

            service.SendAsync(message);
        }

        public void NotifyUserAboutPasswordChanged(User user, string newPassword)
        {
            if (user.Email.Equals(SeedsConstants.DefaultAdminName)) { return; }

            var configuration = new SmtpConfigurator().CreateBaseGmailConfiguration();
            var service = new EmailService(configuration);

            var message = new TrackerEmailMessage
            {
                Destination = user.Email,
                Subject = NotificationResources.rs_PasswordChangedSubject,
                Body = string.Format(NotificationResources.rs_PasswordChangedBody, user.RealName, newPassword)
            };

            service.SendAsync(message);
        }
    }
}
