﻿using System;
using System.Collections.Generic;
using System.Globalization;
using EmitMapper;
using EmitMapper.MappingConfiguration;
using TaskTracker.DAL.DTO.Enums;
using TaskTracker.DAL.DTO.General;
using TaskTracker.DAL.DTO.Infrastructure;
using TaskTracker.DAL.DTO.Projects;
using TaskTracker.DAL.DTO.Tasks;
using TaskTracker.Entities.General;
using TaskTracker.Entities.Infrastructure;
using TaskTracker.Entities.ProjectPart;
using TaskTracker.Entities.TaskPart;
using TaskTracker.Utils;

namespace TaskTracker.Services.DbServices.Converters
{
    public static class DtoConverter
    {
        #region"Infrastructure"

        public static DtoDepartment Converter(Department entity)
        {
            if (entity == null)
                return null;

            #region "Handwriting realization"
            //return new DtoDepartment
            //{
            //    DepartmentId = entity.DepartmentId,
            //    IsRecordActive = entity.IsRecordActive,
            //    Name = entity.Name
            //};
            #endregion

            DtoDepartment ret = new DtoDepartment();
            ObjectsMapper<Department, DtoDepartment> emitMapper = ObjectMapperManager.DefaultInstance.GetMapper<Department, DtoDepartment>();
            return emitMapper.Map(entity, ret);
        }

        public static Department Converter(DtoDepartment dto)
        {
            if (dto == null)
                return null;

            #region "Handwriting realization"
            //return new Department
            //{
            //    DepartmentId = dto.DepartmentId,
            //    IsRecordActive = dto.IsRecordActive,
            //    Name = dto.Name
            //};
            #endregion

            Department ret = new Department();
            ObjectsMapper<DtoDepartment, Department> emitMapper = ObjectMapperManager.DefaultInstance.GetMapper<DtoDepartment, Department>();
            return emitMapper.Map(dto, ret);
        }

        #endregion

        #region"General"

        public static DtoPosition Converter(Position entity)
        {
            if (entity == null)
                return null;

            #region "Handwriting realization"
            //return new DtoPosition 
            //{
            //    PositionId = entity.PositionId,
            //    IsRecordActive = entity.IsRecordActive,
            //    Title = entity.Title
            //};
            #endregion

            DtoPosition ret = new DtoPosition();
            ObjectsMapper<Position, DtoPosition> emitMapper = ObjectMapperManager.DefaultInstance.GetMapper<Position, DtoPosition>();
            return emitMapper.Map(entity, ret);
        }

        public static Position Converter(DtoPosition dto)
            {
                if (dto == null)
                    return null;

            #region "Handwriting realization"
            //return new Position
            //{
            //    PositionId = dto.PositionId,
            //    IsRecordActive = dto.IsRecordActive,
            //    Title = dto.Title
            //};
            #endregion

            Position ret = new Position();
            ObjectsMapper<DtoPosition, Position> emitMapper = ObjectMapperManager.DefaultInstance.GetMapper<DtoPosition, Position>();
            return emitMapper.Map(dto, ret);
            

        }

        public static DtoRoleAction Converter(RoleAction entity, int parentRoleId)
        {
            if (entity == null)
                return null;

            #region "Handwriting realization"
            //return new DtoRoleAction
            //{
            //    RoleId = parentRoleId,
            //    ProjectActionValue = (int)entity.ProjectActionValue,
            //    IsSet = entity.IsSet
            //};
            #endregion

            DtoRoleAction ret = new DtoRoleAction();
            ObjectsMapper<RoleAction, DtoRoleAction> emitMapper = ObjectMapperManager
                                                                    .DefaultInstance
                                                                    .GetMapper<RoleAction, DtoRoleAction>(
                                                                        new DefaultMapConfig().ConvertUsing<ProjectActionsEnum, int>(v => (int)v));
            ret =  emitMapper.Map(entity);

            ret.RoleId = parentRoleId;

            return ret;
        }

        public static RoleAction Converter(DtoRoleAction dto)
        {
            if (dto == null)
                return null;

            #region "Handwriting realization"
            //return new RoleAction
            //{
            //    ProjectActionValue = Enum.IsDefined(typeof (ProjectActionsEnum), dto.ProjectActionValue)
            //                                        ? (ProjectActionsEnum) dto.ProjectActionValue
            //                                        : ProjectActionsEnum.DisableAll,
            //    IsSet = dto.IsSet
            //};
            #endregion

            DefaultMapConfig config = new DefaultMapConfig();

            config.ConvertUsing((DtoRoleAction source) =>
                                                        new RoleAction()
                                                        {
                                                            ProjectActionValue = Enum.IsDefined(typeof (ProjectActionsEnum), source.ProjectActionValue)
                                                                ? (ProjectActionsEnum) source.ProjectActionValue
                                                                : ProjectActionsEnum.DisableAll,

                                                            IsSet = source.IsSet
                                                        });

            var ret = ObjectMapperManager
                        .DefaultInstance
                        .GetMapper<DtoRoleAction, RoleAction>(config)
                        .Map(dto);

            return ret;
        }

        public static DtoRole Converter(Role entity)
        {
            if (entity == null)
                return null;

            #region "Handwriting realization"
            //return new DtoRole
            //{
            //    RoleId = entity.RoleId,
            //    Name = entity.Name,
            //    IsSuperAdmin = entity.IsSuperAdmin,
            //    IsRecordActive = entity.IsRecordActive
            //};
            #endregion

            DtoRole ret = new DtoRole();
            ObjectsMapper<Role, DtoRole> emitMapper = ObjectMapperManager.DefaultInstance.GetMapper<Role, DtoRole>();
            return emitMapper.Map(entity, ret);
        }

        public static Role Converter(DtoRole dto)
        {
            if (dto == null)
              return null;

            #region "Handwriting realization"
            //return new Role
            //{
            //    RoleId = dto.RoleId,
            //    Name = dto.Name,
            //    IsSuperAdmin =  dto.IsSuperAdmin,
            //    IsRecordActive = dto.IsRecordActive
            //};
            #endregion

            Role ret = new Role();
            ObjectsMapper<DtoRole, Role> emitMapper = ObjectMapperManager.DefaultInstance.GetMapper<DtoRole, Role>();
            return emitMapper.Map(dto, ret);
        }

        public static DtoUser Converter(User entity)
        {
            if (entity == null)
                return null;

            #region "Handwriting realization"
            //return new DtoUser
            //{
            //    Avatar = Converter(entity.Avatar),
            //    AvatarId = entity.Avatar.AvatarId,
            //    Department = Converter(entity.Department),
            //    DepartmentId = entity.Department.DepartmentId,
            //    Email = entity.Email,
            //    IsNeedPassCorrection = entity.IsNeedPassCorrection,
            //    IsRecordActive = entity.IsRecordActive,
            //    NickName = entity.NickName,
            //    PassHash = entity.PassHash,
            //    PassSalt = entity.PassSalt,
            //    RealName = entity.RealName,
            //    Role = Converter(entity.Role),
            //    RoleId = entity.Role.RoleId,
            //    UserId = entity.UserId
            //};
            #endregion

            DefaultMapConfig config = new DefaultMapConfig();

            config.ConvertUsing((User source) => new DtoUser()
                                                {
                                                    Avatar = Converter(source.Avatar),
                                                    AvatarId = source.Avatar.AvatarId,
                                                    Department = Converter(source.Department),
                                                    DepartmentId = source.Department.DepartmentId,
                                                    Email = source.Email,
                                                    IsNeedPassCorrection = source.IsNeedPassCorrection,
                                                    IsRecordActive = source.IsRecordActive,
                                                    NickName = source.NickName,
                                                    PassHash = source.PassHash,
                                                    PassSalt = source.PassSalt,
                                                    RealName = source.RealName,
                                                    Role = Converter(source.Role),
                                                    RoleId = source.Role.RoleId,
                                                    UserId = source.UserId
                                                });

            var ret = ObjectMapperManager
                        .DefaultInstance
                        .GetMapper<User, DtoUser>(config)
                        .Map(entity);

            return ret;
        }

        public static User Converter(DtoUser dto)
        {
            if (dto == null)
                return null;

            #region "Handwriting realization"
            //return new User
            //{
            //    Avatar = Converter(dto.Avatar),
            //    Department = Converter(dto.Department),
            //    Email = dto.Email,
            //    IsNeedPassCorrection = dto.IsNeedPassCorrection,
            //    IsRecordActive = dto.IsRecordActive,
            //    NickName = dto.NickName,
            //    PassHash = dto.PassHash,
            //    PassSalt = dto.PassSalt,
            //    RealName = dto.RealName,
            //    Role = Converter(dto.Role),
            //    UserId = dto.UserId
            //};
            #endregion

            DefaultMapConfig config = new DefaultMapConfig();

            config.ConvertUsing((DtoUser source) => new User()
                                                    {
                                                        Avatar = Converter(source.Avatar),
                                                        Department = Converter(source.Department),
                                                        Email = source.Email,
                                                        IsNeedPassCorrection = source.IsNeedPassCorrection,
                                                        IsRecordActive = source.IsRecordActive,
                                                        NickName = source.NickName,
                                                        PassHash = source.PassHash,
                                                        PassSalt = source.PassSalt,
                                                        RealName = source.RealName,
                                                        Role = Converter(source.Role),
                                                        UserId = source.UserId
                                                    });

            var ret = ObjectMapperManager
                        .DefaultInstance
                        .GetMapper<DtoUser, User>(config)
                        .Map(dto);

            return ret;
        }

        public static DtoAvatar Converter(Avatar entity)
        {
            if (entity == null)
                return null;

            #region "Handwriting realization"
            //return new DtoAvatar
            //{
            //    AvatarId = entity.AvatarId,
            //    Data = entity.Data
            //};
            #endregion

            DtoAvatar ret = new DtoAvatar();
            ObjectsMapper<Avatar, DtoAvatar> emitMapper = ObjectMapperManager.DefaultInstance.GetMapper<Avatar, DtoAvatar>();
            return emitMapper.Map(entity, ret);
        }

        public static Avatar Converter(DtoAvatar dto)
        {
            if (dto == null)
                return null;

            #region "Handwriting realization"
            //return new Avatar
            //{
            //    AvatarId = dto.AvatarId,
            //    Data = dto.Data
            //};
            #endregion

            Avatar ret = new Avatar();
            ObjectsMapper<DtoAvatar, Avatar> emitMapper = ObjectMapperManager.DefaultInstance.GetMapper<DtoAvatar, Avatar>();
            return emitMapper.Map(dto, ret);
        }

        public static DtoComment Converter(TaskComment entity)
        {
            if (entity == null)
                return null;

            DefaultMapConfig config = new DefaultMapConfig();

            config.ConvertUsing((TaskComment source) => new DtoComment()
            {
                CommentId = source.TaskCommentId,
                AuthorUserId = source.Author.UserId,
                Author = Converter(source.Author),
                Text = source.Text,
                WhenPosted = source.WhenPost,
                IsRecordActive = source.IsRecordActive
            });

            var ret = ObjectMapperManager.DefaultInstance.GetMapper<TaskComment, DtoComment>().Map(entity);
            return ret;
        }

        #endregion

        #region "Task"

        public static TaskComment Converter(DtoComment entity, int parentTaskId)
        {
            if (entity == null)
                return null;

            DefaultMapConfig config = new DefaultMapConfig();

            config.ConvertUsing((DtoComment source) => new TaskComment()
            {
                TaskCommentId = source.CommentId,
                Author = Converter(source.Author),
                Text = source.Text,
                WhenPost = source.WhenPosted,
                IsRecordActive = source.IsRecordActive
            });

            var ret = ObjectMapperManager.DefaultInstance.GetMapper<DtoComment, TaskComment>().Map(entity);

            ret.TaskId = parentTaskId;

            return ret;
        }

        public static TaskComment Converter(DtoTaskComment entity)
        {
            if (entity == null)
                return null;

            DefaultMapConfig config = new DefaultMapConfig();

            config.ConvertUsing((DtoTaskComment source) => new TaskComment()
            {
                TaskId = source.TaskId,
                TaskCommentId = source.CommentId
            });

            var ret = ObjectMapperManager.DefaultInstance.GetMapper<DtoTaskComment, TaskComment>().Map(entity);
            
            return ret;
        }

        public static DtoTaskComment Converter(TaskComment entity, int emptyValue)
        {
            if (entity == null)
                return null;

            DefaultMapConfig config = new DefaultMapConfig();

            config.ConvertUsing((TaskComment source) => new DtoTaskComment()
            {
                CommentId = source.TaskCommentId,
                TaskId = source.TaskId
            });

            var ret = ObjectMapperManager.DefaultInstance.GetMapper<TaskComment, DtoTaskComment>().Map(entity);
            return ret;
        }

        public static DtoTaskType Converter(TaskType entity)
        {
            if (entity == null)
                return null;

            #region "Handwriting realization"
            //return new DtoTaskType
            //{
            //    TaskTypeId = entity.TaskTypeId,
            //    Name = entity.Name,
            //    IsRecordActive = entity.IsRecordActive
            //};
            #endregion

            DtoTaskType ret = new DtoTaskType();
            ObjectsMapper<TaskType, DtoTaskType> emitMapper = ObjectMapperManager.DefaultInstance.GetMapper<TaskType, DtoTaskType>();
            return emitMapper.Map(entity, ret);
        }

        public static TaskType Converter(DtoTaskType dto)
        {
            if (dto == null)
                return null;

            #region "Handwriting realization"
            //return new TaskType
            //{
            //    TaskTypeId = dto.TaskTypeId,
            //    Name = dto.Name,
            //    IsRecordActive = dto.IsRecordActive
            //};
            #endregion

            TaskType ret = new TaskType();
            ObjectsMapper<DtoTaskType, TaskType> emitMapper = ObjectMapperManager.DefaultInstance.GetMapper<DtoTaskType, TaskType>();
            return emitMapper.Map(dto, ret);
        }

        public static DtoTaskStatus Converter(TaskStatus entity)
        {
            if (entity == null)
                return null;

            #region "Handwriting realization"
            //return new DtoTaskStatus
            //{
            //    TaskStatusId = entity.TaskStatusId,
            //    Name = entity.Name,
            //    IsRecordActive = entity.IsRecordActive
            //};
            #endregion

            DtoTaskStatus ret = new DtoTaskStatus();
            ObjectsMapper<TaskStatus, DtoTaskStatus> emitMapper = ObjectMapperManager.DefaultInstance.GetMapper<TaskStatus, DtoTaskStatus>();
            return emitMapper.Map(entity, ret);
        }

        public static TaskStatus Converter(DtoTaskStatus dto)
        {
            if (dto == null)
                return null;

            #region "Handwriting realization"
            //return new TaskStatus
            //{
            //    TaskStatusId = dto.TaskStatusId,
            //    Name = dto.Name,
            //    IsRecordActive = dto.IsRecordActive
            //};
            #endregion

            TaskStatus ret = new TaskStatus();
            ObjectsMapper<DtoTaskStatus, TaskStatus> emitMapper = ObjectMapperManager.DefaultInstance.GetMapper<DtoTaskStatus, TaskStatus>();
            return emitMapper.Map(dto, ret);
        }

        public static DtoTaskAttachment Converter(TaskAttachment entity)
        {
            if (entity == null)
                return null;

            #region "Handwriting realization"
            //return new DtoTaskAttachment
            //{
            //    TaskAttachmentId = entity.TaskAttachmentsId,
            //    AuthorUserId = entity.Author.UserId,
            //    Name = entity.Name,
            //    Description = entity.Description,
            //    TaskId = entity.TaskId,
            //    Mime = entity.MIME,
            //    AttachDate = entity.AttachDate,
            //    Data = entity.Data,
            //    IsRecordActive = entity.IsRecordActive
            //};
            #endregion

            DefaultMapConfig config = new DefaultMapConfig();

            config.ConvertUsing((TaskAttachment source) => new DtoTaskAttachment()
                                                                {
                                                                    TaskAttachmentId = source.TaskAttachmentsId,
                                                                    Author = Converter(source.Author),
                                                                    AuthorUserId = source.Author.UserId,
                                                                    AttachDate = source.AttachDate,
                                                                    Name = source.Name,
                                                                    Description = source.Description,
                                                                    TaskId = source.TaskId,
                                                                    Mime = source.MIME,
                                                                    Data = source.Data,
                                                                    IsRecordActive = source.IsRecordActive
                                                                });

            var ret = ObjectMapperManager
                        .DefaultInstance
                        .GetMapper<TaskAttachment, DtoTaskAttachment>(config)
                        .Map(entity);

            return ret;
        }

        public static TaskAttachment Converter(DtoTaskAttachment dto)
        {
            if (dto == null)
                return null;

            #region "Handwriting realization"
            //return new TaskAttachment
            //{
            //    TaskAttachmentsId = dto.TaskAttachmentId,
            //    Author = Converter(dto.Author),
            //    Name = dto.Name,
            //    Description = dto.Description,
            //    TaskId = dto.TaskId,
            //    AttachDate = dto.AttachDate,
            //    MIME = dto.Mime,
            //    Data = dto.Data,
            //    IsRecordActive = dto.IsRecordActive
            //};
            #endregion

            DefaultMapConfig config = new DefaultMapConfig();

            config.ConvertUsing((DtoTaskAttachment source) =>
                                                        new TaskAttachment()
                                                        {
                                                            TaskAttachmentsId = source.TaskAttachmentId,
                                                            Author = Converter(source.Author),
                                                            AttachDate = source.AttachDate,
                                                            Name = source.Name,
                                                            Description = source.Description,
                                                            TaskId = source.TaskId,
                                                            MIME = source.Mime,
                                                            Data = source.Data,
                                                            IsRecordActive = source.IsRecordActive
                                                        });

            var ret = ObjectMapperManager
                        .DefaultInstance
                        .GetMapper<DtoTaskAttachment, TaskAttachment>(config)
                        .Map(dto);

            return ret;
        }

        public static DtoTaskStatusHistoryItem Converter(TaskStatusHistoryItem entity)
        {
            if (entity == null)
                return null;

            DefaultMapConfig config = new DefaultMapConfig();

            config.ConvertUsing((TaskStatusHistoryItem source) => new DtoTaskStatusHistoryItem()
            {
                TaskStatusHistoryItemId = source.TaskStatusHistoryItemId,
                TaskId = source.TaskId,
                AssignedTo = Converter(source.AssignedTo),
                AssignedToUserId = source.AssignedTo.UserId,
                CreationDate = source.CreationDate,
                TaskStatus = Converter(source.TaskStatus),
                TaskStatusId = source.TaskStatus.TaskStatusId,
                IsRecordActive = source.IsRecordActive
            });

            var ret = ObjectMapperManager.DefaultInstance.GetMapper<TaskStatusHistoryItem, DtoTaskStatusHistoryItem>().Map(entity);
            return ret;
        }

        public static TaskStatusHistoryItem Converter(DtoTaskStatusHistoryItem dto)
        {
            if (dto == null)
                return null;

            DefaultMapConfig config = new DefaultMapConfig();

            config.ConvertUsing((DtoTaskStatusHistoryItem source) => new TaskStatusHistoryItem()
            {
                TaskStatusHistoryItemId = source.TaskStatusHistoryItemId,
                TaskId = source.TaskId,
                AssignedTo = Converter(source.AssignedTo),
                CreationDate = source.CreationDate,
                TaskStatus = Converter(source.TaskStatus),
                IsRecordActive = source.IsRecordActive
            });

            var ret = ObjectMapperManager.DefaultInstance.GetMapper<DtoTaskStatusHistoryItem, TaskStatusHistoryItem>().Map(dto);
            return ret;
        }

        public static DtoTaskTimeTrackerItem Converter(TaskTimeTrackerItem entity)
        {
            if (entity == null)
                return null;

            DefaultMapConfig config = new DefaultMapConfig();

            config.ConvertUsing((TaskTimeTrackerItem source) => new DtoTaskTimeTrackerItem()
            {
                TaskTimeTrackerItemId = source.TaskTimeTrackerItemId,
                TaskId = source.TaskId,
                Executor = Converter(source.Executor),
                ExecutorUserId = source.Executor.UserId,
                CreationDate = source.CreationDate,
                Description = source.Description,
                SpentTime = source.SpentTime,
                IsRecordActive = source.IsRecordActive
            });

            var ret = ObjectMapperManager.DefaultInstance.GetMapper<TaskTimeTrackerItem, DtoTaskTimeTrackerItem>().Map(entity);
            return ret;
        }

        public static TaskTimeTrackerItem Converter(DtoTaskTimeTrackerItem dto)
        {
            if (dto == null)
                return null;

            DefaultMapConfig config = new DefaultMapConfig();

            config.ConvertUsing((DtoTaskTimeTrackerItem source) => new TaskTimeTrackerItem()
            {
                TaskTimeTrackerItemId = source.TaskTimeTrackerItemId,
                TaskId = source.TaskId,
                Executor = Converter(source.Executor),
                CreationDate = source.CreationDate,
                Description = source.Description,
                SpentTime = source.SpentTime,
                IsRecordActive = source.IsRecordActive
            });

            var ret = ObjectMapperManager.DefaultInstance.GetMapper<DtoTaskTimeTrackerItem, TaskTimeTrackerItem>().Map(dto);
            return ret;
        }

        public static DtoTask Converter(Task entity)
        {
            if (entity == null)
                return null;

            DefaultMapConfig config = new DefaultMapConfig();

            config.ConvertUsing((Task source) => new DtoTask()
            {
                TaskId = source.TaskId,
                ProjectId = source.ProjectId,
                Acronym = source.Acronym,
                Description = source.Description,
                EstimateTime = source.EstimateTime,
                SpentTime = source.SpentTime,
                Caption = source.Caption,
                AssignedTo = Converter(source.AssignedTo),
                AssignedToUserId = source.AssignedTo.UserId,
                Author = Converter(source.Author),
                AuthorUserId = source.Author.UserId,
                TaskStatus = Converter(source.Status),
                TaskStatusId = source.Status.TaskStatusId,
                TaskType = Converter(source.Type),
                TaskTypeId = source.Type.TaskTypeId,
                MasterTaskId = source.MasterTaskId,
                CreateTime = source.CreationTime,

                IsRecordAcitve = source.IsRecordActive
            });

            var ret = ObjectMapperManager.DefaultInstance.GetMapper<Task, DtoTask>().Map(entity);

            foreach (var taskStatusHistoryItem in entity.StatusHistory)
            {
                ret.TaskStatusHistory.Add(Converter(taskStatusHistoryItem));
            }

            foreach (var taskAttachment in entity.Attachments)
            {
                ret.Attachments.Add(Converter(taskAttachment));
            }

            foreach (var taskComment in entity.Comments)
            {
                ret.Comments.Add(Converter(taskComment, 0));
            }

            foreach (var taskTimeTrackerItem in entity.TaskTimeTracker)
            {
                ret.TaskTimeTracker.Add(Converter(taskTimeTrackerItem));
            }

            return ret;
        }

        public static Task Converter(DtoTask dto)
        {
            if (dto == null)
                return null;

            DefaultMapConfig config = new DefaultMapConfig();

            config.ConvertUsing((DtoTask source) => new Task()
            {
                TaskId = source.TaskId,
                ProjectId = source.ProjectId,
                Acronym = source.Acronym,
                Description = source.Description,
                EstimateTime = source.EstimateTime,
                SpentTime = source.SpentTime,
                Caption = source.Caption,
                AssignedTo = Converter(source.AssignedTo),
                Author = Converter(source.Author),
                Status = Converter(source.TaskStatus),
                Type = Converter(source.TaskType),
                MasterTaskId = source.MasterTaskId,
                CreationTime = source.CreateTime,

                IsRecordActive = source.IsRecordAcitve
            });

            var ret = ObjectMapperManager.DefaultInstance.GetMapper<DtoTask, Task>().Map(dto);

            foreach (var taskStatusHistoryItem in dto.TaskStatusHistory)
            {
                ret.StatusHistory.Add(Converter(taskStatusHistoryItem));
            }

            foreach (var taskAttachment in dto.Attachments)
            {
                ret.Attachments.Add(Converter(taskAttachment));
            }

            foreach (var taskComment in dto.Comments)
            {
                ret.Comments.Add(Converter(taskComment));
            }

            foreach (var taskTimeTrackerItem in dto.TaskTimeTracker)
            {
                ret.TaskTimeTracker.Add(Converter(taskTimeTrackerItem));
            }

            return ret;
        }

        #endregion

        #region "Project"

        public static DtoTeam Converter(Team entity)
        {
            if (entity == null)
                return null;
            
            DtoTeam ret = new DtoTeam();
            ObjectsMapper<Team, DtoTeam> emitMapper = ObjectMapperManager.DefaultInstance.GetMapper<Team, DtoTeam>();
            return emitMapper.Map(entity, ret);
        }

        public static Team Converter(DtoTeam dto)
        {
            if (dto == null)
                return null;
            
            Team ret = new Team();
            ObjectsMapper<DtoTeam, Team> emitMapper = ObjectMapperManager.DefaultInstance.GetMapper<DtoTeam, Team>();
            return emitMapper.Map(dto, ret);
        }

        public static DtoTeamMember Converter(TeamMember entity)
        {
            if (entity == null)
                return null;

            DefaultMapConfig config = new DefaultMapConfig();

            config.ConvertUsing((TeamMember source) => new DtoTeamMember()
            {
                Team = Converter(source.Team),
                TeamId = source.Team.TeamId,
                Position = Converter(source.Position),
                PositionId = source.Position.PositionId,
                User = Converter(source.User),
                UserId = source.User.UserId
            });

            var ret = ObjectMapperManager.DefaultInstance.GetMapper<TeamMember, DtoTeamMember>().Map(entity);
            return ret;
        }

        public static TeamMember Converter(DtoTeamMember dto)
        {
            if (dto == null)
                return null;

            DefaultMapConfig config = new DefaultMapConfig();

            config.ConvertUsing((DtoTeamMember source) => new TeamMember()
            {
                Team = Converter(source.Team),
                Position = Converter(source.Position),
                User = Converter(source.User)
            });

            var ret = ObjectMapperManager.DefaultInstance.GetMapper<DtoTeamMember, TeamMember>().Map(dto);
            return ret;
        }

        public static DtoProject Converter(Project entity)
        {
            if (entity == null)
                return null;

            DefaultMapConfig config = new DefaultMapConfig();

            config.ConvertUsing((Project source) => new DtoProject()
            {
                Team = Converter(source.Team),
                TeamId = source.Team.TeamId,
                Name = source.Name,
                Acronym = source.Acronym,
                Description = source.Description,
                EstimateTime = source.EstimateTime,
                IsRecordActive = source.IsRecordActive,
                ProjectId = source.ProjectId,
                ProjectStatus = (int)source.Status
            });

            var ret = ObjectMapperManager.DefaultInstance.GetMapper<Project, DtoProject>().Map(entity);

            foreach (var taskItem in entity.Backlog)
            {
                ret.Tasks.Add(Converter(taskItem));
            }

            foreach (var sprintItem in entity.Sprints)
            {
                ret.Sprints.Add(Converter(sprintItem));
            }


            return ret;
        }

        public static Project Converter(DtoProject dto)
        {
            if (dto == null)
                return null;

            DefaultMapConfig config = new DefaultMapConfig();

            config.ConvertUsing((DtoProject source) => new Project()
            {
                Team = Converter(source.Team),
                Name = source.Name,
                Acronym = source.Acronym,
                Description = source.Description,
                EstimateTime = source.EstimateTime,
                IsRecordActive = source.IsRecordActive,
                ProjectId = source.ProjectId,
                Status = Enum.IsDefined(typeof(ProjectStatusesEnum), source.ProjectStatus)
                                                                ? (ProjectStatusesEnum)source.ProjectStatus
                                                                : ProjectStatusesEnum.NotSet
            });

            var ret = ObjectMapperManager.DefaultInstance.GetMapper<DtoProject, Project>().Map(dto);

            foreach (var taskItem in dto.Tasks)
            {
                ret.Backlog.Add(Converter(taskItem));
            }

            foreach (var sprintItem in dto.Sprints)
            {
                ret.Sprints.Add(Converter(sprintItem));
            }

            return ret;
        }

        public static DtoSprint Converter(Sprint entity)
        {
            if (entity == null)
                return null;

            DefaultMapConfig config = new DefaultMapConfig();

            config.ConvertUsing((Sprint source) => new DtoSprint()
            {
                SprintId = source.SprintId,
                Project = Converter(source.Project),
                ProjectId = source.Project.ProjectId,
                Caption = source.Caption,
                Description = source.Description,
                StartDate = source.StartDate,
                EndDate = source.EndDate,
                SprintStatus = (int)source.SprintStatus,
                IsRecordActive = source.IsRecordActive
            });

            var ret = ObjectMapperManager.DefaultInstance.GetMapper<Sprint, DtoSprint>().Map(entity);

            //foreach (var taskItem in entity.SprintBacklog)
            //{
            //    ret.Backlog.Add(Converter(taskItem));
            //}
            
            return ret;
        }

        public static Sprint Converter(DtoSprint dto)
        {
            if (dto == null)
                return null;

            DefaultMapConfig config = new DefaultMapConfig();

            config.ConvertUsing((DtoSprint source) => new Sprint()
            {
                SprintId = source.SprintId,
                Project = Converter(source.Project),
                Caption = source.Caption,
                Description = source.Description,
                StartDate = source.StartDate,
                EndDate = source.EndDate,
                IsRecordActive = source.IsRecordActive,
                SprintStatus = Enum.IsDefined(typeof(SprintStatusesEnum), source.SprintStatus)
                                                                ? (SprintStatusesEnum)source.SprintStatus
                                                                : SprintStatusesEnum.NotSet
            });

            var ret = ObjectMapperManager.DefaultInstance.GetMapper<DtoSprint, Sprint>().Map(dto);

            //foreach (var sprintBacklogItem in dto.Backlog)
            //{
            //    ret.SprintBacklog.Add(Converter(sprintBacklogItem));
            //}
            
            return ret;
        }



        #endregion

    }
}