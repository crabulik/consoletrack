﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;
using TaskTracker.Constants;
using TaskTracker.DAL;
using TaskTracker.DAL.Contracts;
using TaskTracker.DAL.DTO.General;
using TaskTracker.Entities.General;
using TaskTracker.Services.DbServices.Converters;
using TaskTracker.Services.Enums;
using TaskTracker.Services.NotificationServices;
using TaskTracker.Utils;

namespace TaskTracker.Services.DbServices
{
    public sealed class DbUsersService
    {
        private IUnitOfWork Uow { get; }

        public DbUsersService(IUnitOfWork uow)
        {
            Uow = uow;
        }
        //CheckUserCredentials, Avatars

        #region "For Role"

        public SaveResult SaveRole(Role newRole)
        {
            if (newRole == null)
            {
                throw new ArgumentNullException(nameof(newRole));
            }

            var repository = Uow.Roles;
            var dtoForSave = DtoConverter.Converter(newRole);

            if (newRole.RoleId == EntitiesConstants.IdIsNotSet)
            {
                repository.Add(dtoForSave);
            }
            else
            {
                var objectToUpdate = repository.GetSingle(newRole.RoleId);

                if (objectToUpdate == null)
                {
                    return SaveResult.EntityNotFound;
                }
                objectToUpdate.Copy(dtoForSave);
                repository.Edit(objectToUpdate);
            }

            Uow.Save();
            newRole.RoleId = dtoForSave.RoleId;
            return SaveResult.Saved;
        }

        public void DeleteRole(int id)
        {
            var deletedObject = Uow.Roles.GetSingle(id);
            if (deletedObject != null)
            {
                deletedObject.IsRecordActive = false;
                Uow.Roles.Edit(deletedObject);
                Uow.Save();
            }
        }

        public Role GetRole(int id)
        {
            var repository = Uow.Roles;
            var dto = repository.GetSingle(id);
            if (dto != null)
            {
                return DtoConverter.Converter(dto);
            }

            return null;
        }

        public Role GetRole(string name)
        {
            var repository = Uow.Roles;
            var roleDto = repository.GetAll().FirstOrDefault(p => String.Equals(p.Name, name));

            if (roleDto == null)
            {
                return null;
            }

            var role = DtoConverter.Converter(roleDto);
            return role;
        }

        public IList<Role> GetRoles()
        {
            var result = new List<Role>();

            var repository = Uow.Roles;

            foreach (var dto in repository.GetAll().Where(p => p.IsRecordActive))
            {
                result.Add(DtoConverter.Converter(dto));
            }

            return result;
        }

        #endregion

        #region Avatar

        public SaveResult SaveAvatar(Avatar newAvatar)
        {
            return SaveResult.EntityNotFound;
        }

        public void DeleteAvatar(int id)
        {

        }

        public Avatar GetAvatar(int id)
        {
            var avatarsRepo = Uow.Avatars;
            var dto = avatarsRepo.GetSingle(id);
            if (dto != null)
            {
                return DtoConverter.Converter(dto);
            }
            return null;
        }

        public Avatar GetDefaultAvatar()
        {
            var avatarsRepo = Uow.Avatars;
            var defaultAvatar = avatarsRepo.GetAll().FirstOrDefault(p => p.IsDefault);
            if (defaultAvatar != null)
            {
                return DtoConverter.Converter(defaultAvatar);
            }
            return null;
        }

        public IList<Avatar> GetAvatars()
        {
            return null;
        }

        #endregion

        #region User

        public SaveResult SaveUser(User newUser)
        {
            if (newUser == null)
            {
                throw new ArgumentNullException(nameof(newUser));
            }

            var usersRepo = Uow.Users;

            var dtoUser = DtoConverter.Converter(newUser);

            //If references to existing pbjects, need to null them for avoiding creating duplicates.

            if (dtoUser.AvatarId != EntitiesConstants.IdIsNotSet)
            {
                dtoUser.Avatar = null;
            }
            if (dtoUser.RoleId != EntitiesConstants.IdIsNotSet)
            {
                dtoUser.Role = null;
            }
            if (dtoUser.DepartmentId != EntitiesConstants.IdIsNotSet)
            {
                dtoUser.Department = null;
            }

            // Save operations
            if (newUser.UserId == EntitiesConstants.IdIsNotSet)
            {
                usersRepo.Add(dtoUser);
            }
            else
            {
                var userToUpdate = usersRepo.GetSingle(newUser.UserId);
                if (userToUpdate == null)
                {
                    return SaveResult.EntityNotFound;
                }
                userToUpdate.DepartmentId = DtoConverter.Converter(newUser).DepartmentId;
                userToUpdate.RoleId = DtoConverter.Converter(newUser).RoleId;
                userToUpdate.Copy(dtoUser);
                usersRepo.Edit(userToUpdate);
            }
            try
            {
                Uow.Save();
                newUser.UserId = dtoUser.UserId;
                return SaveResult.Saved;
            }
            catch (DataException)
            {
                // no duplicate users on my watch!
            }
            return SaveResult.UndefinedError;
        }

        public void DeleteUser(int id)
        {
            var deletedObject = Uow.Users.GetSingle(id);
            if (deletedObject != null)
            {
                deletedObject.IsRecordActive = false;
                Uow.Users.Edit(deletedObject);
                Uow.Save();
            }
        }

        public User GetUser(int id)
        {
            var repository = Uow.Users;
            var departmentsRepository = Uow.Departments;
            var rolesRepository = Uow.Roles;
            var avatarsRepository = Uow.Avatars;
            var dto = repository.GetSingle(id);
            if (dto != null)
            {
                dto.Role = rolesRepository.GetSingle(dto.RoleId);
                dto.Department = departmentsRepository.GetSingle(dto.DepartmentId);
                dto.Avatar = avatarsRepository.GetSingle(dto.AvatarId);
                return DtoConverter.Converter(dto);
            }

            return null;
        }

        public IList<User> GetUsers()
        {
            var result = new List<User>();

            var repository = Uow.Users;

            foreach (var dto in repository.GetAll().Where(p => p.IsRecordActive))
            {
                result.Add(DtoConverter.Converter(dto));
            }

            return result;
        }

        public IList<User> GetUsersForGrid()
        {
            var result = new List<User>();

            var repository = Uow.Users;

            var allStoUsers = repository.GetAll(new []{ nameof(DtoUser.Role), nameof(DtoUser.Department) }).Where(p => p.IsRecordActive);

            foreach (var dto in allStoUsers)
            {
                result.Add(DtoConverter.Converter(dto));
            }

            return result;
        }

        public User GetUser(string email)
        {
            var repository = Uow.Users;
            var userDto = repository.GetAll().FirstOrDefault(p => String.Equals(p.Email, email));

            if (userDto == null)
            {
                return null;
            }

            var user = DtoConverter.Converter(userDto);
            return user;
        }       
        #endregion

    }
}
