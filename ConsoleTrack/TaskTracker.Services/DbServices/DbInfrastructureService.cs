﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaskTracker.Constants;
using TaskTracker.DAL.Contracts;
using TaskTracker.DAL.Exceptions;
using TaskTracker.Entities.General;
using TaskTracker.Entities.Infrastructure;
using TaskTracker.Services.DbServices.Converters;
using TaskTracker.Services.Enums;

namespace TaskTracker.Services.DbServices
{
    public sealed class DbInfrastructureService
    {
        private IUnitOfWork Uow { get; }

        public DbInfrastructureService(IUnitOfWork uow)
        {
            Uow = uow;
        }

        public IList<Department> GetDepartments()
        {
            var result = new List<Department>();

            var repository = Uow.Departments;

            foreach (var dto in repository.GetAll().Where(p => p.IsRecordActive))
            {
                result.Add(DtoConverter.Converter(dto));
            }

            return result;
        }

        public Department GetDepartment(int id)
        {
            var repository = Uow.Departments;
            var dto = repository.GetSingle(id);
            if (dto != null)
            {
                return DtoConverter.Converter(dto);
            }

            return null;
        }

        public void DeleteDepartment(int id)
        {
            var deletedObject = Uow.Departments.GetSingle(id);
            if (deletedObject != null)
            {
                deletedObject.IsRecordActive = false;
                Uow.Departments.Edit(deletedObject);
                Uow.Save();
            }
        }

        public SaveResult SaveDepartment(Department entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }
            
            var repository = Uow.Departments;
            var dtoForSave = DtoConverter.Converter(entity);
            if (entity.DepartmentId == EntitiesConstants.IdIsNotSet)
            {
                
                repository.Add(dtoForSave);              
            }
            else
            {
                var objectToUpdate = repository.GetSingle(entity.DepartmentId);
                if (objectToUpdate == null)
                {
                    return SaveResult.EntityNotFound;
                }
                objectToUpdate.Copy(dtoForSave);
                repository.Edit(objectToUpdate);

            }

            Uow.Save();
            entity.DepartmentId = dtoForSave.DepartmentId;
            return SaveResult.Saved;
        }
        //23.12.2015
        
        public IList<Position> GetPositions()
        {
            var result = new List<Position>();

            var repository = Uow.Positions;

            foreach (var dto in repository.GetAll().Where(p => p.IsRecordActive))
            {
                result.Add(DtoConverter.Converter(dto));
            }

            return result;
        }

        

        public Position GetPosition(int id)
        {
            var repository = Uow.Positions;
            var dto = repository.GetSingle(id);
            if (dto != null)
            {
                return DtoConverter.Converter(dto);
                // return DtoConverter.Converter(dto);
            }

            return null;
        }

        public void DeletePosition(int id)
        {
            var deletedObject = Uow.Positions.GetSingle(id);
            if (deletedObject != null)
            {
                deletedObject.IsRecordActive = false;
                Uow.Positions.Edit(deletedObject);
                Uow.Save();
            }
        }

        public SaveResult SavePosition(Position entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            var repository = Uow.Positions;
            var dtoForSave = DtoConverter.Converter(entity);
            if (entity.PositionId == EntitiesConstants.IdIsNotSet)
            {

                repository.Add(dtoForSave);
            }
            else
            {
                var objectToUpdate = repository.GetSingle(entity.PositionId);
                if (objectToUpdate == null)
                {
                    return SaveResult.EntityNotFound;
                }
                objectToUpdate.Copy(dtoForSave);
                repository.Edit(objectToUpdate);

            }

            Uow.Save();
            entity.PositionId = dtoForSave.PositionId;
            return SaveResult.Saved;
        }
    }
}
