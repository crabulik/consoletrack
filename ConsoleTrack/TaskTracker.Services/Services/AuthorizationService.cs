﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;
using TaskTracker.Constants;
using TaskTracker.Entities.General;
using TaskTracker.Resources;
using TaskTracker.Services.DbServices;
using TaskTracker.Services.Enums;
using TaskTracker.Services.Exceptions;
using TaskTracker.Services.NotificationServices;
using TaskTracker.Utils;

namespace TaskTracker.Services.Services
{
    public class AuthorizationService
    {
        private DbUsersService _usersService;
        private PasswordCypher _cypher;
        private UsersNotifier _notifier;

        public AuthorizationService(DbUsersService usersService, UsersNotifier notifier)
        {
            _usersService = usersService;
            _cypher = new PasswordCypher();
            _notifier = notifier;
        }

        public void CreateNewUser(User newUser)
        {
            var tmpPass = GetTmpPass();
            _cypher.Generate(tmpPass);
            newUser.PassHash = _cypher.KeyBytes;
            newUser.PassSalt = _cypher.SaltBytes;
            var saveResult = _usersService.SaveUser(newUser);
            if (saveResult != SaveResult.Saved)
            {
                throw new AuthorizationServiceException(String.Format(SystemResources.rs_CreateNewUserSaveExceptionMessage, saveResult));
            }
            try
            {
                _notifier.NotifyUserAboutRegistration(newUser, tmpPass);
            }
            catch (FormatException)
            {
                // invalid email
            }

        }

        private string GetTmpPass()
        {
            return Membership.GeneratePassword(10, 1);
        }

        public int LogIn(string email, string password)
        {
            var user = _usersService.GetUser(email);

            if ((user == null) || (!user.IsRecordActive))
            {
                return DbConstants.LoginFail;
            }

            if (CheckUserPassword(user, password))
            {
                return user.UserId;
            }
            return DbConstants.LoginFail;
        }

        public bool IsPasswordMatch(int userId, string password)
        {
            var user = _usersService.GetUser(userId);

            if ((user != null) )
            {
                return CheckUserPassword(user, password);
            }

            return false;
        }

        private bool CheckUserPassword(User user, string password)
        {
            // Default admin with default password enters into the system with any password.
            if ((string.Equals(user.Email, SeedsConstants.DefaultAdminName)) && (user.PassHash.Length == 0) && (user.PassSalt.Length == 0))
            {
                return true;
            }

            var dbPasswordKey = user.PassHash;
            var dbSalt = user.PassSalt;
            _cypher.Generate(password, dbSalt);
            var genPasswordKey = _cypher.KeyBytes;
            

            if (dbPasswordKey != null && dbPasswordKey.SequenceEqual(genPasswordKey))
            {
                return true;
            }
            return false;
        }

        public void SetNewPassword(int userId, string oldPassword, string newPassword)
        {
            var user = _usersService.GetUser(userId);

            if ((user != null))
            {
                if (CheckUserPassword(user, oldPassword))
                {
                    _cypher.Generate(newPassword);
                    user.PassHash = _cypher.KeyBytes;
                    user.PassSalt = _cypher.SaltBytes;
                    user.IsNeedPassCorrection = false;
                    var saveResult = _usersService.SaveUser(user);
                    if (saveResult != SaveResult.Saved)
                    {
                        throw new AuthorizationServiceException(String.Format(SystemResources.rs_CreateNewUserSaveExceptionMessage, saveResult));
                    }
                    _notifier.NotifyUserAboutPasswordChanged(user, newPassword);
                }
                else
                {
                    throw new AuthorizationServiceException(String.Format(SystemResources.rs_UserOldPasswordMismatchMessage, userId));
                }
            }
            else
            {
                throw new AuthorizationServiceException(String.Format(SystemResources.rs_CantFindUserByIdExceptionMessage, userId));
            }
        }
    }
}
