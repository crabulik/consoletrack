﻿namespace TaskTracker.Services.Enums
{
    public enum SaveResult
    {
         Saved,
         EntityNotFound,
         UndefinedError
    }
}